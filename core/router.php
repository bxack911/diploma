<?php
class Route
{
	static function start()
	{
		if( !empty($_COOKIE['uid']) )
		{
			$controller_name = "my_page";
			$action_name = 'default';
			$params = array();
		}
		else
		{
			$controller_name = "auth";
			$action_name = 'default';
			$params = array();
		}
		
		$routes = explode('/', $_SERVER['REQUEST_URI']);

		if($routes[1] != "socket.io")
		{
		/*$modals = stristr($_SERVER['REQUEST_URI'], '#');
		var_dump(parse_url($_SERVER['SERVER_NAME'], PHP_URL_FRAGMENT))
		echo "<pre style='color: #fff' >".$_SERVER['QUERY_STRING']."</pre>";
		if( !empty($modals[0]) )
		{
			echo "<pre style='color: #fff' >".$modals[0]."</pre>";
		}*/

		if ( !empty($routes[1]) )
		{	
			$controller_name = $routes[1];
		}
		
		if ( !empty($routes[2]) )
		{
			$action_name = $routes[2];
		}

		if( !empty($routes[3]) )
		{
			$params[0] = $routes[3]; 
		}

		if( $controller_name == "api" )
		{
		    if( $action_name == "" )
		    {
		        Route::ErrorPage404();
		    }
		    else
		    {
		        $full_form_data = explode('?', $_SERVER['REQUEST_URI']);
		        if(!empty($full_form_data[2]))
		        {
		        	$form_data = explode("&", $full_form_data[1].$full_form_data[2]);
		        }
		        else
		        {
		        	$form_data = explode("&", $full_form_data[1]);
		        }

		        for( $i = 0; $i < count($form_data); $i++ )
		        {
		            $params[$i] = $form_data[$i];
		        }
		    }
		}

		$model_name = 'Model_'.$controller_name;
		$controller_name = 'Controller_'.$controller_name;
		$action_name = 'action_'.$action_name;

		$rep = str_replace("Model_","",$model_name);
		$model_file = "model_mod_aps_name_".$rep."_by_b-a.php";
		$model_path = "./models/".$model_file;
		if(file_exists($model_path))
		{
			if( !empty($_COOKIE['uid']) )
			{
				include "./models/".$model_file;
			}
			else
			{
				if($controller_name == "Controller_api")
				{
					include "./models/model_mod_aps_name_api_by_b-a.php";
				}
				else
				{
					include "./models/model_mod_aps_name_auth_by_b-a.php";
				}
			}
		}
		$replaced = str_replace("Controller_","",$controller_name);
		$controller_file = strtolower("controller_contr_aps_name_".$replaced."_by_b-a").'.php';
		$controller_path = "./controllers/".$controller_file;
		if(file_exists($controller_path))
		{
			if( !empty($_COOKIE['uid']) )
			{
				include "./controllers/".$controller_file;
			}
			else if($controller_name == "Controller_api")
			{
				$controller_name = 'Controller_api';
				include "./controllers/controller_contr_aps_name_api_by_b-a.php";
			}
			else
			{
				$controller_name = 'Controller_auth';
				$params = array();
				include "./controllers/controller_contr_aps_name_auth_by_b-a.php";
			}
		}
		else
		{
			echo "fewfwefew";
			Route::ErrorPage404();
		}
		
		$controller = new $controller_name;
		$action = $action_name;
		

		if( $controller_name == "api" )
		{
		    $controller -> $action( $params );
		}
		else
		{
			if( method_exists($controller, $action) && !empty($params[0]) )
			{
				$controller -> $action( $params );
			}
			else if( method_exists($controller, $action) )
			{
				$controller -> $action();
			}
			else
			{
				Route::ErrorPage404();
			}
		}
	}
	}
	
	function ErrorPage404()
	{
    $host = 'http://'.$_SERVER['HTTP_HOST'].'/';
    header('HTTP/1.1 404 Not Found');
		header("Status: 404 Not Found");
		header('Location:'.$host.'404');
    }
}
?>