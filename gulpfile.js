var gulp = require('gulp'),
concatCss = require('gulp-concat-css'),
cleanCSS = require('gulp-clean-css'),
rename = require("gulp-rename"),
uncss = require('gulp-uncss-sp'),
concat = require('gulp-concat'),
uglify = require('gulp-uglify'),
sass = require('gulp-sass');

gulp.task('sass', function(){
	return gulp.src('css/sass/**/*.scss')
	.pipe(sass({outputStyle:'expended'}).on('error',sass.logError))
	.pipe(gulp.dest('css'))
});

gulp.task('default', function(){
	gulp.watch('css/sass/**/*.scss',gulp.series('sass'));
});

//gulp.task('default', ['watch']);



gulp.task('js-concat', function() {
	return gulp.src('./js/*.js')
	.pipe(concat('all.js'))
    //.pipe(uglify())
    .pipe(gulp.dest('./dist/js/'));

  });

gulp.task('js-minify', function() {
	pump([
		gulp.src('./dist/js/*.js'),
		gulp.dest('./dist/js/min/')
		]
		);
});

gulp.task('css-concat', function () {
	return gulp.src('css/*.css')
	.pipe(concatCss("bundle.css"))
	.pipe(cleanCSS({compatibility: 'ie8'}))
	.pipe(rename("bundle.min.css"))
    /*.pipe(uncss({
            html: ['index.html']
          }))*/
          .pipe(gulp.dest('dist/style/'));
        });