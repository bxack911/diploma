<title><?php echo $data['my_info']['main_name']; ?></title>
<?php if($_COOKIE["priv"] == "admin"){ ?>
<div>
    <!--<?php if($data["my_info"]["sys_status"] == "active" && $data["my_info"]["sys_priv"] != "admin"){ ?>
        <p style='color: red; cursor: pointer;' class="admin_act" act="banned">Забанить</p>
        <input type="hidden" value="<?php echo $data["my_info"]['id']; ?>" />
    <?php }else if($data["my_info"]["sys_priv"] != "admin"){ ?>
        <p style='color: green; cursor: pointer;' class="admin_act" act="activate">Разблочить</p>
        <input type="hidden" value="<?php echo $data["my_info"]['id']; ?>" />
    <?php } ?>-->
</div>
<?php } ?>
<?php if(!empty($data['id'])){ ?>
<div id="my_page_user">
        <div id="my_page_user_top">
            <div>
                <img class="avatar_to_ch" src="http://localhost/img/<?php echo ($data['sys_avatar'] == '') ? 'no_photo.jpg' : $data['sys_avatar']; ?>" alt="<?php echo $data['my_info']['sys_avatar']; ?>" />
            </div>
            <div>
                <div>
                    <?php echo $data['main_name']; ?> <?php echo $data['main_lastname']; ?>
                </div>
                <div>
                    <i class="fa fa-map-marker"></i>
                    Ukraine
                </div>
                <div>
                    <i class="fab fa-instagram"></i>
                    <i class="fab fa-twitter"></i>
                    <i class="fab fa-facebook"></i>
                </div>
            </div>
        </div>
        <div id="my_page_user_bottom">
            <div id="my_page_user_bottom_nums">
                <ul>
                    <li>29</li>
                    <li>342</li>
                    <li>3K</li>
                    <li>291K</li>
                    <li>17</li>
                </ul>
            </div>
            <div id="my_page_user_bottom_menu">
                <ul>
                    <li>FOTOS</li>
                    <li>FOLLOWERS</li>
                    <li>FOLGE ICH</li>
                    <li>BEWERTUNGEN</li>
                    <li>COLLECTIONS</li>
                </ul>
            </div>
        </div>
    </div>
<?php }else{ ?>
    <div id="my_page_user">
        <div id="my_page_user_top">
            <div>
                <img class="avatar_to_ch" src="http://localhost/img/<?php echo ($data['my_info']['sys_avatar'] == '') ? 'no_photo.jpg' : $data['my_info']['sys_avatar']; ?>" alt="<?php echo $data['my_info']['sys_avatar']; ?>" />
            </div>
            <div>
                <div>
                    <?php echo $data['my_info']['main_name']; ?> <?php echo $data['my_info']['main_lastname']; ?>
                </div>
                <div>
                    <i class="fa fa-map-marker"></i>
                    Ukraine
                </div>
                <div>
                    <i class="fab fa-instagram"></i>
                    <i class="fab fa-twitter"></i>
                    <i class="fab fa-facebook"></i>
                </div>
            </div>
        </div>
        <div id="my_page_user_bottom">
            <div id="my_page_user_bottom_nums">
                <ul>
                    <li><?php echo $data['my_info']["wall_c"]; ?></li>
                    <li><?php echo $data['my_info']['followers_c']; ?></li>
                    <li><?php echo $data['my_info']['following_c']; ?></li>
                    <li>291K</li>
                    <li><?php echo $data['my_info']["shops_c"]; ?></li>
                </ul>
            </div>
            <div id="my_page_user_bottom_menu">
                <ul>
                    <li>POSTS</li>
                    <li>FOLLOWERS</li>
                    <li>FOLLOWING</li>
                    <li>BEWERTUNGEN</li>
                    <li>SHOPS</li>
                </ul>
            </div>
        </div>
        
    </div>
<?php } ?>
<div id="my_page_transition"></div>
<div id="my_page">        
    <div id="my_page_menu">
    <div id="my_page_menu_search">
        <i class="fas fa-search"></i>
        <input type="text" placeholder="Search...">
        <button>+</button>
    </div>
    <p id="my_page_wall_count">Finding <span><?php echo $data["wall_count"]; ?></span> records:</p>
        <ul>
            <?php for($i = 0; $i < $data["wall_count"]; $i++){ ?>
                <li>
                    <div><?php if($data[$i]["wall_back_type"] == "img"){ ?>
                            <img src="<?php echo __NAME__; ?>img/<?php echo $data[$i]["wall_back_set"]; ?>" />
                        <?php }else{ ?>
                            <img src="<?php echo __NAME__; ?>img/<?php echo ($data['my_info']['sys_avatar'] == '') ? 'no_photo.jpg' : $data['my_info']['sys_avatar']; ?>" alt="<?php echo $data['my_info']['sys_avatar']; ?>" />
                        <?php } ?>
                    </div>
                    <div>
                        <p><?php echo $data[$i]["wall_title"]; ?></p>
                        <p><?php echo $data[$i]["wall_type"]; ?></p>
                    </div>
                    <div>
                        <i class="fas fa-ellipsis-h"></i>
                        <p id="wall-<?php echo $i; ?>"><?php echo $data[$i]["wall_date"]; ?></p>
                    </div>
                </li>
                <input type="hidden" value="<?php echo $data[$i]["wall_id"]; ?>" />
            <?php } ?>
        </ul>
    </div>
    <div id="my_page_content">
        <div id="my_page_content_main_overlay"></div>
        <div id="my_page_content_main"
            <?php switch($data[0]["wall_back_type"]){ case 'anim': ?>
                style='
                    background: <?php echo $data[0]["wall_back_set"]; ?>;
                    background-size: 400% 400%;

                    -webkit-animation: AnimationName 59s ease infinite;
                    -moz-animation: AnimationName 59s ease infinite;
                    animation: AnimationName 59s ease infinite;
                '
            <?php break; case 'grad': ?>
                 style='background: <?php echo $data[0]["wall_back_set"]; ?>;'
            <?php break; case 'col': ?>
                style='background: #<?php echo $data[0]["wall_back_set"]; ?>;'
            <?php break; ?>
            <?php } ?>
        >
            <?php if($data[0]["wall_back_type"] == "img"){ ?>
                    <img src="../img/<?php echo $data[0]["wall_back_set"]; ?>" />
            <?php } ?>
        </div>
        <div id="my_page_content_main_cont">
            <?php if($data["wall_count"] > 0){ ?>
                <p><?php echo $data[0]["wall_title"]; ?></p>
                <p><?php echo $data[0]["wall_descr"]; ?></p>
            <?php } ?>
            <div id="my_page_content_main_cont_cards"></div>
        </div>
        <div id="my_page_content_us_info">
            <?php if($data["wall_count"] > 0){ ?>
                <img src="../img/<?php echo $data[0]['from_info']['avatar']; ?>" />
                <p><?php echo $data[0]['from_info']['name']; ?></p>
            <?php } ?>
        </div>
    </div>
    
</div>
