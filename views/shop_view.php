<title>Shop - <?php echo $data["shop"]["shop_name"] ?></title>

<input type="hidden" id="real_shop_id" value="<?php echo $data["shop"]["shop_id"]; ?>" />
<div id="shop_cont">
	<div id="shop_header">
		<div>
			<img src="http://localhost/img/shops_logo/<?php echo $data["shop"]["shop_logo"]; ?>" alt="<?php echo $data["shop"]["shop_logo"]; ?>">
			<h1><?php echo $data["shop"]["shop_name"] ?></h1>
		</div>
		<?php if($data["shop"]["podp"] == false){ ?>
			<i class="fas fa-cart-plus"></i>
			<label>Subscribe</label>
		<?php }else{ ?>
			<?php if($data["shop"]["shop_admin_id"] == $_COOKIE['uid']){ ?>
				<input type="hidden" value="admin" />
				<input type="hidden" value="<?php echo $data["shop"]["shop_id"]; ?>" />
				<i class="fas fa-trash-alt"></i>
				<label>Delete</label>
			<?php }else{ ?>
				<i class="fas fa-ban"></i>
				<label>Unsubscribe</label>
			<?php } ?>
		<?php } ?>
	</div>
	<div>Finding&nbsp;<b><?php echo $data["shop"]["prod_count"]; ?></b>&nbsp;products: <div id="add_product_but"><i class="fas fa-folder-plus"></i><button>Add product</button></div></div>
	<div id="shop_content">
		<div id="shop_content_left">
			<?php if($data["shop"]["prod_count"] > 0){ for($i = 0; $i < $data["shop"]["prod_count"]; $i++){ ?>
				<ul>
					<li><img src="http://localhost/img/products/<?php echo $data["shop"]["prod-$i"]["photo"]; ?>" alt="<?php echo $data["shop"]["prod-$i"]["photo"]; ?>"></li>
					<li><p><?php echo $data["shop"]["prod-$i"]["name"]; ?></p></li>
					<li><p><?php echo $data["shop"]["prod-$i"]["descr"]; ?></p></li>
					<div>
						<li><p><?php echo $data["shop"]["prod-$i"]["cost"]; ?> grn.</p></li>
						<li><i class="fas fa-cart-plus"></i><label>Buy</label></li>
						<input type="hidden" value="<?php echo $data["shop"]["prod-$i"]["id"]; ?>" />
						<input type="hidden" value="<?php echo $data["shop"]["prod-$i"]["cost"]; ?>" />
						<input type="hidden" value="<?php echo $data["shop"]["shop_id"]; ?>" />
					</div>
					<i class="fas fa-pencil-alt"></i>
					<input type="hidden" value="<?php echo $data["shop"]["prod-$i"]["id"]; ?>" />
					<input type="hidden" value="<?php echo $data["shop"]["prod-$i"]["name"]; ?>" />
					<input type="hidden" value="<?php echo $data["shop"]["prod-$i"]["cost"]; ?>" />
					<input type="hidden" value="<?php echo $data["shop"]["prod-$i"]["descr"]; ?>" />
				</ul>
			<?php }} ?>
		</div>
		<div id="shop_content_right">
			<div id="cats">
				<h5>Categories</h5>
				<ul>
					<?php if($data["shop"]['shop_admin_id'] == $_COOKIE['uid']){ ?>
						<li title="New category">
							<div>
								<i class="fas fa-plus"></i>
							</div>
							<div>
								<p>Add new</p>
								<p></p>
							</div>
						</li>
						<input type="hidden" value='<?php echo $data["shop"]["shop_id"]; ?>' />
					<?php } ?>
					<?php for($i = 0; $i < $data["cats"]["cats_count"]; $i++){ ?>
						<li class="cats" title="Категория <?php echo $data["cats"]["cat-$i"]["name"]; ?>">
							<div>
								<i class='<?php echo $data["cats"]["cat-$i"]["icon"]; ?>'></i>
							</div>
							<div>
								<p><?php echo $data["cats"]["cat-$i"]["name"]; ?></p>
								<p>0 products</p>
							</div>
							<div>
								<i class="fa fa-angle-down" toggled="false"></i>
							</div>
						</li>
						<div>
							<ul>
								<?php if($data["shop"]['shop_admin_id'] == $_COOKIE['uid']){ ?>
									<li title="New podcategory">
										<div>
											<i class="fas fa-plus"></i>
										</div>
										<div>
											<p>Add new</p>
											<p></p>
										</div>
									</li>
									<input type="hidden" value='<?php echo $data["cats"]["cat-$i"]["id"]; ?>' />
								<?php } ?>

								<?php for($j = 0; $j < $data["cats"]["cat-$i"]["podcats"]; $j++){ ?>
									<li title="Podcategory <?php echo $data["cats"]["cat-$i"]["podcat-$j"]["name"]; ?>">
										<div>
											<i class='<?php echo $data["cats"]["cat-$i"]["podcat-$j"]["icon"]; ?>'></i>
										</div>
										<div>
											<p><?php echo $data["cats"]["cat-$i"]["podcat-$j"]["name"]; ?></p>
											<p>0 products</p>
										</div>
									</li>
								<?php } ?>
							</ul>
						</div>
					<?php } ?>
				</ul>
			</div>
			<div>
				<h5>Subscribers</h5>
				<div>
					<?php for($i = 0; $i < $data["shop"]["users_count"]; $i++){ if($data["shop"]["user-$i"]['type'] == 'user'){ ?>
						<div>
							<div>
								<a href='https://localhost/my_page/user/<?php echo $data["shop"]["user-$i"]['id']; ?>'><img src='<?php echo __NAME__ ?>/img/<?php echo $data["shop"]["user-$i"]['sys_avatar']; ?>' /></a>
							</div>
							<div>
								<a href='https://localhost/my_page/user/<?php echo $data["shop"]["user-$i"]['id']; ?>'><?php echo $data["shop"]["user-$i"]['main_name']; ?> <?php echo $data["shop"]["user-$i"]['main_lastname']; ?></a>
								<div>Subscriber</div>
							</div>
						</div>
					<?php }} ?>
					</div>
			</div>
			<div>
				<h5>Administrators</h5>
				<div>
					<?php for($i = 0; $i < $data["shop"]["users_count"]; $i++){ if($data["shop"]["user-$i"]['type'] == 'admin'){ ?>
						<div>
							<div>
								<a href='https://localhost/my_page/user/<?php echo $data["shop"]["user-$i"]['id']; ?>'><img src='<?php echo __NAME__ ?>/img/<?php echo $data["shop"]["user-$i"]['sys_avatar']; ?>' /></a>
							</div>
							<div>
								<a href='https://localhost/my_page/user/<?php echo $data["shop"]["user-$i"]['id']; ?>'><?php echo $data["shop"]["user-$i"]['main_name']; ?> <?php echo $data["shop"]["user-$i"]['main_lastname']; ?></a>
								<div>Administrator</div>
							</div>
						</div>
					<?php }} ?>
					</div>
					<div>
					<?php for($i = 0; $i < $data["shop"]["users_count"]; $i++){ if($data["shop"]["user-$i"]['type'] == 'moderator'){ ?>
						<div>
							<div>
								<a href='https://localhost/my_page/user/<?php echo $data["shop"]["user-$i"]['id']; ?>'><img src='<?php echo __NAME__ ?>/img/<?php echo $data["shop"]["user-$i"]['sys_avatar']; ?>' /></a>
							</div>
							<div>
								<a href='https://localhost/my_page/user/<?php echo $data["shop"]["user-$i"]['id']; ?>'><?php echo $data["shop"]["user-$i"]['main_name']; ?> <?php echo $data["shop"]["user-$i"]['main_lastname']; ?></a>
								<div>Moderator</div>
							</div>
						</div>
					<?php }} ?>
				</div>
			</div>
		</div>
	</div>
</div>