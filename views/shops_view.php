<title>Магазины</title>
<div id="shops_cont">
	<div>
		<div class="group">      
	      <input type="text" required>
	      <span class="highlight"></span>
	      <span class="bar"></span>
	      <label>Что ищем...?</label>
	    </div>
	    <div>
	    	<i class="fas fa-hand-holding-usd"></i>
	    	<label>Start to earn</label>
	    </div>
	</div>
	<div id="shops_list">
		<?php for($i = $data["shops"]["count"]-1; $i >= 0; $i--){ ?>
			<ul>
					<li>
						<img src="<?php echo __NAME__; ?>img/shops_logo/<?php echo $data["shops"]["shop-$i"]["logo"]; ?>" alt="<?php echo $data["shops"]["shop-$i"]["logo"]; ?>">
					</li>
					<li>
						<a href="/shop/default/<?php echo $data["shops"]["shop-$i"]["id"]; ?>"><?php echo $data["shops"]["shop-$i"]["name"]; ?></a>
						<p><?php echo $data["shops"]["shop-$i"]["descr"]; ?></p>
						<div>
							<?php switch( $data["shops"]["shop-$i"]["spec"]){
									case 'Building': echo '<i class="fas fa-hammer"></i>'; break;
									case 'Electronic': echo '<i class="fas fa-laptop"></i>'; break;
									case 'Clothes': echo '<i class="fas fa-tshirt"></i>'; break;
								} ?>
							<label><?php echo $data["shops"]["shop-$i"]["spec"]; ?></label>
						</div>
						<div>
							<i class="fas fa-cart-arrow-down"></i>
							<label><?php echo $data["shops"]["shop-$i"]["prods"]; ?></label>
						</div>
						<div>
							<i class="fas fa-users"></i>
							<label>184</label>
						</div>
					</li>
					<li>
						<i class="fas fa-ellipsis-h shops_list_drop_but"></i>
						  <div class="shops_list_dropdown">
						    <ul>
						       <?php if($data["shops"]["shop-$i"]["admin"] == 2){ ?>
						       		<li><i class="fas fa-cog"></i> Administrate</li>
						       		<input type="hidden" value="admin" />
						       		<input type="hidden" value="<?php echo $data["shops"]["shop-$i"]["id"]; ?>" />
						       		<input type="hidden" value="<?php echo $data["shops"]["shop-$i"]["name"]; ?>" />
						       		<input type="hidden" value="<?php echo $data["shops"]["shop-$i"]["descr"]; ?>" />
						       		<li><i class="fas fa-trash-alt"></i> Delete</li>
						       <?php }else if($data["shops"]["shop-$i"]["admin"] == 0){ ?>
						       		<li><i class="fas fa-shopping-cart"></i> To shopping</li>
						       		<input type="hidden" value="user" />
						       		<input type="hidden" value="<?php echo $data["shops"]["shop-$i"]["id"]; ?>" />
						       		<li><i class="fas fa-user-times"></i> Unsubscribe</li>
						       <?php } ?>
						    </ul>
						  </div>
					</li>
			</ul>
		<?php } ?>
	</div>
</div>