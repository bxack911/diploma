<title>Діалоги</title>
<div id="dialogs_conteiner">
	<div id="dialogs_view">
		<div id="dialogs_view_search">
			<i class="fas fa-search"></i>
   	 		<input type="text" placeholder="Search...">
   	 		<button>+</button>
		</div>
		<?php for( $i = 0; $i < $data['dialogs']["counter"]; $i++ ){ ?>
		<ul>
			<li class="dialogs_list_my_ava">
				<a><img src="/img/<?php echo ($data['dialogs']['mes_avatar'][$i] == '') ? 'no_photo.jpg' : $data['dialogs']['mes_avatar'][$i]; ?>" /></a>
			</li>
			<li id="dialog_list_name">
				<a>
					<?php echo $data['dialogs']["mes_name"][$i]; ?> <?php echo $data['dialogs']["mes_lastname"][$i]; ?></a>
				 <a><?php echo $data['dialogs']["mes_text"][$i]; ?>
				 	<?php 
				 		$id = $data['dialogs']['dialog_id'][$i];
				 		if(!empty($data["dialog_notice"]["dialog_c-$id"]))
				 		{
				 			if($data["dialog_notice"]["dialog_c-$id"]["count"] > 0)
				 			{
				 				echo "<span style='float: right;'>".$data["dialog_notice"]["dialog_c-$id"]["count"]."</span>";
				 			}
				 		} 
				 	?>
				 </a>
			</li>
			<li>
				<i class="fas fa-ellipsis-h"></i>
				<div>
					<?php echo $data['dialogs']['mes_date'][$i]; ?>
				</div>
				<!--<div>
					<i class="fas fa-video call_to_friend" id='<?php echo $data['user_friends_id'][$i]; ?>'></i>
					<input type="hidden" value="<?php echo $data['user_friends_id'][$i]; ?>" />
					<input type="hidden" value="<?php echo $data['user_friends_name'][$i]; ?>" />
					<i class="fas fa-phone"></i>	
				</div>-->
			</li>
		</ul>
		<input type="hidden" value="<?php echo $data['dialogs']['dialog_id'][$i]; ?>"' />
		<?php } ?>
	</div>
	<div id="messages_view">
		<div>
			
		</div>
		<div>
			
		</div>
		<div id='input_message'>
			
		</div>
		<div id='messages_profile_m' class="">
			<div></div>
		</div>
	</div>
