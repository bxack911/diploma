<?php
class Model_api extends Model
{
	public function user_reg( $params_name, $params_value )
	{
		$name = $params_name[0];
		$lastname = $params_value[0];
 		$email = $params_name[1];
 		$password = $params_value[1];

 		    // check if user is already existed with the same email
    if (DB::getInstance()->isUserExisted($email)) {
        // user already existed
        $response["error"] = TRUE;
        $response["error_msg"] = "User already existed with " . $email;
        echo json_encode($response);
    } else {
        // create a new user
        $user = DB::getInstance()->storeUser($name, $lastname, $email, $password);
        if ($user) {
            // user stored successfully
            $response["error"] = FALSE;
            $response["uid"] = $user["unique_id"];
            $response["user"]["name"] = $user["main_name"];
            $response["user"]["main_lastname"] = $user["main_lastname"];
            $response["user"]["sys_email"] = $user["sys_email"];
            echo json_encode($response);
        } else {
            // user failed to store
            $response["error"] = TRUE;
            $response["error_msg"] = "Unknown error occurred in registration!";
            echo json_encode($response);
        }
    }
	}

	public function auth( $params_name, $params_value )
	{
		$email = $params_name[0];
	    $password = $params_value[0];
	 
	    // get the user by email and password
	    $user = DB::getInstance()->getUserByEmailAndPassword($email, $password);
	 
	    if ($user != false) {
	        // use is found
	        $response["error"] = FALSE;
	        $response["uid"] = $user["unique_id"];
	        $response["user"]["main_name"] = $user["main_name"];
	        $response["user"]["main_lastname"] = $user["main_lastname"];
	        $response["user"]["sys_email"] = $user["sys_email"];

	        setcookie("uid", $user["unique_id"], time()+60*60*24*365*10, "/");
			setcookie("status", $user["sys_status"], time()+60*60*24*365*10, "/");
			setcookie("priv", $user["sys_priv"], time()+60*60*24*365*10, "/");

	        echo json_encode($response);
	    } else {
	        // user is not found with the credentials
	        $response["error"] = TRUE;
	        $response["error_msg"] = "Login credentials are wrong. Please try again!";
	        echo json_encode($response);
	    }
	}

	public function search_wall( $params_name, $params_value )
	{
		$data = array();
		$data['html'] = "";

		$res = DB::getInstance() -> query("SELECT id,title,type,date,user_id FROM us_wall WHERE title LIKE '%".$params_value[0]."%' AND user_id = '".$params_name[0]."'");
		$res_counter = $res -> count();
		$counter = $res_counter;

		for($i = 0; $i < $res_counter; $i++)
		{
			$imgq = DB::getInstance() -> query("SELECT type,img_background FROM wall_settings WHERE wall_id = '".$res -> results()[$i] -> id."' LIMIT 1");
			$img = "";
			if($imgq -> results()[$counter] -> type == "img")
			{
				$imgc = $imgq -> results()[$counter] -> img_background;
			}
			else
			{
				$user = DB::getInstance() -> query("SELECT sys_avatar FROM us_main WHERE unique_id = '".$res -> results()[$i] -> user_id."'");
				$imgc = ($imgq -> results()[$counter+1] -> sys_avatar == '') ? 'no_photo.jpg' : $imgq -> results()[$res_counter+2] -> sys_avatar;
			}
			$counter++;

			$data['html'] .=	'<li>
                    <div>
                      <img src="'.__NAME__.'img/'.$imgc.'" alt="'.$imgc.'" />
                    </div>
                    <div>
                        <p>'.$res -> results()[$i] -> title.'</p>
                        <p>'.$res -> results()[$i] -> type.'</p>
                    </div>
                    <div>
                        <i class="fas fa-ellipsis-h"></i>
                        <p id="wall-'.$i.'">'.$res -> results()[$i] -> date.'</p>
                    </div>
                </li>
                <input type="hidden" value="'.$res -> results()[$i] -> id.'" />';
		}

		return json_encode($data);
	}

	public function add_wall( $params_name, $params_value )
	{
		DB::getInstance() -> no_returns("INSERT INTO us_wall(user_id, title, descr, from_id) VALUES(
				'".$params_name[0]."',
				'".urldecode($params_name[1])."',
				'".urldecode($params_value[1])."',
				'".$params_value[0]."'
			)");

		$wall_id = DB::getInstance() -> query("SELECT id FROM us_wall WHERE user_id='".$params_name[0]."'");

		echo $params_value[3];
		if(!empty($params_value[3]))
		{
			$cards = explode(',',$params_value[3]);
			$cards_count = count($cards);
			for($i = 0; $i < $cards_count; $i++)
			{
				DB::getInstance() -> no_returns("INSERT INTO wall_cards(card_id,type,wall_id) VALUES(
						'".$cards[$i]."',
						'prod',
						".$wall_id->results()[$wall_id->count()-1] -> id."
					)");
			}
		}

		switch($params_name[2])
		{
			case 'col':
				DB::getInstance() -> no_returns("INSERT INTO wall_settings(wall_id,gradient_anim,gradient_background,color_background,img_background,type,cards) VALUES(
					".$wall_id->results()[$wall_id->count()-1] -> id.",
					'',
					'',
					'".urldecode($params_value[2])."',
					'',
					'col',
					1
				)");
			break;
			case 'grad':
				DB::getInstance() -> no_returns("INSERT INTO wall_settings(wall_id,gradient_anim,gradient_background,color_background,img_background,type,cards) VALUES(
					".$wall_id->results()[$wall_id->count()-1] -> id.",
					'',
					'".urldecode($params_value[2])."',
					'',
					'',
					'grad',
					1
				)");
			break;
			case 'anim':
				DB::getInstance() -> no_returns("INSERT INTO wall_settings(wall_id,gradient_anim,gradient_background,color_background,img_background,type,cards) VALUES(
					".$wall_id->results()[$wall_id->count()-1] -> id.",
					'".urldecode($params_value[2])."',
					'',
					'',
					'',
					'col',
					1
				)");
			break;
		}
		
		
	}

	public function delete_wall( $params_name, $params_value )
	{
		DB::getInstance() -> no_returns("DELETE FROM us_wall WHERE id = ".$params_value[0]);
		DB::getInstance() -> no_returns("DELETE FROM wall_cards WHERE wall_id = ".$params_value[0]);
		DB::getInstance() -> no_returns("DELETE FROM wall_settings WHERE wall_id = ".$params_value[0]);
	}

	public function update_wall( $params_name, $params_value )
	{
		DB::getInstance() -> no_returns("UPDATE us_wall SET title = '".urldecode($params_name[1])."' WHERE id = ".$params_value[0]);
		DB::getInstance() -> no_returns("UPDATE us_wall SET descr = '".urldecode($params_value[1])."' WHERE id = ".$params_value[0]);
		DB::getInstance() -> no_returns("UPDATE wall_settings SET gradient_anim = '' WHERE wall_id = ".$params_value[0]);
		DB::getInstance() -> no_returns("UPDATE wall_settings SET gradient_background = '' WHERE wall_id = ".$params_value[0]);
		DB::getInstance() -> no_returns("UPDATE wall_settings SET color_background = '' WHERE wall_id = ".$params_value[0]);
		DB::getInstance() -> no_returns("UPDATE wall_settings SET img_background = '' WHERE wall_id = ".$params_value[0]);

		switch($params_name[2])
		{
			case 'col':
				DB::getInstance() -> no_returns("UPDATE wall_settings SET color_background = '".urldecode($params_value[2])."' WHERE wall_id = ".$params_value[0]);			
				DB::getInstance() -> no_returns("UPDATE wall_settings SET type = 'col' WHERE wall_id = ".$params_value[0]);
			break;
			case 'grad':
				DB::getInstance() -> no_returns("UPDATE wall_settings SET gradient_background = '".urldecode($params_value[2])."' WHERE wall_id = ".$params_value[0]);			
				DB::getInstance() -> no_returns("UPDATE wall_settings SET type = 'grad' WHERE wall_id = ".$params_value[0]);
			break;
			case 'anim':
				DB::getInstance() -> no_returns("UPDATE wall_settings SET gradient_anim = '".urldecode($params_value[2])."' WHERE wall_id = ".$params_value[0]);			
				DB::getInstance() -> no_returns("UPDATE wall_settings SET type = 'anim' WHERE wall_id = ".$params_value[0]);
			break;
			case 'img':
				DB::getInstance() -> no_returns("UPDATE wall_settings SET img_background = '".urldecode($params_value[2])."' WHERE wall_id = ".$params_value[0]);			
				DB::getInstance() -> no_returns("UPDATE wall_settings SET type = 'img' WHERE wall_id = ".$params_value[0]);
			break;
		}

		$cards = explode(",",$params_value[3]);

		DB::getInstance() -> no_returns("DELETE FROM wall_cards WHERE wall_id = ".$params_value[0]);
		for($i = 0; $i < count($cards); $i++)
		{
			DB::getInstance() -> no_returns("INSERT INTO wall_cards(card_id,type,wall_id) VALUES(
					".$cards[$i].",
					'prod',
					".$params_value[0]."
				)");
		}
	}

	public function show_wall( $params_name, $params_value )
	{
		$data = array();
		$update = array();
		$dt = 0;
		if($params_name[0] == "update"){$dt = 1;}else{$dt = 0;}

		$data['cards'] = 0;
		$info = DB::getInstance() -> query("SELECT * FROM us_wall WHERE id = '".$params_value[0]."'");
		$user = DB::getInstance() -> query("SELECT sys_avatar, main_name FROM us_main WHERE unique_id = '".$info -> results()[0] -> from_id."'");

		if($dt == 0){$data['content'] = "<p>".$info -> results()[0] -> title."</p>
                           													<p>".$info -> results()[0] -> descr."</p>
                           													<div id='my_page_content_main_cont_cards'></div>";
    }
    else
    {
    	$update["wall_id"] = $params_value[0];
    	$update["content"]["title"] = $info -> results()[0] -> title;
    	$update["content"]["descr"] = $info -> results()[0] -> descr;
    }

        $img = "";
        if($user -> results()[1] -> sys_avatar == "") $img = "no_photo.jpg";
        else $img = $user -> results()[1] -> sys_avatar;
        $data["us_info"] = "<img src='../img/".$img."' />
                            <p>".$user -> results()[1] -> main_name."</p>";


        $wall_settings = DB::getInstance() -> query("SELECT * FROM wall_settings WHERE wall_id = ".$info -> results()[0] -> id." LIMIT 1");                   
        $data["type"] = $wall_settings -> results()[2] -> type;
        $update["type"] = $wall_settings -> results()[2] -> type;
        $data["setting"] = "";
        $update["setting"] = "";
        switch($data["type"])
        {
        	case 'anim':
        		if($dt == 0){$data["setting"] = $wall_settings -> results()[2] -> gradient_anim;}
        		else{$update["setting"] = $wall_settings -> results()[2] -> gradient_anim;}
        	break;
        	case 'grad':
        		if($dt == 0){$data["setting"] = $wall_settings -> results()[2] -> gradient_background;}
        		else{$update["setting"] = $wall_settings -> results()[2] -> gradient_background;}
        	break;
        	case 'col':
        		if($dt == 0){$data["setting"] = $wall_settings -> results()[2] -> color_background;}
        		else{$update["setting"] = $wall_settings -> results()[2] -> color_background;}
        	break;
        	case 'img':
        		if($dt == 0){$data["setting"] = "<img src='../img/".$wall_settings -> results()[2] -> img_background."' />";}
        		else{$update["setting"] = "<img src='../img/".$wall_settings -> results()[2] -> img_background."' />";}
        	break;
        }


        if($wall_settings -> results()[2] -> cards == 1)
        {
        	$data['cards'] = 1;
        	$update['cards'] = 1;
        	$html = "";
        	$wall_cards = DB::getInstance() -> query("SELECT * FROM wall_cards WHERE wall_id = ".$params_value[0]); 
        	$wall_cards_count = $wall_cards -> count();
        	$counter = $wall_cards_count+3;
        	$wall_cards_counter = 3;

        	for($i = 0; $i < $wall_cards_count; $i++)
        	{
        		if($wall_cards -> results()[$wall_cards_counter] -> type == "prod")
        		{
        			$prods = DB::getInstance() -> query('SELECT sh_product.*,us_shops.name AS "sshop" FROM sh_product INNER JOIN us_shops ON us_shops.id = sh_product.shop_id WHERE sh_product.id = '.$wall_cards -> results()[$wall_cards_counter] -> card_id);      			

        			if($dt == 1)
        			{
	        				$update['html'] .= "
									<div class='modal_add_wall_preview_cont_cards_wall_cards'>
										<div><img src='http://localhost/img/star.png' alt='star.png not found' /></div>									
										<div>
											<img src='http://localhost/img/products/".strtolower($prods -> results()[$counter] -> sshop)."/".$prods -> results()[$counter] -> photo."' alt='".$prods -> results()[$counter] -> photo." not found' title='".$prods -> results()[$counter] -> photo."' />
										</div>
										<div>
											<p>".$prods -> results()[$counter] -> name."</p>
										</div>
										<div>
											".$prods -> results()[$counter] -> cost." grn.
										</div>
										<div>
											<a href='".$prods -> results()[$counter] -> shop_id."'>My!!!</a>
										</div>
										<div></div>
									</div>
	        			";
        			}
        			else
        			{
	        			$data['html'] .= "
									<div class='my_page_wall_cards'>
										<div><img src='http://localhost/img/star.png' alt='star.png not found' /></div>									
										<div>
											<img src='http://localhost/img/products/".strtolower($prods -> results()[$counter] -> sshop)."/".$prods -> results()[$counter] -> photo."' alt='".$prods -> results()[$counter] -> photo." not found' title='".$prods -> results()[$counter] -> photo."' />
										</div>
										<div>
											<p>".$prods -> results()[$counter] -> name."</p>
										</div>
										<div>
											".$prods -> results()[$counter] -> cost." grn.
										</div>
										<div>
											<a href='".$prods -> results()[$counter] -> shop_id."'>My!!!</a>
										</div>
										<div></div>
									</div>
	        			";
        			}
        			$counter++;
        		}
        		else
        		{
        			$prods = DB::getInstance() -> query('SELECT * FROM us_shops WHERE id = '.$wall_cards -> results()[$wall_cards_counter] -> card_id);
       		
        			if($dt == 1)
        			{
        				$update['html'] .= "
									<div class='modal_add_wall_preview_cont_cards_wall_cards'>
										<div><img src='http://localhost/img/star.png' alt='star.png not found' /></div>									
										<div>
											<img src='http://localhost/img/shops_logo/".$prods -> results()[$counter] -> logo."' alt='".$prods -> results()[$counter] -> logo." not found' title='".$prods -> results()[$counter] -> logo."' />
										</div>
										<div>
											<p>".$prods -> results()[$counter] -> name."</p>
										</div>
										<div>
											".$prods -> results()[$counter] -> descr."
										</div>
										<div>
											<a href='".$prods -> results()[$counter] -> id."'>My!!!</a>
										</div>
										<div></div>
									</div>
	        			";
        			}
        			else
        			{
	        			$data['html'] .= "
									<div class='my_page_wall_cards'>
										<div><img src='http://localhost/img/star.png' alt='star.png not found' /></div>									
										<div>
											<img src='http://localhost/img/shops_logo/".$prods -> results()[$counter] -> logo."' alt='".$prods -> results()[$counter] -> logo." not found' title='".$prods -> results()[$counter] -> logo."' />
										</div>
										<div>
											<p>".$prods -> results()[$counter] -> name."</p>
										</div>
										<div>
											".$prods -> results()[$counter] -> descr."
										</div>
										<div>
											<a href='".$prods -> results()[$counter] -> id."'>My!!!</a>
										</div>
										<div></div>
									</div>
	        			";
	        		}
        			$counter++;
        		}
        		$wall_cards_counter++;
        	}
        }
        

		/*$imgs = DB::getInstance() -> query("SELECT * FROM wall_imgs WHERE wall_id = '".$info -> results()[0] -> id."'");
		$imgs_count = $imgs -> count();

		$data["main_img"] = "<img src='../img/".$imgs -> results()[2] -> name."' />";
		if($imgs_count > 1)
		{
			for($i = 0; $i < $imgs_count; $i++)
			{
				$data["img-$i"] = $imgs -> results()[$i+3] -> name;
			}
		}*/

		if($params_name[0] == "update")
		{
			return json_encode($update);
		}
		else
		{
			return json_encode($data);
		}
	}

	public function get_shop_cards( $params_name, $params_value )
	{
		$html = '';
		$html_u = '';

		if($params_name[0] == "get")
		{
			$res = multi_thread_request(array(__NAME__."api/get_shop_prod/?id=".$params_value[0]));
			$products = json_decode($res[__NAME__."api/get_shop_prod/?id=".$params_value[0]],true);

			$html .= '<button id="modal_add_wall_cards_back">Back</button>';
			$html_u .= '<button id="modal_update_wall_cards_back">Back</button>';

			for($i = 0; $i < $products["prod_count"]; $i++){

				$html .= '<div id="modal_add_wall_cards_product">
						<div>
							<img src="'.__NAME__.'img/products'.$products["prod-$i"]["photo"].'" alt="'.$products["prod-$i"]["photo"].'">
						</div>
						<div>
							<p>'.$products["prod-$i"]["name"].'</p>
							<p>'.$products["prod-$i"]["cost"].' grn.</p>
						</div>
						<input type="hidden" value="'.$products["prod-$i"]["id"].'" />
					</div>';

				$html_u .= '<div id="modal_update_wall_cards_product">
						<div>
							<img src="'.__NAME__.'img/products'.$products["prod-$i"]["photo"].'" alt="'.$products["prod-$i"]["photo"].'">
						</div>
						<div>
							<p>'.$products["prod-$i"]["name"].'</p>
							<p>'.$products["prod-$i"]["cost"].' grn.</p>
						</div>
						<input type="hidden" value="'.$products["prod-$i"]["id"].'" />
					</div>';
			}
		}
		else
		{
			$prods = DB::getInstance() -> query('SELECT sh_product.*,us_shops.name AS "sshop" FROM sh_product INNER JOIN us_shops ON us_shops.id = sh_product.shop_id WHERE sh_product.id = '.$params_value[0]);

			$html .= "
								<div class='modal_add_wall_preview_cont_cards_wall_cards'>
									<div><img src='http://localhost/img/star.png' alt='star.png not found' /></div>									
									<div>
										<img src='http://localhost/img/products/".strtolower($prods -> results()[0] -> sshop)."/".$prods -> results()[0] -> photo."' alt='".$prods -> results()[0] -> photo." not found' title='".$prods -> results()[$counter] -> photo."' />
									</div>
									<div>
										<p>".$prods -> results()[0] -> name."</p>
									</div>
									<div>
										".$prods -> results()[0] -> cost." grn.
									</div>
									<div>
										<a href='".$prods -> results()[0] -> shop_id."'>My!!!</a>
									</div>
									<div></div>
								</div>
        			";
      $html_u .= "
								<div class='modal_update_wall_preview_cont_cards_wall_cards'>
									<div><img src='http://localhost/img/star.png' alt='star.png not found' /></div>									
									<div>
										<img src='http://localhost/img/products/".strtolower($prods -> results()[0] -> sshop)."/".$prods -> results()[0] -> photo."' alt='".$prods -> results()[0] -> photo." not found' title='".$prods -> results()[$counter] -> photo."' />
									</div>
									<div>
										<p>".$prods -> results()[0] -> name."</p>
									</div>
									<div>
										".$prods -> results()[0] -> cost." grn.
									</div>
									<div>
										<a href='".$prods -> results()[0] -> shop_id."'>My!!!</a>
									</div>
									<div></div>
								</div>
        			";
		}
		if($params_value[1] == "add")
		{
			return $html;
		}
		else
		{
			return $html_u;
		}
	}

	public function add_message( $params_name, $params_value )
	{
		date_default_timezone_set("Europe/Kiev");
		DB::getInstance() -> no_returns("INSERT INTO us_messages (from_id, dialog_id, text, date) VALUES(
				'".$params_value[1]."',
				".$params_name[0].",
				'".base64_encode(htmlspecialchars(urldecode($params_value[0])))."',
				'".date('Y-m-d')."'
			)");
	}

	public function add_friend( $params_name, $params_value )
	{
		DB::getInstance() -> no_returns("INSERT INTO followers(user1,user2) VALUES(
				'".$params_name[0]."',
				'".$params_value[0]."'
			)");
	}

	public function delete_friend( $params_name, $params_value )
	{
		DB::getInstance() -> no_returns("DELETE FROM followers WHERE user1 = '".$params_name[0]."' AND user2 = '".$params_value[0]."'");
	}

	public function get_user_info( $params_name, $params_value )
	{
		require_once './include/api/get_user_info.php';
		echo json_encode($data);
	}

	public function search_users( $params_name, $params_value )
	{
		$users = DB::getInstance() -> query("SELECT * FROM us_main WHERE main_name LIKE '%".$params_value[0]."%' OR main_lastname LIKE '%".$params_value[0]."%'");

		$count = $users->count();

		$data = array();

		for($i = 0; $i < $count; $i++)
		{
			$data["user-$i"]["id"] = $users -> results()[$i] -> unique_id;
			$data["user-$i"]["main_name"] = $users -> results()[$i] -> main_name;
			$data["user-$i"]["main_lastname"] = $users -> results()[$i] -> main_lastname;
			$data["user-$i"]["sys_avatar"] = $users -> results()[$i] -> sys_avatar;
		}

		$data['count'] = $count;

		return json_encode($data);
	}

	public function add_black_list( $params_name, $params_value )
	{
		DB::getInstance() -> no_returns("INSERT INTO black_list(user1,user2) VALUES(
				'".$params_name[0]."',
				'".$params_value[0]."'
			)");
	}

	public function remove_black_list( $params_name, $params_value )
	{
		DB::getInstance() -> no_returns("DELETE FROM black_list WHERE user1 = '".$params_name[0]."' AND user2 = '".$params_value[0]."'");
	}

	public function get_user_dialogs( $params_name, $params_value )
	{
		$dialogs = DB::getInstance() -> query("SELECT * FROM us_dialogs");

		$dialogs_count = $dialogs -> count();

		for( $i = 0; $i < $dialogs_count; $i++ ) $users[$i] = explode(";", $dialogs -> results()[$i] -> users);

		$data = array(
					   "dialog_id" => array(),
					   "mes_name" => array(),
					   "mes_lastname" => array(),
					   "mes_avatar" => array(),
					   "mes_date" => array(),
					   "counter" => 0
					  );
		$index = 0;
		$dat_ind = $dialogs_count;
		for( $i = 0; $i < $dialogs_count; $i++ )
		{
			for( $z = 0; $z < count($users[$i]); $z++ )
			if( $users[$i][$z] == $params_value[0] )
			{
				if( $z == 0 )
				{
					$oth_us = DB::getInstance() -> query("SELECT id, main_name, main_lastname, sys_avatar FROM us_main WHERE unique_id = '".$users[$i][1]."'");
						$data["mes_name"][$index] = $oth_us -> results()[$dat_ind] -> main_name;
						$data["mes_lastname"][$index] = $oth_us -> results()[$dat_ind] -> main_lastname;
						$data["mes_id"][$index] = $oth_us -> results()[$dat_ind] -> id;
						$data["mes_avatar"][$index] = $oth_us -> results()[$dat_ind] -> sys_avatar;
						$dat_ind++;
						$oth_mes = DB::getInstance() -> query("SELECT * FROM us_messages WHERE dialog_id = ".$dialogs -> results()[$i] -> id." ORDER BY id DESC");
						$data["mes_text"][$index] = base64_decode($oth_mes -> results()[$dat_ind] -> text);
						$data["mes_date"][$index] = $oth_mes -> results()[$dat_ind] -> date;
						$dat_ind += $oth_mes -> count();
				}
				else if( $z == 1 )
				{
					$oth_us = DB::getInstance() -> query("SELECT id, main_name, main_lastname, sys_avatar FROM us_main WHERE unique_id = '".$users[$i][0]."'");
						$data["mes_name"][$index] = $oth_us -> results()[$dat_ind] -> main_name;
						$data["mes_lastname"][$index] = $oth_us -> results()[$dat_ind] -> main_lastname;
						$data["mes_avatar"][$index] = $oth_us -> results()[$dat_ind] -> sys_avatar;
						$data["mes_id"][$index] = $oth_us -> results()[$dat_ind] -> id;
						$dat_ind ++;

						$oth_mes = DB::getInstance() -> query("SELECT * FROM us_messages WHERE dialog_id = ".$dialogs -> results()[$i] -> id." ORDER BY id DESC");
						$data["mes_text"][$index] = base64_decode($oth_mes -> results()[$dat_ind] -> text);
						$data["mes_date"][$index] = $oth_mes -> results()[$dat_ind] -> date;
						$dat_ind += $oth_mes -> count();
				}
				else
				{
						$oth_us = DB::getInstance() -> query("SELECT id, main_name, main_lastname, sys_avatar FROM us_main WHERE unique_id = '".$users[$i][$z]."'");
						$data["mes_name"][$index] = $oth_us -> results()[$dat_ind] -> main_name;
						$data["mes_lastname"][$index] = $oth_us -> results()[$dat_ind] -> main_lastname;
						$data["mes_avatar"][$index] = $oth_us -> results()[$dat_ind] -> sys_avatar;
						$data["mes_id"][$index] = $oth_us -> results()[$dat_ind] -> id;
						$dat_ind ++;

						$oth_mes = DB::getInstance() -> query("SELECT * FROM us_messages WHERE dialog_id = ".$dialogs -> results()[$i] -> id." ORDER BY id DESC");
						$data["mes_text"][$index] = base64_decode($oth_mes -> results()[$dat_ind] -> text);
						$data["mes_date"][$index] = $oth_mes -> results()[$dat_ind] -> date;
						$dat_ind += $oth_mes -> count();
				}

				$data["dialog_id"][$index] = $dialogs -> results()[$i] -> id;
				$index++;
			}
		}

		$data["counter"] = $index;
		echo json_encode($data);
	}

	public function get_messages( $params_name, $params_value )
	{
		$mes = DB::getInstance() -> query("SELECT * FROM us_messages WHERE dialog_id = ".$params_value[0]);

		$data = array(
					  "id" => array(),	
					  "dialog_id" => $params_value[0],
					  "text" => array(),
					  "date" => array(),
					  "name" => array(),
					  "avatar" => array(),
					  "from" => array(),
					  "counter" => 0
					 );

		$mes_counter = $mes -> count();

		$is_user = false;
		for( $i = 0; $i < $mes_counter; $i++ )
		{
			if($mes -> results()[$i] -> from_id == $params_name[0])
			{
				$is_user = true;
			}
		}

		$html = "";
		$div_top = "";
		$mes_div = "";
		$input_div = "";
		$profile_m = "";

			for( $i = 0; $i < $mes_counter; $i++ )
			{
				$user = DB::getInstance() -> query( "SELECT unique_id, id, main_name, main_lastname, main_status, sys_avatar FROM us_main WHERE unique_id = '".$mes -> results()[$i] -> from_id."'" );



				$data["text"][$i] = nl2br(smile(base64_decode(htmlspecialchars($mes -> results()[$i] -> text))));
				$data["date"][$i] = $mes -> results()[$i] -> date;
				$data["from_id"][$i] = $mes -> results()[$i] -> from_id;
				$data['id'][$i] = $user -> results()[$mes_counter+$i] -> id;
				$data['unique_id'][$i] = $user -> results()[$mes_counter+$i] -> unique_id;
				$data["name"][$i] = $user -> results()[$mes_counter+$i] -> main_name;
				$data["lastname"][$i] = $user -> results()[$mes_counter+$i] -> main_lastname;
				$data["status"][$i] = $user -> results()[$mes_counter+$i] -> main_status;
				$data["avatar"][$i] = $user -> results()[$mes_counter+$i] -> sys_avatar;
			}

			$img_prof = ($data["avatar"][0] == '') ? 'no_photo.jpg' : $data["avatar"][0];

			$div_top .= "<div>
							<p>".$data["name"][0]." ".$data["lastname"][0]."</p>
							<p>".$data["status"][0]."</p>
						</div>
						<div>
							<i class='fa fa-phone'></i>
							<i class='fa fa-video'></i>
						</div>";
					$mes_div.="<ul>";
					for( $i = 0; $i < $mes_counter; $i++ )
					{
						$img = ($data["avatar"][$i] == '') ? 'no_photo.jpg' : $data["avatar"][$i];
						if($mes -> results()[$i] -> from_id == $params_name[0])
						{
							$mes_div.="<li class='my_message'>
									<div><img src='http://localhost/img/".$img."' /></div>
									<div>".$data["text"][$i]."</div>
									<div></div>
								</li>";
						}
						else
						{
							$mes_div.="<li class='friend_message'>
									<div></div>
									<div>".$data["text"][$i]."</div>
									<div><img src='http://localhost/img/".$img."' /></div>
								</li>";
						}
					}
					$mes_div.="</ul></div>";
					$input_div.="
							<input id='new_message_inp' type='text' placeholer='Write message...' />
							<input type='hidden' value='".$params_value[0]."' />
							<input type='hidden' value='".$data["unique_id"][0]."' />
							<div>
								<i class='far fa-smile'></i>
								<i class='fas fa-paperclip'></i>
								<i id='new_mes_but' class='fas fa-chevron-circle-up'></i>
								<input type='hidden' value='".$params_value[0]."' />
								<input type='hidden' value='".$img."' />
							</div>
					";
					$profile_m.="<p>&#8595;</p>
						<img src='".__NAME__."/img/".$img_prof."' />
						<p>".$data["name"][0]." ".$data["lastname"][0]."</p>
						<p>".$data["status"][0]."</p>
						<p>Wall of ".$data["name"][0]." ".$data["lastname"][0]."<p>
						<ul class='profile_m_ul_tag'>
					";

						$wall_counter = $mes_counter*2;
						$wall = DB::getInstance() -> query("SELECT * FROM us_wall WHERE user_id = '".$mes -> results()[0] -> from_id."'");
						$wall_c = $wall -> count();
						$imgs_counter = 0;

						for($i = 0; $i < $wall_c; $i++)
						{
							$imgs = DB::getInstance() -> query("SELECT * FROM wall_settings WHERE wall_id = ".$wall -> results()[$wall_counter+$i] -> id);
							$imgscc = $imgs -> count();
							switch($imgs->results()[$wall_counter+$wall_c+$imgs_counter] -> type)
							{
								case 'col':
									if($imgs->results()[$wall_counter+$wall_c+$imgs_counter] -> color_background == "")
									{
											$profile_m.="<li>
												<div class='profile_m_ul_overlay'></div>
												<p>".$wall -> results()[$wall_counter+$i] -> title."</p>
												";
									}
									else
									{
											$profile_m.="<li style='background: ".$imgs->results()[$wall_counter+$wall_c+$imgs_counter] -> color_background."'>
												<div class='profile_m_ul_overlay'></div>
												<p>".$wall -> results()[$wall_counter+$i] -> title."</p>
												";
									}
								break;
								case 'grad':
									$profile_m.="<li style='background: ".$imgs->results()[$wall_counter+$wall_c+$imgs_counter] -> gradient_background."'>
											<div class='profile_m_ul_overlay'></div>
											<p>".$wall -> results()[$wall_counter+$i] -> title."</p>
									";
								break;
								case 'anim':
									$profile_m.="<li style='background: ".$imgs->results()[$wall_counter+$wall_c+$imgs_counter] -> gradient_anim."'>
											<div class='profile_m_ul_overlay'></div>
											<p>".$wall -> results()[$wall_counter+$i] -> title."</p>
									";
								break;
								case 'img':
								break;
							}
							$imgs_counter+=$imgscc;

							
							
							/*if($imgscc == 1)
							{
								$profile_m.="<div class='profile_m_img'>";
									$profile_m.="<img src='../img/".$imgs->results()[$wall_counter+$wall_c+$imgs_counter] -> name."' />";
								$profile_m.="</div>";
							}
							else
							{
								$profile_m.="<div class='profile_m_imgs'>";
								for($j = 0; $j < $imgscc; $j++)
								{
									$profile_m.="<img src='../img/".$imgs->results()[$wall_counter+$wall_c+$j+$imgs_counter] -> name."' />";
								}
								$profile_m.="</div>
										</li>";
							}*/
						}
					$profile_m.="</ul>";
						
						
			$data["div_top"] = $div_top;
			$data["mes_div"] = $mes_div;
			$data["input_div"] = $input_div;
			$data["profile_m"] = $profile_m;
			$data["counter"] = $mes_counter;

		return json_encode($data);
	}

	public function search_messages( $params_name, $params_value )
	{
		
	}

	public function search_dialogs( $params_name, $params_value )
	{
		$data = array();
		$dialogs = DB::getInstance() -> query("SELECT * FROM us_dialogs WHERE users LIKE '%".$params_name[0]."%'");
		$dialogs_count = $dialogs -> count();
		$counter = $dialogs_count;
		$html = '<div id="dialogs_view_search">
									<i class="fas fa-search"></i>
						   	 		<input type="text" placeholder="Search...">
						   	 		<button>+</button>
								</div>';

		for($i = 0; $i < $dialogs_count; $i++)
		{
			$dialog_exp = explode(";",$dialogs -> results()[$i] -> users);
			if($dialog_exp[1] == $params_name[0])
			{
				$user = DB::getInstance() -> query("SELECT * FROM us_main WHERE unique_id = '".$dialog_exp[0]."' AND(main_name LIKE '%".$params_value[0]."%' OR main_lastname LIKE '%".$params_value[0]."%')");

				if($user -> count() > 0)
				{
					$last_mes = DB::getInstance() -> query("SELECT text,date FROM us_messages WHERE dialog_id = ".$dialogs -> results()[$i] -> id." ORDER BY id DESC LIMIT 1");

					$data[$i] = $dialogs -> results()[$i] -> id;

					$avatar = ($user->results()[$counter] -> sys_avatar == '') ? 'no_photo.jpg' : $user->results()[$counter] -> sys_avatar;
					$html.='<ul>
										<li class="dialogs_list_my_ava">
											<a><img src="/img/'.$avatar.'" /></a>
										</li>
										<li id="dialog_list_name">
											<a>
												'.$user->results()[$counter] -> main_name.' '.$user->results()[$counter] -> main_lastname.'</a>
											 <a>'.nl2br(smile(base64_decode(htmlspecialchars($last_mes -> results()[$counter+1] -> text)))).'</a>
										</li>
										<li>
											<i class="fas fa-ellipsis-h"></i>
											<div>
												'.$last_mes -> results()[$counter+1] -> date.'
											</div>
										</li>
									</ul>
									<input type="hidden" value="'.$dialogs -> results()[$i] -> id.'" />';
					$counter+=2;
				}
				/*else
				{
					$mes = DB::getInstance() -> query("SELECT * FROM us_messages WHERE dialog_id = ".$dialogs -> results()[$i] -> id);
					$mes_counter = $mes -> count();

					for($j = 0; $j < $mes_counter; $j++)
					{
						//echo nl2br(smile(base64_decode(htmlspecialchars($mes -> results()[$counter+$j] -> text))));
						if(count(explode($params_value[0],(nl2br(smile(base64_decode(htmlspecialchars($mes -> results()[$counter+$j] -> text))))))) > 1)
						{
							
						}
					}
					$counter+=$mes_counter;
				}*/
			}
		}

		return $html;
	}

	public function get_search( $params_name, $params_value )
	{
		$html = "";
		
		require_once './include/api/get_search.php';

		return $html;
	}

	public function create_dialog( $params_name, $params_value )
	{
		date_default_timezone_set("Europe/Kiev");

		$us_dialogs_c = DB::getInstance() -> query("SELECT id,users FROM us_dialogs WHERE users LIKE'%".$params_name[0]."%' AND users LIKE'%".$params_value[0]."%'");


		if($us_dialogs_c -> count() == 0)
		{
			DB::getInstance() -> no_returns("INSERT INTO us_dialogs(users)
				VALUES(
					'".$params_name[0].";".$params_value[0]."'
				)");

			$id = DB::getInstance() -> query("SELECT id FROM us_dialogs");

			DB::getInstance() -> no_returns("INSERT INTO us_messages(from_id,dialog_id,text,date)
				VALUES(
					'".$params_name[0]."',
					".$id -> results()[$id->count()-1] -> id.",
					'".base64_encode(htmlspecialchars(urldecode($params_value[1])))."',
					'".date('Y-m-d')."'
				)");

				return $id -> results()[$id->count()-1] -> id;
		}
		else
		{
			DB::getInstance() -> no_returns("INSERT INTO us_messages(from_id,dialog_id,text,date)
				VALUES(
					'".$params_name[0]."',
					".$us_dialogs_c -> results()[0] -> id.",
					'".base64_encode(htmlspecialchars(urldecode($params_value[1])))."',
					'".date('Y-m-d')."'
				)");

			return $params_name[0];
		}

	}

	public function get_user_shops( $params_name, $params_value )
	{
		$shops = DB::getInstance() -> query("SELECT * FROM shops_relations WHERE user_id = '".$params_value[0]."'");
		$shops_count = $shops -> count();

		$data = array();

		$data["count"] = $shops_count;
		$counter = $shops_count;

		for($i = 0; $i < $shops_count; $i++)
		{
			$shops = DB::getInstance() -> query("SELECT * FROM us_shops WHERE id = ".$shops->results()[$i] -> shop_id." ORDER BY id DESC");
			
			$data["shop-$i"]["id"] = $shops->results()[$counter]->id;
			if($shops->results()[$i]->type == "admin"){$data["shop-$i"]["admin"] = 2;}
			else if($shops->results()[$i]->type == "moder"){$data["shop-$i"]["admin"] = 1;}
			else{$data["shop-$i"]["admin"] = 0;}
			$data["shop-$i"]["name"] = $shops->results()[$counter]->name;
			$data["shop-$i"]["logo"] = $shops->results()[$counter]->logo;
			$data["shop-$i"]["descr"] = $shops->results()[$counter]->descr;
			$data["shop-$i"]["spec"] = $shops->results()[$counter]->spec;
			$prod_count = DB::getInstance() -> query("SELECT COUNT(*) AS count FROM sh_product WHERE shop_id = ".$shops->results()[$counter]->id);
			
			$counter++;
			if($prod_count->results()[$counter]->count > 0)
			{
				$data["shop-$i"]["prods"] = $prod_count->results()[$counter]->count;
				$counter++;
			}
			else
			{
				$data["shop-$i"]["prods"] = 0;
			}
		}
		return json_encode($data);
	}

	public function delete_user_shop( $params_name, $params_value )
	{
		DB::getInstance() -> query("DELETE FROM shops_relations WHERE user_id = '".$params_name[0]."' AND shop_id = ".$params_value[0]);
	}

	public function add_user_shop( $params_name, $params_value )
	{
		DB::getInstance() -> query("INSERT INTO shops_relations(user_id,shop_id,type)VALUES(
				'".$params_name[0]."',
				".$params_value[0].",
				'user'
			)");
	}

	public function add_shop( $params_name, $params_value )
	{
		$valid = DB::getInstance() -> query("SELECT id FROM us_shops WHERE name = '".htmlspecialchars(urldecode($params_value[0]))."'");
		
		if($valid -> count() == 0)
		{
			DB::getInstance() -> no_returns("INSERT INTO us_shops(name,logo,descr)VALUES(
					'".htmlspecialchars(urldecode($params_value[0]))."',
					'".htmlspecialchars($params_name[1])."',
					'".htmlspecialchars(urldecode($params_value[1]))."'
				)");

			$id_query = DB::getInstance() -> query("SELECT id FROM us_shops WHERE logo='".htmlspecialchars($params_name[1])."' ORDER BY id DESC LIMIT 1");

			DB::getInstance() -> query("INSERT INTO shops_relations(user_id,shop_id,type)VALUES(
				'".$params_name[0]."',
				".$id_query -> results()[0] -> id.",
				'admin'
			)");

			return $id;
		}
		else
		{
			return "name!";
		}
	}

	public function delete_shop( $params_name, $params_value )
	{
		$shop = DB::getInstance() -> query("SELECT name FROM us_shops WHERE id = ".$params_value[0]);

    rmdir(__NAME__."/img/products/".$shop->results()[0] -> name);

		DB::getInstance() -> no_returns("DELETE FROM us_shops WHERE id = ".$params_value[0]);
		DB::getInstance() -> no_returns("DELETE FROM shops_relations WHERE shop_id = ".$params_value[0]);
		DB::getInstance() -> no_returns("DELETE FROM sh_product WHERE shop_id = ".$params_value[0]);


		return json_encode(__NAME__."/img/products/".$shop->results()[0] -> name);
	}

	public function update_shop( $params_name, $params_value )
	{
		echo "UPDATE us_shops SET name = '".$params_value[0]."' WHERE id = ".$params_name[0];
		DB::getInstance() -> no_returns("UPDATE us_shops SET name = '".urldecode($params_value[0])."' WHERE id = ".$params_name[0]);
		DB::getInstance() -> no_returns("UPDATE us_shops SET descr = '".urldecode($params_name[1])."' WHERE id = ".$params_name[0]);
	}

	public function search_shops( $params_name, $params_value )
	{
		$data = array();
		$data['html'] = "";
		$shops = DB::getInstance() -> query("SELECT * FROM us_shops WHERE name LIKE '%".htmlspecialchars(urldecode($params_value[0]))."%' LIMIT 15");

		for($i =0; $i < $shops->count(); $i++){
			$data["html"].="<ul>
					<li>
						<img src='http://localhost/img/shops_logo/".$shops->results()[$i] -> logo."' alt='".$shops->results()[$i] -> logo."'>
					</li>
					<li>
						<a href='/shop/default/".$shops->results()[$i] -> id."'>".$shops->results()[$i] -> name."</a>
						<p>".$shops->results()[$i] -> descr."</p>
					</li>
					<li>
						<i class='fas fa-ellipsis-h shops_list_drop_but'></i>
						  <div class='shops_list_dropdown'>
						    <ul>";
						       		$data["html"].="<li>Администрировать</li>
						       		<input type='hidden' value='admin' />
						       		<input type='hidden' value='".$shops->results()[$i] -> id."' />
						       		<li>Удалить</li>";
								$data["html"].="</ul>
										  </div>
									</li>
								</ul>";
				}

				return json_encode($data);
	}

	public function get_shop_prod( $params_name, $params_value )
	{
		$data = array();

		$shop_info = DB::getInstance() -> query("SELECT * FROM us_shops WHERE id = ".$params_value[0]);

		$data["shop_id"] = $shop_info->results()[0] -> id;
		$data["shop_name"] = $shop_info->results()[0] -> name;
		$data["shop_logo"] = $shop_info->results()[0] -> logo;
		$data["shop_descr"] = $shop_info->results()[0] -> descr;
		$data["shop_spec"] = $shop_info->results()[0] -> spec;

		$products = DB::getInstance() -> query("SELECT * FROM sh_product WHERE shop_id = ".$params_value[0]);
		$prod_count = $products->count();
		$data["prod_count"] = $prod_count;

		if($prod_count > 0)
		{
			for($i = 0; $i < $prod_count; $i++)
			{
				$data["prod-$i"]["id"] = $products->results()[$i+1] -> id;
				$data["prod-$i"]["name"] = $products->results()[$i+1] -> name;
				if(strlen($products->results()[$i+1] -> descr) < 250)
				{
					$data["prod-$i"]["descr"] = $products->results()[$i+1] -> descr;
				}
				else
				{
					$data["prod-$i"]["descr"] = mb_strimwidth($products->results()[$i+1] -> descr,0,250);
				}
				$data["prod-$i"]["cost"] = $products->results()[$i+1] -> cost;
				//$data["prod-$i"]["photo"] = str2url($shop_info->results()[0] -> name)."/".$products->results()[$i+1] -> photo;
				$data["prod-$i"]["photo"] = "/".$shop_info->results()[0] -> name."/".$products->results()[$i+1] -> photo;
			}
		}
		else
		{
			$data['error'] = "Null prod res";
		}

		$users = DB::getInstance() -> query("SELECT * FROM shops_relations WHERE shop_id = ".$params_value[0]);
		$users_count = $users -> count();
		$data["users_count"] = $users_count;
		$counter = $prod_count+$users_count+1;

		for($i = 0; $i < $users_count; $i++)
		{
			$data["user-$i"]["type"] = $users -> results()[$i+1+$prod_count] -> type;

			$user = DB::getInstance() -> query("SELECT unique_id, main_name,main_lastname,sys_avatar FROM us_main WHERE unique_id = '".$users -> results()[$i+1+$prod_count] -> user_id."'");

			$data["user-$i"]["id"] = $user -> results()[$counter] -> unique_id;
			$data["user-$i"]["main_name"] = $user -> results()[$counter] -> main_name;
			$data["user-$i"]["main_lastname"] = $user -> results()[$counter] -> main_lastname;
			if($user -> results()[$counter] -> sys_avatar == '')
			{
				$data["user-$i"]["sys_avatar"] = "no_photo.jpg";
			}
			else
			{
				$data["user-$i"]["sys_avatar"] = $user -> results()[$counter] -> sys_avatar;
			}
			$counter++;
		}

		$us = DB::getInstance() -> query("SELECT id FROM shops_relations WHERE user_id='".$params_name[0]."' AND shop_id = ".$params_value[0]);
		if($us -> count() > 0)
		{
			$data["podp"] = true;
		}
		else
		{
			$data["podp"] = false;
		}

		return json_encode($data);
	}

	public function add_shop_prod( $params_name, $params_value )
	{
		DB::getInstance() -> no_returns("INSERT INTO sh_product(name,descr,cost,photo,shop_id)VALUES(
				'".htmlspecialchars(urldecode($params_name[0]))."',
				'".htmlspecialchars(urldecode($params_value[0]))."',
				'".htmlspecialchars(urldecode($params_name[1]))."',
				'".htmlspecialchars(urldecode($params_value[1]))."',
				".$params_value[2]."
			)");

		return "OK!";
	}

	public function delete_shop_prod( $params_name, $params_value )
	{
		DB::getInstance() -> no_returns("DELETE FROM sh_product WHERE id = ".$params_value[0]);
	}

	public function update_shop_prod( $params_name, $params_value )
	{
		DB::getInstance() -> no_returns("UPDATE sh_product SET name = '".urldecode($params_value[0])."' WHERE id = ".$params_name[0]);
		DB::getInstance() -> no_returns("UPDATE sh_product SET cost = '".urldecode($params_name[1])."' WHERE id = ".$params_name[0]);
		DB::getInstance() -> no_returns("UPDATE sh_product SET descr = '".urldecode($params_value[1])."' WHERE id = ".$params_name[0]);
	}

	public function get_cats( $params_name, $params_value )
	{
		$data = array();
		$cats = DB::getInstance() -> query("SELECT * FROM sh_cats WHERE shop_id = ".$params_value[0]);
		$cats_count = $cats->count();
		$data['cats_count'] = $cats_count;
		$counter = 0;

		for($i = 0; $i < $cats_count; $i++)
		{
			$data["cat-$i"]["id"] = $cats -> results()[$i] -> id;
			$data["cat-$i"]["name"] = $cats -> results()[$i] -> name;
			$data["cat-$i"]["icon"] = $cats -> results()[$i] -> icon;
			$data["cat-$i"]["shop_id"] = $cats -> results()[$i] -> shop_id;

			$podcat = DB::getInstance() -> query("SELECT * FROM sh_podcats WHERE cat_id = ".$cats->results()[$i] -> id);
			$podcat_count = $podcat->count();
			$data["cat-$i"]["podcats"] = $podcat_count;

			for($j = 0; $j < $podcat_count; $j++)
			{
				$data["cat-$i"]["podcat-$j"]["id"] = $podcat->results()[$j+$cats_count+$counter] -> id;
				$data["cat-$i"]["podcat-$j"]["name"] = $podcat->results()[$j+$cats_count+$counter] -> name;
				$data["cat-$i"]["podcat-$j"]["icon"] = $podcat->results()[$j+$cats_count+$counter] -> icon;
				$data["cat-$i"]["podcat-$j"]["cat"] = $podcat->results()[$j+$cats_count+$counter] -> cat_id;
			}
			$counter+=$podcat_count;
		}

		return json_encode($data);
	}

	public function add_cat( $params_name, $params_value )
	{
		if($params_name[0] == "cat")
		{
			DB::getInstance()->no_returns("INSERT INTO sh_cats(name,icon,shop_id)VALUES(
					'".urldecode($params_name[1])."',
					'".urldecode($params_value[1])."',
					".$params_value[0]."
				)");
			return $params_value[0]."|".$params_name[1]."|".$params_value[1];
		}
		else
		{
			DB::getInstance()->no_returns("INSERT INTO sh_podcats(name,icon,cat_id)VALUES(
					'".urldecode($params_name[1])."',
					'".urldecode($params_value[1])."',
					".$params_value[0]."
				)");
			return $params_value[0]."|".$params_name[1]."|".$params_value[1];
		}
	}

	public function update_cat( $params_name, $params_value )
	{

	}

	public function delete_cat( $params_name, $params_value )
	{

	}

	public function get_cart( $params_name, $params_value )
	{
		$cart = DB::getInstance() -> query("SELECT * FROM cartung WHERE user_id = '".$params_value[0]."'");

		$products = DB::getInstance() -> query("SELECT * FROM cart_prod WHERE cart_id = ".$cart -> results()[0] -> id);

		$prods_count = $products -> count();
		$data["cartung"]["prod_count"] = $prods_count;

		if($cart -> count() > 0)
		{
			$data["cartung"]["id"] = $cart -> results()[0] -> id;
			$data["cartung"]["count"] = $products -> results()[0] -> count;
			$data["cartung"]["sum"] = $products -> results()[0] -> cost;
		}
		else
		{
			$data["cartung"]["id"] = $cart -> results()[0] -> id;
			$data["cartung"]["count"] = 0;
			$data["cartung"]["sum"] = 0;
		}

		$counter = $prods_count+1;

		for($i = 0; $i < $prods_count; $i++)
		{
			$product = DB::getInstance() -> query("SELECT id, name, cost, photo, shop_id FROM sh_product WHERE id = ".$products -> results()[$i+1] -> prod_id);
			
			$data["prod-$i"]["sum"] = $product -> results()[$i+1] -> sum;
			$data["prod-$i"]["count"] = $product -> results()[$i+1] -> count;
			$data["prod-$i"]["id"] = $product -> results()[$counter] -> id;
			$data["prod-$i"]["name"] = $product -> results()[$counter] -> name;
			$data["prod-$i"]["cost"] = $product -> results()[$counter] -> cost;
			$data["prod-$i"]["shop_id"] = $product -> results()[$counter] -> shop_id;
			
			$counter++;
			$shop_name = DB::getInstance() -> query("SELECT id,name FROM us_shops WHERE id = ".$product -> results()[$counter-1] -> shop_id);
			//$data["prod-$i"]["photo"] = strtolower($product -> results()[$counter] -> name)."/".$product -> results()[$counter-1] -> photo;
			
			$data["prod-$i"]["photo"] = strtolower("Magaz")."/".$product -> results()[$counter-1] -> photo;
			$counter++;
		}

		return json_encode($data);
	}

	public function delete_cart( $params_name, $params_value )
	{
		echo $params_value[0];
		DB::getInstance() -> no_returns("DELETE FROM cart_prod WHERE cart_id = ".$params_value[0]);

		$order = DB::getInstance() -> query("SELECT order_id FROM cartung WHERE id = ".$params_value[0]);

		DB::getInstance() -> no_returns("DELETE FROM cartung WHERE id = ".$params_value[0]);
		DB::getInstance() -> no_returns("DELETE FROM orders WHERE id = ".$order -> results()[0] -> order_id);
	}

	public function plus_cart( $params_name, $params_value )
	{
		$cartung = DB::getInstance() -> query("SELECT * FROM cartung WHERE user_id = '".$params_name[0]."'");
		$counter = 0;

		if($cartung -> count() > 0)
		{
			$counter++;
			$prod = DB::getInstance() -> query("SELECT * FROM cart_prod WHERE prod_id = '".$params_value[0]."'");
			if($prod -> count() > 0)
			{
				$counter++;
				$count = intval($cartung -> results()[0] -> count)+1;
				$sum = intval($cartung -> results()[0] -> cost)+intval($params_name[1]);
				$prod_sum = intval($prod-> results()[1] -> sum)+intval($params_name[1]);
				echo $prod_sum;
				$prod_count = intval($prod -> results()[1] -> count)+1;

				DB::getInstance() -> no_returns("UPDATE cart_prod SET sum = '".$prod_sum."' WHERE id = ".$prod-> results()[1] -> id);
				DB::getInstance() -> no_returns("UPDATE cart_prod SET count = ".$prod_count." WHERE id = ".$prod-> results()[1] -> id);
				DB::getInstance() -> no_returns("UPDATE cartung SET count = ".$count." WHERE id = ".$cartung -> results()[0] -> id);
				DB::getInstance() -> no_returns("UPDATE cartung SET cost = '".$sum."' WHERE id = ".$cartung -> results()[0] -> id);
				DB::getInstance() -> no_returns("UPDATE orders SET sum = '".$sum."' WHERE id = ".$cartung -> results()[0] -> order_id);
			}
			else
			{
				DB::getInstance() -> no_returns("INSERT INTO cart_prod(prod_id,shop_id,cart_id,count,sum)VALUES(
					".$params_value[0].",
					".$params_value[1].",
					'".$cartung -> results()[0] -> id."',
					1,
					'".$params_name[1]."'
				)");
				$counter++;
				$count = intval($cartung -> results()[0] -> count)+1;
				$sum = intval($cartung -> results()[0] -> cost)+intval($params_name[1]);
				DB::getInstance() -> no_returns("UPDATE cartung SET count = ".$count." WHERE id = ".$cartung -> results()[0] -> id);
				DB::getInstance() -> no_returns("UPDATE cartung SET cost = '".$sum."' WHERE id = ".$cartung -> results()[0] -> id);
				DB::getInstance() -> no_returns("UPDATE orders SET sum = '".$sum."' WHERE id = ".$cartung -> results()[0] -> order_id);
			}
		}
		else
		{
			DB::getInstance() -> no_returns("INSERT INTO orders(sum,from_id,date)VALUES(
					'".$params_name[1]."',
					'".$params_name[0]."',
					'".date('yyyy-mm-dd')."'
			)");
			$last_order = DB::getInstance() -> query("SELECT id FROM orders WHERE from_id = '".$params_name[0]."' ORDER BY id DESC LIMIT 1");

			echo $last_order -> results()[$counter+1] -> id;

			echo "INSERT INTO cartung(cost,count,user_id,order_id)VALUES(
					'".$params_name[1]."',
					1,
					'".$params_name[0]."',
					".$last_order -> results()[$counter] -> id."
			)";

			DB::getInstance() -> no_returns("INSERT INTO cartung(cost,count,user_id,order_id)VALUES(
					'".$params_name[1]."',
					1,
					'".$params_name[0]."',
					".$last_order -> results()[$counter] -> id."
			)");
			$counter++;

			$last_cart = DB::getInstance() -> query("SELECT id FROM cartung WHERE user_id = '".$params_name[0]."'");	

			DB::getInstance() -> no_returns("INSERT INTO cart_prod(prod_id,shop_id,cart_id,count,sum)VALUES(
					".$params_value[0].",
					".$params_value[1].",
					'".$last_cart -> results()[$counter] -> id."',
					1,
					'".$params_name[1]."'
			)");
			$counter++;
		}
	}

	public function minus_cart( $params_name, $params_value )
	{
		$cartung = DB::getInstance() -> query("SELECT * FROM cartung WHERE user_id = '".$params_name[0]."'");

		$prod = DB::getInstance() -> query("SELECT * FROM cart_prod WHERE prod_id = '".$params_value[0]."'");

			$count = intval($cartung -> results()[0] -> count)-1;
			$sum = intval($cartung -> results()[0] -> cost)-intval($params_name[1]);
			$prod_sum = intval($prod-> results()[1] -> sum)-intval($params_name[1]);
			echo $prod_sum;
			$prod_count = intval($prod -> results()[1] -> count)-1;

			DB::getInstance() -> no_returns("UPDATE cart_prod SET sum = '".$prod_sum."' WHERE id = ".$prod-> results()[1] -> id);
			DB::getInstance() -> no_returns("UPDATE cart_prod SET count = ".$prod_count." WHERE id = ".$prod-> results()[1] -> id);
			DB::getInstance() -> no_returns("UPDATE cartung SET count = ".$count." WHERE id = ".$cartung -> results()[0] -> id);
			DB::getInstance() -> no_returns("UPDATE cartung SET cost = '".$sum."' WHERE id = ".$cartung -> results()[0] -> id);
			DB::getInstance() -> no_returns("UPDATE orders SET sum = '".$sum."' WHERE id = ".$cartung -> results()[0] -> order_id);
	}

	public function pay_order( $params_name, $params_value )
	{
		$id = DB::getInstance() -> query("SELECT order_id,cost FROM cartung WHERE id = ".$params_name[0]);

		DB::getInstance() -> no_returns("UPDATE orders SET name = '".urldecode($params_name[1])."' WHERE id = ".$id->results()[0] -> order_id);
		DB::getInstance() -> no_returns("UPDATE orders SET city = '".urldecode($params_value[1])."' WHERE id = ".$id->results()[0] -> order_id);
		DB::getInstance() -> no_returns("UPDATE orders SET pay_type = '".$params_value[0]."' WHERE id = ".$id->results()[0] -> order_id);
		DB::getInstance() -> no_returns("UPDATE orders SET moder = 1 WHERE id = ".$id->results()[0] -> order_id);
	
		DB::getInstance() -> no_returns("DELETE FROM cartung WHERE id = ".$params_name[0]);
		DB::getInstance() -> no_returns("DELETE FROM cart_prod WHERE cart_id = ".$params_name[0]);

		return $id->results()[0] -> order_id;
	}
}
?>