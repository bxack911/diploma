$(document).ready(function(){

var host = "http://localhost/";
	$("#auth_cont input").focus(function(){
		$(this).prev().animate({
			marginTop: "-=3vh",
		},400);

		$(this).prev().css("color","#000");


		$(this).next().animate({
			width: "+=29.5vw",
			marginLeft: "-=14.75vw"
		},400);
	});

	$("#auth_cont input").focusout(function(){
		$(this).prev().animate({
			marginTop: "+=3vh",
		},400);

		$(this).prev().css("color","#42420C");

		$(this).next().animate({
			width: "-=29.5vw",
			marginLeft: "+=14.75vw"
		},400);
	});

	$("#to_reg_but").click(function(){
		$("#auth_cont").animate({
			opacity: "0",
			marginTop: "+=10vh"
		},500);

		$("#auth_cont").css("z-index",'0');

		$("#reg_cont").animate({
			opacity: "1",
			marginTop: "-=10vh"
		},500);

		$("#reg_cont").css("z-index",'2');
	});


	$("#reg_cont input").focus(function(){
		$(this).prev().animate({
			marginTop: "-=3vh",
		},400);

		$(this).prev().css("color","#000");


		$(this).next().animate({
			width: "+=29.5vw",
			marginLeft: "-=14.75vw"
		},400);
	});

	$("#reg_cont input").focusout(function(){
		$(this).prev().animate({
			marginTop: "+=3vh",
		},400);

		$(this).prev().css("color","#42420C");

		$(this).next().animate({
			width: "-=29.5vw",
			marginLeft: "+=14.75vw"
		},400);
	});

	$("#to_auth_but").click(function(){
		$("#auth_cont").animate({
			opacity: "1",
			marginTop: "-=10vh"
		},500);

		$("#auth_cont").css("z-index",'2');

		$("#reg_cont").animate({
			opacity: "0",
			marginTop: "+=10vh"
		},500);

		$("#reg_cont").css("z-index",'0');
	});

	$("#reg_but").click(function(){
		var name_surname = $("#reg_name").val();
		var name = '';
		var surname = '';
		var email = $("#reg_email").val();
		var pass = $("#reg_pass").val();
		var rep_pass = $("#reg_pass_rep").val();
		var to_ajax = 1;

		if(name_surname == '')
		{
			$("#reg_name").css("border-bottom","2px solid red");
			to_ajax = 0;
		}
		else
		{
			var exp = name_surname.split(' ');
			name = exp[0];
			if(exp[1] == undefined)
			{
				surname = '';
			}
			else
			{
				surname = exp[1];
			}
			$("#reg_name").css("border-bottom","2px solid green");
		}

		if(email == '')
		{
			$("#reg_email").css("border-bottom","2px solid red");
			to_ajax = 0;
		}
		else
		{
			$("#reg_email").css("border-bottom","2px solid green");
		}

		if(pass.length < 8 || pass.length > 60)
		{
			$("#reg_pass").css("border-bottom","2px solid red");
			to_ajax = 0;
		}
		else
		{
			$("#reg_pass").css("border-bottom","2px solid green");
		}

		if(pass != rep_pass)
		{
			$("#reg_pass_rep").css("border-bottom","2px solid red");
			to_ajax = 0;
		}
		else
		{
			$("#reg_pass_rep").css("border-bottom","2px solid green");
		}

		if(to_ajax == 1)
		{
			alert(host+"api/user_reg/?"+name+"="+surname+"&"+email+"="+pass);
			$.ajax({
				type: "POST",
				url: host+"api/user_reg/?"+name+"="+surname+"&"+email+"="+pass,
				dataType: "html",
				cache: false,
				async: false,
				success: function(data){
					var dat = JSON.parse(data);
					if(dat.error == true)
					{
						alert(dat.error_msg);
					}
					else
					{
						window.location.href="/";
					}
				}
			});
		}
		else
		{
			to_ajax = 1;
		}
	});

	$("#go_but").click(function(){
		var email = $("#auth_login").val();
		var pass = $("#auth_pass").val();

		$.ajax({
			type: "POST",
			url: host+"api/auth/?"+email+"="+pass,
			dataType: "html",
			cache: false,
			async: false,
			success: function(data){
				var dat = JSON.parse(data);
				if(dat.error == true)
				{
					alert(dat.error_msg);
				}
				else
				{
					window.location.href="/";
				}
			}
		});
	});
});