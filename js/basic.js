//Відео-чат
/*var config = 
{
    apiKey: "AIzaSyA7m139DR91sbHinG8_QzVE8WgO-yAD7Qc",
    authDomain: "snnsh-5e384.firebaseapp.com",
    databaseURL: "https://snnsh-5e384.firebaseio.com",
    projectId: "snnsh-5e384",
    storageBucket: "",
    messagingSenderId: "493174809361"
};
firebase.initializeApp(config);

var yourVideo = document.getElementById("yourVideo");
var friendsVideo = document.getElementById("friendsVideo");
var myUsername;
var targetUsername;
var servers = {'iceServers': [{'urls': 'stun:stun.services.mozilla.com'}, {'urls': 'stun:stun.l.google.com:19302'}, {'urls': 'turn:numb.viagenie.ca','credential': 'beaver','username': 'webrtc.websitebeaver@gmail.com'}]};
var pc = new RTCPeerConnection(servers);
var database = firebase.database().ref();
var userTarget;

//function connect() 
//{
    database = firebase.database().ref();
    yourVideo = document.getElementById("yourVideo");
    friendsVideo = document.getElementById("friendsVideo");
    myUsername = $("#user_id").val();
    userTarget = targetUsername;
    //Create an account on Viagenie (http://numb.viagenie.ca/), and replace {'urls': 'turn:numb.viagenie.ca','credential': 'websitebeaver','username': 'websitebeaver@email.com'} with the information from your account
    servers = {'iceServers': [{'urls': 'stun:stun.services.mozilla.com'}, {'urls': 'stun:stun.l.google.com:19302'}, {'urls': 'turn:numb.viagenie.ca','credential': 'beaver','username': 'webrtc.websitebeaver@gmail.com'}]};
    pc = new RTCPeerConnection(servers);
    pc.onicecandidate = (event => event.candidate?sendToServer({type: "new-ice-candidate", target: userTarget, ice: event.candidate}):console.log("Sent All Ice") );
    pc.onaddstream = (event => friendsVideo.srcObject = event.stream);
	
	database.on('child_added', readMessage);
    
    function readMessage(evt) {
        var jssPARSE = JSON.parse(evt.A.B);
        console.dir(jssPARSE);
        //var type = jssPARSE.type;
        //var sender = jssPARSE.name;
        //var target = jssPARSE.target;
        //var ice = jssPARSE.ice;
        //var sdp = jssPARSE.sdp;
        console.log(jssPARSE.type);
        console.log(jssPARSE.name);
        console.log(jssPARSE.target);
        //console.log(jssPARSE.ice);
        //console.log(jssPARSE.sdp);
        if (jssPARSE.target == myUsername && jssPARSE.type == "call") 
        {
            //alert("Vam zvonit " + jssPARSE.name);
            //console.log("Vam zvonit " + jssPARSE.name);
            document.getElementById(sounds.rington).play();
            alertMsg(jssPARSE);
        }
        if (jssPARSE.name != myUsername) {
            if (jssPARSE.ice != undefined){
                pc.addIceCandidate(new RTCIceCandidate(jssPARSE.ice));
            }
            else if (jssPARSE.type == "video-offer"){
                pc.setRemoteDescription(new RTCSessionDescription(jssPARSE.sdp))
                  .then(() => pc.createAnswer())
                  .then(answer => pc.setLocalDescription(answer))
                  .then(() => sendToServer({name: myUsername, type: "video-answer", target: userTarget, sdp: pc.localDescription}));
            }
            else if (jssPARSE.type == "video-answer"){
                showFriendsFace();
                document.getElementById(sounds.calling).pause();
                pc.setRemoteDescription(new RTCSessionDescription(jssPARSE.sdp));
            }
            else if (jssPARSE.type == "hang-up"){
            	closeVideoCall();
            }
            else if (jssPARSE.type == "call-To-Off"){
            	alert(jssPARSE.name + " отклонил ваш вызов");
            }
            else if (jssPARSE.type == "callon"){
            	showFriendsFace();
            }
   		}
};
//}


function alertMsg(jssPARSE) 
{
	var answer = confirm("Вам звонит " + jssPARSE.name + <br /> + "Желаете ответить на звонок?");
  if (answer) 
	{
    document.getElementById(sounds.rington).pause();
		$("#overlay").css("display","block");
		$("#video_call_modal").css("display","block");
		showFace();
	}
	else
	{
    document.getElementById(sounds.rington).pause();
    document.getElementById(sounds.end).play();
	sendToServer({name: myUsername, type: "call-To-Off", target: userTarget});
	}
}


function sendToServer(massage) 
{
    var massageJSON = JSON.stringify(massage);
    console.log("Sending '" + massage.type + "' message: " + massageJSON);
    var msg = database.push(massageJSON);
    msg.remove();
}

var sounds = {
            'calling': 'callingSignal',
            'end': 'endCallSignal',
            'rington': 'ringtoneSignal'
        };

//database.on('child_added', readMessage);
function showFace() {
	userTarget = targetUsername;
	sendToServer({type: "callon"});
  	navigator.mediaDevices.getUserMedia({audio:true, video:true})
    .then(stream => yourVideo.srcObject = stream)
    .then(stream => pc.addStream(stream));
}
function showMyFace() {
	userTarget = targetUsername;
  	showFriendsFace();
  	document.getElementById(sounds.calling).play();
	sendToServer({name: myUsername, type: "call", target: userTarget});
  	navigator.mediaDevices.getUserMedia({audio:true, video:true})
    .then(stream => yourVideo.srcObject = stream)
    .then(stream => pc.addStream(stream));
}

function showFriendsFace() {
  pc.createOffer()
    .then(offer => pc.setLocalDescription(offer) )
    .then(() => sendToServer({name: myUsername, type: "video-offer", target: userTarget, sdp: pc.localDescription}));
}

function hangUpCall() 
{
  closeVideoCall();
  sendToServer({
    name: myUsername,
    type: "hang-up",
    target: userTarget
  });
}

function closeVideoCall() 
{
  var remoteVideo = document.getElementById("friendsVideo");
  var localVideo = document.getElementById("yourVideo");

  console.log("Closing the call");

  if (pc) 
  {
  	document.getElementById(sounds.calling).pause();
  	document.getElementById(sounds.end).play();
  	console.log("--> Closing the peer connection");

    pc.onaddstream = null;
    pc.ontrack = null;
    pc.onremovestream = null;
    pc.onnicecandidate = null;
    pc.oniceconnectionstatechange = null;
    pc.onsignalingstatechange = null;
    pc.onicegatheringstatechange = null;
    pc.onnotificationneeded = null;

    if (remoteVideo.srcObject) 
    {
      remoteVideo.srcObject.getTracks().forEach(track => track.stop());
    }

    if (localVideo.srcObject) 
    {
      localVideo.srcObject.getTracks().forEach(track => track.stop());
    }

    remoteVideo.src = null;
    localVideo.src = null;

    pc.close();
    pc = null;

    $(".modal_window").css("display","none");
    $("#overlay").css("display","none");

    document.getElementById(sounds.calling).load();
  }
}
*/

//Створення ajax
var host = "http://localhost";
function create_ajax(url,success)
{
	$.ajax({
		type: "POST",
		url: url,
		dataType: "html",
		asynch: true,
		cache: false,
		success: function(data)
		{
			success(data);
		},
		error: function(data)
		{
			alert("ERROR: "+data);
		}	
	});
}

function create_ajax_array(url,success,data)
{
	$.ajax({
		url: url,
		data: data,
		cache: false,
		contentType: false,
		processData: false,
		type: 'POST',
		success: function(data){
			success(data);
		},
		error: function(data){
			alert("Error: "+data);
		}
	});
}

$(document).ready(function(){
	/*
		Formate dates to valid type
	*/
	moment().format();
	//alert(moment("2019-03-12 00:00:00").fromNow());



	$("#messages_row").scrollTop($("#messages_row").prop('scrollHeight'));

	$("#menu_logout > i").click(function(){
		function success(data)
		{
			window.location.reload();
		}
		create_ajax("/progs/logout.php",success);
	});

	$("#my_page_menu_search > input").keyup(function(event){
		var id = $("#user_id").val();
		var text = $(this).val();
		if (event.keyCode == 13) {
			function success(data)
			{
				var dat = JSON.parse(data);
				$("#my_page_menu > ul").html(dat.html);
			}
			create_ajax(host+"/api/search_wall/?"+id+"="+text,success);
		}
	});

	$("body").on('click','#my_page_menu > ul > li',function(){
		var id = $(this).next().val();

		function success(data)
		{
			var dat = JSON.parse(data);

      $("#my_page_content_main_overlay,#my_page_content_main").animate({
      	width: "0vw",
      	marginLeft: "+=36.1vw"
      },{
      	duration: 200,
      	complete: function()
      	{
      		switch(dat.type)
		      {
		      	case 'anim':
		      		$("#my_page_content_main").html("");
		      		document.getElementById('my_page_content_main').style.background = dat.setting;
		      		$("#my_page_content_main").css({'background-size': '400% 400%', '-webkit-animation': 'AnimationName 59s ease infinite', 'animation': 'AnimationName 59s ease infinite', '-moz-animation': 'AnimationName 59s ease infinite'})
		      	break;
		      	case 'grad':
		      		document.getElementById('my_page_content_main').style.background = 'transparent';
		      		$("#my_page_content_main").css({'background-size': '', '-webkit-animation': '', 'animation': '', '-moz-animation': ''})
		      		document.getElementById('my_page_content_main').style.background = dat.setting;
		      		$("#my_page_content_main").html("");
		      	break;
		      	case 'col':
		      		document.getElementById('my_page_content_main').style.background = 'transparent';
		      		$("#my_page_content_main").css({'background-size': '', '-webkit-animation': '', 'animation': '', '-moz-animation': ''})
		      		document.getElementById('my_page_content_main').style.background = dat.setting;
		      		$("#my_page_content_main").html("");
		      	break;
		      	case 'img':
		      		document.getElementById('my_page_content_main').style.background = 'transparent';
		      		$("#my_page_content_main").html(dat.setting);
		      	break;
		      }
      		$("#my_page_content_main").html(dat.main_img);
					$("#my_page_content_main_cont").html(dat.content);
					$("#my_page_content_us_info").html(dat.us_info);
					if(dat.cards == 1)
					{
						$("#my_page_content_main_cont_cards").html(dat.html);
					}

					$("#my_page_content_main_overlay,#my_page_content_main").animate({
		      	width: "72.2vw",
		      	marginLeft: "0vw"
		      },200);
      	}
      });

			
		}
		create_ajax(host+"/api/show_wall/?id="+id,success);
	});

	$("#overlay").click(function(){
		$("#new_message_modal").css("display","none");
		$("#overlay").css("display","none");
		$("#header > #search").css("z-index","0");
		$("#header > #search > input").css("z-index","0");
		$("#search > div").css("z-index","0");

		$("#search > input").val("");
		$("#search > div").html("");
	});

	$("body").on("click","#photo_modal button",function(){
		var cok = $("#user_id").val();
		var gal_id = $(this).next().val();
		var text = $(this).prev().val();
		var node = $(this);
		function success(data)
		{
			node.prev().prev().prepend(data);
		}
		create_ajax(host+"/api/add_comment/?"+cok+"="+gal_id+"&text="+text,success);
	});

	$("body").on("click", ".new_message", function(){
		$("#overlay").css("display","block");
		var id = $(this).next().val();
		var name = $(this).next().next().val();
		var ava = $(this).next().next().next().val();
		$("#new_message_modal").css("display","block");
		$("#new_message_modal > img").attr("src", "./img/"+ava);
		$("#new_message_modal > p").html(name);
		$("#new_message_modal > #fr_id").val(id);

		$("#header > #search").css("z-index","0");
		$("#header > #search > input").css("z-index","0");
		$("#search > div").css("z-index","0");

		$("#search > input").val("");
		$("#search > div").html("");
	});

	/*$("body").on("click","#friends_view > ul > li:nth-child(2) > a:nth-child(2)",function(){
		var cok = $("#user_id").val();
		var us_id = $(this).next().val();

		function success(data)
		{
			window.location.href="http://y-o.fun/messages/default/"+data;
		}
		create_ajax("http://y-o.fun/api/create_dialog/?"+cok+"="+us_id,success);
	});*/

	$("body").on("click","#friends_view > ul > li > div:nth-child(1) > i:nth-child(3)",function(){
		var id = $(this).parent().next().children('input').val();
		var user_id = $("#user_id").val();

		function success(data)
			{
				window.location.reload();
			}
			create_ajax(host+"/api/delete_friend/?"+user_id+"="+id,success);
	});

	$("body").on("click","#friends_view > ul > li > div:nth-child(1) > i:nth-child(1)",function(){
		var id = $(this).parent().next().children('input').val();
		var user_id = $("#user_id").val();

		alert(host+"/api/add_black_list/?"+user_id+"="+id);

		function success(data)
		{
			window.location.reload();
		}
			create_ajax(host+"/api/add_black_list/?"+user_id+"="+id,success);
	});

	$("#search_friends_input_form > input").focus(function(){
		var query = $(this).val();
		var id = $("#user_id").val();
		if (event.keyCode == 13) {
			function success(data)
			{
				$("#search_friends_result").html(data);
				$("#search_friends_stat > label:nth-child(2)").html($("#friends_search_count").val());
			}
			create_ajax(host+"/api/get_search/?"+id+"="+query,success);
		}
	});

	$("#search_friends_input_form > input").keyup(function(e){
		if(e.keyCode == 13)
		{
			var query = $(this).val();
			var id = $("#user_id").val();
			if (event.keyCode == 13) {
				function success(data)
				{
					$("#search_friends_result").html(data);
					$("#search_friends_stat > label:nth-child(2)").html($("#friends_search_count").val());
				}
				create_ajax(host+"/api/get_search/?"+id+"="+query,success);
			}
		}
	});

	$("body").on('click','#search_friends_input_form > i',function(){
		var query = $("#search_friends_input_form > input").val();
		var id = $("#user_id").val();
			function success(data)
			{
				$("#search_friends_result").html(data);
				$("#search_friends_stat").next().next().html($("#friends_search_count").val());
			}
			create_ajax(host+"/api/get_search/?"+id+"="+query,success);
	});

	$("body").on('click','#search_friends_result > ul > li > div:nth-child(2) i:nth-child(1)',function(){
		var fr = $(this).next().next().next().val();
		var id = $("#user_id").val();

		function success(data)
			{
				window.location.reload();
				//$("#search_friends_result").html(data);
				//$("#search_friends_stat").next().next().html($("#friends_search_count").val());
			}
			create_ajax(host+"/api/add_friend/?"+id+"="+fr,success);
	});



	$("#create_dialog_content > ul > li").click(function(){
		var cok = $("#user_id").val();
		var id = $(this).next().val();
		var text = 'Hi. I want to start community with you!';
			function success(data)
			{
				window.location.reload();
			}

			create_ajax(host+"/api/create_dialog/?"+cok+"="+id+"&text="+text,success);
	});

	$(".delete_fr").click(function(){
		var cok = $("#user_id").val();
		var id = $(this).children("input").val();
		alert(id);

		function success(data)
		{
			window.location.reload();
		}
		create_ajax(host+"/api/delete_friend/?"+cok+"="+id,success);
	});

	$(".call_to_friend").click(function(){
		$("#overlay").css("display","block");
		$("#video_call_modal").css("display","block");
		targetUsername = $(this).next().val();
		targetUser = $(this).next().next().val();
		showMyFace();
		//connect();
	});

	$(".end_call_button").click(function(){
		$(".modal_window").css("display","none");
		$("#overlay").css("display","none");
		//ex();
	});

	$(".settings_input").keyup(function(){
		var text = $(this).val();
		var type = $(this).attr("id");
		var cok = $("#user_id").val();
		var node = $(this);

		function success(data)
		{
			if(data == "")
			{
				node.css("border","1px solid green");
			}
		}
		create_ajax(host+"/api/update_user_info/?"+cok+"="+text+"&type="+type,success);
	});

	$(".group > input").keyup(function(event){
		var id = $("#user_id").val();
		var text = $(this).val();
		if (event.keyCode == 13) {
			function success(data)
			{
				var dat = JSON.parse(data);
				$("#shops_list").html(dat.html);
			}
			create_ajax(host+"/api/search_shops/?"+id+"="+text,success);
		}
	});

	$(".shops_list_dropdown > ul > li:nth-child(6)").click(function(){
		var data = new FormData();
		var cok = $("#user_id").val();
		var type = $(this).prev().prev().prev().prev().val();
		var id = $(this).prev().prev().prev().val();

		data.append('id',id);

		var text = (type=="admin") ? 'Are you sure you want to delete the store?' : 'Are you sure you want to unsubsribe the store?';

		//var elem = $(this).closest('.item');
		        $.confirm({
		            'title'        : 'Shop deleted',
		            'message'    : text,
		            'buttons'    : {
		                'Yes'    : {
		                    'class'    : 'blue',
		                    'action': function(){
		                    	if(type == "admin")
													{
														function success(dat)
														{
															alert(dat);
													        window.location.reload();
														}
														create_ajax_array(host+"/progs/delete_shop.php",success,data);
													}
													else
													{
														function success(dat)
														{
													        window.location.reload();
														}
														create_ajax(host+"/api/delete_user_shop/?"+cok+"="+id,success);
													}
		                    }
		                },
		                'no'    : {
		                    'class'    : 'gray',
		                    'action': function(){}    // Nothing to do in this case. You can as well omit the action property.
		                }
		            }
		        });
	});

	$("#shop_header > label").click(function(){
		var cok = $("#user_id").val();
		var type = $(this).prev().prev().prev().val();
		var id = $(this).prev().prev().val();

		var text = (type=="admin") ? 'Are you sure you want to delete the store?' : 'Are you sure you want to unsubsribe the store?';

		//var elem = $(this).closest('.item');
		        $.confirm({
		            'title'        : 'Shop deleted',
		            'message'    : text,
		            'buttons'    : {
		                'Yes'    : {
		                    'class'    : 'blue',
		                    'action': function(){
		                    	if(type == "admin")
								{
									function success(data)
									{
								        window.location.reload();
									}
									create_ajax(host+"/api/delete_shop/?"+cok+"="+id,success);
								}
								else
								{
									function success(data)
									{
								        window.location.reload();
									}
									create_ajax(host+"/api/delete_user_shop/?"+cok+"="+id,success);
								}
		                    }
		                },
		                'no'    : {
		                    'class'    : 'gray',
		                    'action': function(){}    // Nothing to do in this case. You can as well omit the action property.
		                }
		            }
		        });
	});

	$("#modal_update_prod_top > div").click(function(){
	var id = $(this).next().val();

	var text = 'Are you sure you want to delete the product?';

	//var elem = $(this).closest('.item');
	    $.confirm({
	        'title'        : 'Product deleted',
	        'message'    : text,
	        'buttons'    : {
	            'Yes'    : {
	                'class'    : 'blue',
	                'action': function(){
					function success(data)
					{
				        window.location.reload();
					}
					create_ajax(host+"/api/delete_shop_prod/?id="+id,success);
					
	         }
	            },
	            'no'    : {
	                'class'    : 'gray',
	                'action': function(){}    // Nothing to do in this case. You can as well omit the action property.
	            }
	        }
	    });
	});


	$("#modal_create_shop > div:nth-child(2) > div:nth-child(2) > input").change(function(){
		var input = $(this)[0];
		var id = $(this).attr('id');

		if ( input.files && input.files[0] ) {
		    if ( input.files[0].type.match('image.*') ) {
		      var reader = new FileReader();
		      reader.onload = function(e) { $('#'+id).attr('src', e.target.result); }
		      reader.readAsDataURL(input.files[0]);
		    }
		  }
	});

	$("#create_shop_button").click(function(){
		$("#modal_create_shop_overflow").css("display","block");
		var name = $(this).prev().prev().val();
		var descr = $(this).prev().val();
		var cok = $("#user_id").val();
		if(name == "")
		{
			$("#modal_create_shop > div:nth-child(2) > div:nth-child(3) input").css("border","1px solid red");
		}
		if(descr == "")
		{
			$("#modal_create_shop > div:nth-child(2) > div:nth-child(3) textarea").css("border","1px solid red");
		}
		
		var data = new FormData();
			$.each($("#modal_create_shop > div:nth-child(2) > div:nth-child(2) > input")[0].files, function(i, file) {
    		data.append('file-'+i, file);
		});

		data.append('id',cok);
		data.append('name',decodeURIComponent(name));
		data.append('descr',decodeURIComponent(descr));

		function success(dat)
		{
			alert(dat);
			if(data != "name!")
			{
				$("#modal_create_shop_overflow .spinner").css("display","none");
				$("#load_message").html("Перенаправляем вас к магазину");
				window.location.href=host+"shop/default/"+data;
			}
			else
			{
				$("#modal_create_shop_overflow").css("display","none");
				$("#modal_create_shop > div:nth-child(2) > div:nth-child(3) input").css("border","1px solid red");
				$("#modal_create_shop > div:nth-child(2) > div:nth-child(3) p").html("Магазин с таким именем уже сущевствует!");
			}
		}
		create_ajax_array('../progs/create_shop.php',success,data);
	});

	$("#modal_create_shop > div:nth-child(2) > div:nth-child(3) input").keyup(function(){
		$("#modal_create_shop > div:nth-child(2) > div:nth-child(3) p").html("");
		$(this).css("border","1px solid rgb(169, 169, 169)");
	});

	$("#modal_update_shop > div:nth-child(2) > div:nth-child(2) > input").change(function(){
		var input = $(this)[0];
		var id = $(this).attr('id');

		if ( input.files && input.files[0] ) {
		    if ( input.files[0].type.match('image.*') ) {
		      var reader = new FileReader();
		      reader.onload = function(e) { $('#'+id).attr('src', e.target.result); }
		      reader.readAsDataURL(input.files[0]);
		    }
		  }
	});

	$("#update_shop_button").click(function(){
		var id = $("#modal_update_shop_id").val();
		var name = $("#modal_update_shop > div:nth-child(2) > div:nth-child(3) > input").val();
		var descr = $("#modal_update_shop > div:nth-child(2) > div:nth-child(3) > textarea").val();


		$("#modal_update_shop_overflow").css("display","block");
		if(name == "")
		{
			$("#modal_update_shop > div:nth-child(2) > div:nth-child(3) input").css("border","1px solid red");
		}
		if(descr == "")
		{
			$("#modal_update_shop > div:nth-child(2) > div:nth-child(3) textarea").css("border","1px solid red");
		}
		
		var data = new FormData();
			$.each($("#modal_update_shop > div:nth-child(2) > div:nth-child(2) > input")[0].files, function(i, file) {
    		data.append('file-'+i, file);
		});

		data.append('id',id);
		data.append('name',decodeURIComponent(name));
		data.append('descr',decodeURIComponent(descr));

		function success(dat)
		{
			window.location.reload();
		}
		
		create_ajax_array('../progs/update_shop.php',success,data);
	});

	$("#modal_update_shop > div:nth-child(2) > div:nth-child(3) input").keyup(function(){
		$("#modal_update_shop > div:nth-child(2) > div:nth-child(3) p").html("");
		$(this).css("border","1px solid rgb(169, 169, 169)");
	});

	$("#modal_add_prod > div:nth-child(2) > div:nth-child(2) > input").change(function(){
		var input = $(this)[0];
		var id = $(this).attr('id');

		if ( input.files && input.files[0] ) {
		    if ( input.files[0].type.match('image.*') ) {
		      var reader = new FileReader();
		      reader.onload = function(e) { $('#'+id).attr('src', e.target.result); }
		      reader.readAsDataURL(input.files[0]);
		    }
		  }
	});

	$("#create_prod_button").click(function(){
		$("#modal_add_prod_overflow").css("display","block");
		var name = $(this).prev().prev().prev().val();
		var descr = $(this).prev().val();
		var cost = $(this).prev().prev().val();
		var cok = $("#real_shop_id").val();
		if(name == "")
		{
			$("#modal_add_prod > div:nth-child(2) > div:nth-child(3) input").css("border","1px solid red");
		}
		if(descr == "")
		{
			$("#modal_add_prod > div:nth-child(2) > div:nth-child(3) textarea").css("border","1px solid red");
		}
		
		var data = new FormData();
			$.each($("#modal_add_prod > div:nth-child(2) > div:nth-child(2) > input")[0].files, function(i, file) {
    		data.append('file-'+i, file);
		});

		data.append('id',cok);
		data.append('name',decodeURIComponent(name));
		data.append('cost',cost);
		data.append('descr',decodeURIComponent(descr));

		function success(data)
		{
			$("#modal_add_prod_overflow .spinner").css("display","none");
			$("#load_message").html("Обновляем магазин");
			window.location.reload();
		}
		create_ajax_array('/progs/add_prod.php',success,data);
	});

	$("#modal_add_prod > div:nth-child(2) > div:nth-child(3) input").keyup(function(){
		$("#modal_add_prod > div:nth-child(2) > div:nth-child(3) p").html("");
		$(this).css("border","1px solid rgb(169, 169, 169)");
	});

	$("#update_prod_button").click(function(){
		$("#modal_add_prod_overflow").css("display","block");
		var name = $("#modal_update_prod > div:nth-child(2) > div:nth-child(3) > input:nth-child(2)").val();
		var cost = $("#modal_update_prod > div:nth-child(2) > div:nth-child(3) > input:nth-child(3)").val();
		var cok = $("#modal_update_prod > div:nth-child(2) > div:nth-child(3) > input:nth-child(6)").val();
		var descr = $("#modal_update_prod > div:nth-child(2) > div:nth-child(3) > textarea").val();

		if(name == "")
		{
			$("#modal_update_prod > div:nth-child(2) > div:nth-child(3) input").css("border","1px solid red");
		}
		if(descr == "")
		{
			$("#modal_update_prod > div:nth-child(2) > div:nth-child(3) textarea").css("border","1px solid red");
		}
		
		var data = new FormData();
			$.each($("#modal_update_prod > div:nth-child(2) > div:nth-child(2) > input")[0].files, function(i, file) {
    		data.append('file-'+i, file);
		});

		data.append('id',cok);
		data.append('name',decodeURIComponent(name));
		data.append('cost',cost);
		data.append('descr',decodeURIComponent(descr));

		function success(data)
		{
			$("#modal_update_prod_overflow .spinner").css("display","none");
			$("#load_message").html("Обновляем магазин");
			window.location.reload();
		}
		create_ajax_array('/progs/update_prod.php',success,data);
	});

	$("#modal_update_prod > div:nth-child(2) > div:nth-child(3) input").keyup(function(){
		$("#modal_update_prod > div:nth-child(2) > div:nth-child(3) p").html("");
		$(this).css("border","1px solid rgb(169, 169, 169)");
	});

	$("body").on("click",".modal_cartung_product_plus",function(){
		var id = $("#user_id").val();
		var prod_id = $(this).next().next().next().val();
		var cost = $(this).next().next().next().next().val();
		var shop_id = $(this).next().next().next().next().next().val();
		var node = $(this);

		function success(data)
		{
			var count = parseInt(node.next().next().children('b').html())+1;

			var count_all = parseInt($("#cart > div:nth-child(2)").html())+1;
			var sum_all = parseInt($("#cart > div:nth-child(3)").html().split(' ')[0])+parseInt(cost);

			$("#modal_cartung_content > div:nth-child(2) > label:nth-child(1) > b:nth-child(1)").html(count_all);
			$("#modal_cartung_content > div:nth-child(2) > label:nth-child(1) > b:nth-child(2)").html(sum_all);
			$("#cart > div:nth-child(3)").html(sum_all+" grn.");
			$("#cart > div:nth-child(2)").html(count_all);

			node.next().next().children('b').html(count);
			node.next().html(data+" grn.");
		}
		create_ajax(host+"/api/plus_cart/?"+id+"="+prod_id+"&"+cost+"="+shop_id,success);
	});

	$("body").on("click",".modal_cartung_product_minus",function(){
		var id = $("#user_id").val();
		var prod_id = $(this).next().next().next().next().val();
		var cost = $(this).next().next().next().next().next().val();
		var shop_id = $(this).next().next().next().next().next().next().val();
		var node = $(this);

		function success(data)
		{
			var count = parseInt(node.next().next().next().children('b').html())-1;

			var count_all = parseInt($("#cart > div:nth-child(2)").html())-1;
			var sum_all = parseInt($("#cart > div:nth-child(3)").html().split(' ')[0])-parseInt(cost);

			$("#modal_cartung_content > div:nth-child(2) > label:nth-child(1) > b:nth-child(1)").html(count_all);
			$("#modal_cartung_content > div:nth-child(2) > label:nth-child(1) > b:nth-child(2)").html(sum_all);
			$("#cart > div:nth-child(3)").html(sum_all+" grn.");
			$("#cart > div:nth-child(2)").html(count_all);

			node.next().next().next().children('b').html(count);
			node.next().next().html(data+" grn.");
		}
		create_ajax(host+"/api/minus_cart/?"+id+"="+prod_id+"&"+cost+"="+shop_id,success);
	});

	$("#modal_order_content > div > div:nth-child(2) > i").click(function(){
		$("#do_payment").next().val($(this).attr("pay-type"));
	});

	$("#do_payment").click(function(){
		var name = $("#modal_order_content > div > div:nth-child(1) > input:nth-child(2)").val();
		var city = $("#modal_order_content > div > div:nth-child(1) > input:nth-child(3)").val();
		var cart = $("#cart_id").val();
		var pay_type = $(this).next().val();
		var cook = $("#user_id").val();

		function success(data)
		{
			console.log(data);
		}
		create_ajax(host+"/api/pay_order/?"+cart+"="+pay_type+"&"+name+"="+city+"&id="+cook,success);
	});


	$("body").on('click','#shop_content_left > ul > div > li:nth-child(2)',function(){
		var id = $("#user_id").val();
		var prod_id = $(this).next().val();
		var cost = $(this).next().next().val();
		var shop_id = $(this).next().next().next().val();

		function success(data)
		{
			window.location.reload();
		}
		create_ajax(host+"/api/plus_cart/?"+id+"="+prod_id+"&"+cost+"="+shop_id,success);
		
	});

	$("#modal_cartung_content > div:nth-child(2) > label:nth-child(2)").click(function(){
		var cart_id = $("#cart_id").val();

		function success(data)
		{
			window.location.reload();
		}
		create_ajax(host+"/api/delete_cart/?id="+cart_id,success);
	});

	$("#cats > ul:nth-child(2) > li:nth-child(1)").click(function(){
		var shop_id = $(this).next().val();
		var html = "<li id='cats_add_form'><div><i class='fas fa-eye-slash'></i></div><div><input placeholder='Name...' /></div><div id='cats_add'><i class='fa fa-plus'></i><i class='fas fa-minus'></i></div><input type='hidden' value='cat' /><input type='hidden' value='"+shop_id+"' /></li>"
		
		$(this).after(html);
	});

	$("body").on("click","#cats_add > i:nth-child(1)",function(){
		var type = $(this).next().val();
		var name = $(this).prev().children().val();
		var icon = $(this).prev().prev().children().attr("class");
		var id = $(this).next().next().val();

		function success(data)
		{
			alert(data);
		}
		create_ajax(host+"/api/add_cat/?"+type+"="+id+"&"+name+"="+icon,success);
	});

	$("body").on("click","#cats_add > i:nth-child(2)",function(){
		$("#cats_add_form").detach();
	});

	$("#cats > ul:nth-child(2) > div > ul > li:nth-child(1)").click(function(){
		var shop_id = $(this).next().val();
		var html = "<li id='podcats_add_form'><div><i class='fas fa-eye-slash'></i></div><div><input placeholder='Name...' /></div><div id='podcats_add'><i class='fa fa-plus'></i><i class='fas fa-minus'></i></div><input type='hidden' value='podcat' /><input type='hidden' value='"+shop_id+"' /></li>"
		$(this).after(html);
	});

	$("body").on("click","#podcats_add > i:nth-child(1)",function(){
		var type = $(this).next().val();
		var name = $(this).prev().children().val();
		var icon = $(this).prev().prev().children().attr("class");
		var id = $(this).next().next().val();

		function success(data)
		{
			alert(data);
		}
		create_ajax(host+"/api/add_cat/?"+type+"="+id+"&"+name+"="+icon,success);
	});

	$("body").on("click","#podcats_add > i:nth-child(2)",function(){
		$("#podcats_add_form").detach();
	});

	$(".cats").click(function(){
		$(this).next().slideToggle(200);
		if($(this).attr("toggled") == "false")
		{
			$(this).attr("toggled","true");
			$(this).children("div:nth-child(3)").children('i').css("transform","rotate(0deg)");
		}
		else
		{
			$(this).attr("toggled","false");
			$(this).children("div:nth-child(3)").children('i').css("transform","rotate(180deg)");
		}
	});

	var dialog_id = "";
	$("#dialogs_view > ul").click(function(){
		var id = $("#user_id").val();
		var dialog = $(this).next().val();
		dialog_id = $(this).next().val();

		function success(data)
		{
			$dat = JSON.parse(data);
			$("#messages_view > div:nth-child(2)").animate({
				height: '-=67vh'
			},
			{
				duration: 800,
			complete: function(){
				$("#messages_view > div:nth-child(2)").css("height", "0");
				$("#messages_view > div:nth-child(1)").html($dat['div_top']);
			    $("#messages_view > div:nth-child(2)").html($dat['mes_div']);
				$("#messages_view > div:nth-child(3)").html($dat['input_div']);
				$("#messages_profile_m > div").html($dat['profile_m']);

			    $("#messages_view > div:nth-child(2)").animate({
			    height: '+=67vh'
			  }, {
			    duration: 800,
			    complete: function() {
			      $("#messages_view > div:nth-child(1), #messages_view > div:nth-child(2)").animate({
			      	opacity: '1'
			      },400);
			      var destination = $("#messages_view").prop('scrollHeight');
			      $("#messages_view").animate({ 
			        	scrollTop: destination 
			        },200);
			      $(".profile_m_ul_tag").lightSlider({
			        item: 1,
			    	});
			    }
			  });
			}
			});
			      

			
		}
		create_ajax(host+"/api/get_messages/?"+id+"="+dialog,success);
	});

	$("body").on('keyup','#dialogs_view_search input',function(ev){
		var id = $("#user_id").val();
		var query = $(this).val();

		if(ev.keyCode == '13')
		{
			function success(data)
			{
				$("#dialogs_view").html(data);
			}
			create_ajax(host+"/api/search_dialogs/?"+id+"="+query,success);
		}
	});

	$("body").on('click',"#new_mes_but",function(){
		var dialog = $(this).next().val();
		var ava = $(this).next().next().val();
		var id = $("#user_id").val();
		var text = $("#new_message_inp").val();

		if(text != '')
		{
			var destination = $("#messages_view").prop('scrollHeight');

			function success(data)
			{
				$("#messages_view > div:nth-child(2) > ul").append("<li class='my_message new_message' style='opacity: 0; width: 0; margin-top: 20vh'><div><img src='https://y-o.fun/img/"+ava+"' /></div><div>"+text+"</div><div></div></li>");
				$(".new_message").animate({
					opacity: 1,
					width: '100%',
					marginTop: '-=15vh'
				},200);

				$(".new_message").attr("class","my_message");
			}

	        $("#messages_view").animate({ 
	        	scrollTop: destination 
	        },
	        {
	        		duration: 200,
	        		complete: function()
	        		{
	        			create_ajax(host+"/api/add_message/?"+dialog+"="+text+"&id="+id,success);
	        			$("#messages_view").animate({ 
				        	scrollTop: destination 
				        },200);
	        		}
	    });
	  }
	});

	$("body").on('keyup',"#input_message > input",function(e){
		if(e.keyCode == 13)
		{
			var dialog = $("#new_mes_but").next().val();
			var ava = $("#new_mes_but").next().next().val();
			var id = $("#user_id").val();
			var text = $("#new_message_inp").val();

			if(text != '')
			{
				var destination = $("#messages_view").prop('scrollHeight');

				function success(data)
				{
					$("#messages_view > div:nth-child(2) > ul").append("<li class='my_message new_message' style='opacity: 0; width: 0; margin-top: 20vh'><div><img src='"+host+"/img/"+ava+"' /></div><div>"+text+"</div><div></div></li>");
					$(".new_message").animate({
						opacity: 1,
						width: '100%',
						marginTop: '-=15vh'
					},200);

					$(".new_message").attr("class","my_message");
				}

		        $("#messages_view").animate({ 
		        	scrollTop: destination 
		        },
		        {
		        		duration: 200,
		        		complete: function()
		        		{
		        			create_ajax(host+"/api/add_message/?"+dialog+"="+text+"&id="+id,success);
		        			$("#messages_view").animate({ 
					        	scrollTop: destination 
					        },200);
		        		}
		      });
	      }
		}
	});

	$("#messages_view > div:nth-child(1)").click(function(){
		if($("#messages_profile_m").attr("class") == "")
		{
			$("#messages_view").animate({
				width: "-=16vw"
			},800);

			$("#messages_profile_m").animate({
					height: "+=83vh",
					marginTop: "-=80vh"
				},800);

			$("#messages_profile_m > div").animate({
					opacity: "1"
				},800);
			$("#messages_profile_m").attr("class","active");
		}
	});

	$("body").on('click','.friend_message',function(){
		if($("#messages_profile_m").attr("class") == "")
		{
			$("#messages_view").animate({
				width: "-=16vw"
			},800);

			$("#messages_profile_m").animate({
					height: "+=83vh",
					marginTop: "-=80vh"
				},800);

			$("#messages_profile_m > div").animate({
					opacity: "1"
				},800);
			$("#messages_profile_m").attr("class","active");
		}
	});

	$("body").on('click','#messages_profile_m > div > p:nth-child(1)',function(){
		$("#messages_view").animate({
			width: "+=16vw"
		},800);

		$("#messages_profile_m").animate({
			marginTop: "+=80vh",
				height: "-=83vh"
			},800);

		$("#messages_profile_m > div").animate({
				opacity: "0"
			},800);
			$("#messages_profile_m").attr("class","");
	});

	var config = {
	    apiKey: "AIzaSyBeoJ-bXcfRKJ8_bQ9zUek9WqDH9xTAWCg",
	    authDomain: "y-o-fun.firebaseapp.com",
	    databaseURL: "https://y-o-fun.firebaseio.com",
	    projectId: "y-o-fun",
	    storageBucket: "y-o-fun.appspot.com",
	    messagingSenderId: "665753098818"
	  };
	  firebase.initializeApp(config);

	var database = firebase.database().ref();
	var valid = true;
	var interval = 2000;
	function add_elem()
	{
		var img = $("#friend_message > div:nth-child(3) > img").attr("src");
	  			if($("#content").attr("act") == "")
	  			{
	  				$("#messages_view > div:nth-child(2) > ul").append("<li class='friend_message on_delete'><div></div><div>Typing...</div><div><img src='"+img+"'></div></li>");
	  			}
	  			setTimeout(function(){
	  				$(".on_delete").remove();
	  				$("#content").attr("act","");
	  			},interval)
	  			$("#content").attr("act","active");
	}

	function valid_dialog(evt)
    {
  		var dialog_idd = evt.A.k.ba.left.value.B;
  		if(dialog_idd == dialog_id)
  		{
	  		var user2 = evt.A.k.ba.right.value.k.ba.left.value.B;
	  		var user1 = evt.A.k.ba.value.k.ba.left.value.B;
	  		if($("#user_id").val() != user1)
	  		{
	  			add_elem();
	  		}
  		}
	}
  	database.on('child_added', valid_dialog);

	function create_dialog_fire(dialog_id, user_1, user_2)
	{
  		var dat = {id: dialog_id,user1: {typing: 'true', id: user_1},user2:{typing: 'false', id: user_2}};
  		msg = database.push(dat); 		
	  	msg.remove();
	}

	var full_href = window.location.href;
	var exp_href = full_href.split('/');
	var href = exp_href[3];

	if(href == "dialogs")
	{
		$("body").css("overflow",'hidden');
	}

	$("body").on('keydown','#new_message_inp',function(){
		create_dialog_fire($(this).next().val(),$("#user_id").val(),$(this).next().next().val());
		interval+=2000;
	});
$.getJSON(
    'https://cdn.rawgit.com/highcharts/highcharts/057b672172ccc6c08fe7dbb27fc17ebca3f5b770/samples/data/usdeur.json',
function (data) {

        Highcharts.chart('shop_stat_cont', {
            chart: {
                zoomType: 'x',
                backgroundColor: 'transparent',
                textColor: '#fff'
            },
            title: {
                text: 'USD to EUR exchange rate over time'
            },
            subtitle: {
                text: document.ontouchstart === undefined ?
                        'Click and drag in the plot area to zoom in' : 'Pinch the chart to zoom in'
            },
            xAxis: {
                type: 'datetime'
            },
            yAxis: {
                title: {
                    text: 'Exchange rate'
                }
            },
            legend: {
                enabled: false
            },
            plotOptions: {
                area: {
                    fillColor: {
                        linearGradient: {
                            x1: 0,
                            y1: 0,
                            x2: 0,
                            y2: 1
                        },
                        stops: [
                            [0, Highcharts.getOptions().colors[0]],
                            [1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
                        ]
                    },
                    marker: {
                        radius: 2
                    },
                    lineWidth: 1,
                    states: {
                        hover: {
                            lineWidth: 1
                        }
                    },
                    threshold: null
                }
            },

            series: [{
                type: 'area',
                name: 'USD to EUR',
                data: [1, 0, 4,1.5,10,3]
            }]
        });
    }
    );
//---------------------------Update avatar (uploaded and cropping)

	$(document).on("change", "#my_info", function(){

		var data = new FormData();
		$.each($('#add_avatar_new_image')[0].files, function(i, file) {
    	data.append('file-'+i, file);
		});

		$.ajax({
    		url: '/progs/upload_new_avatar.php',
    		data: data,
    		cache: false,
    		contentType: false,
    		processData: false,
    		type: 'POST',
    		success: function(data){
        		var dat = data.split('=');
        		var filenamee = dat[1].split('&');
        		var filename = filenamee[0];
        		var height = dat[2];
        		/*alert(filename);
        		alert(height);*/
        		$.ajax({
					type: "POST",
					url: "/progs/cropping.php",
					data: "filename="+filename+"&height="+height,
					dataType: "html",
					asynch: false,
					cache: false,
					success: function(data)
					{
						alert(data);
						$("#page_add_avatar_modal").html(data);

						$("#page_add_avatar_modal_avatar_submit_crop_button").click(function(){
							
							var x = $("#page_add_avatar_modal_avatar_submit_crop_x").val();
							var y = $("#page_add_avatar_modal_avatar_submit_crop_y").val();
							var w = $("#page_add_avatar_modal_avatar_submit_crop_w").val();
							var h = $("#page_add_avatar_modal_avatar_submit_crop_h").val();
							var file = $("#page_add_avatar_modal_avatar_filename").val();

							$.ajax({
								type: "POST",
								url: "/progs/create_crop.php",
								data: "filename="+file+"&x="+x+"&w="+w+"&h="+h,
								dataType: "html",
								asynch: false,
								cache: false,
								success: function(data)
								{
									if(data == "OK")
									{
										window.location.href = "/";
										window.location.reload;
									}
								}
							});
						});
					}
		});
    		}
		});
	});
});