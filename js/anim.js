$(document).ready(function(){
	var full_href = window.location.href;
	var exp_href = full_href.split('/');
	var href = exp_href[3];

	if(href == "dialogs")
	{
		$("body").css("background","#FBF9F9");
	}

	if(href == 'friends' || href == "dialogs" || href == "shops" || href == "shop")
	{
		$("#header_image").animate({
			height: "-=55vh"
		});

		$("#header_owerflow").animate({
			height: "-=55vh"
		});

		$("#shop_stat_icon").animate({
			marginTop: "-=55vh"
		});

		$("#header").animate({
			height: "-=55vh"
		});

		$("#menu_icon").animate({
			marginTop: "-=55vh"
		});

		$("#menu_settings").animate({
			marginTop: "-=55vh"
		});

		$("#menu_logout").animate({
			marginTop: "-=55vh"
		});

		$("#content").animate({
			marginTop: "-=55vh"
		});
	}

	var on_more = false;

	$("body").on("click","#shop_stat_icon",function(){
		if(on_more == false)
		{
			on_more = true;
			$("#header_image").animate({
				height: "+=55vh"
			});

			$("#shop_stat_cont").css("display", "block");
			$('#shop_stat_overlay').css("display","block");
			$('#shop_stat_mirror').css("display","block");

			$("#header_owerflow").animate({
				height: "+=55vh"
			});

			$("#shop_stat_icon").animate({
				marginTop: "+=55vh"
			});

			$("#shop_stat_icon > i").css({
				'-moz-transform':'rotate(180deg)',
		          '-webkit-transform':'rotate(180deg)',
		          '-o-transform':'rotate(180deg)',
		          '-ms-transform':'rotate(180deg)',
		          'transform':'rotate(180deg)'
			},300);

			$("#header").animate({
				height: "+=55vh"
			});

			$("#menu_icon").animate({
				marginTop: "+=55vh"
			});

			$("#menu_settings").animate({
				marginTop: "+=55vh"
			});

			$("#menu_logout").animate({
				marginTop: "+=55vh"
			});

			$("#content").animate({
				marginTop: "+=55vh"
			});
		}
		else
		{
			on_more = false;
			$("#header_image").animate({
			height: "-=55vh"
			});

			$("#header_owerflow").animate({
				height: "-=55vh"
			});

			$("#shop_stat_icon").animate({
				marginTop: "-=55vh"
			}, 400, function()
				{
					$("#shop_stat_cont").css("display", "none");
					$('#shop_stat_overlay').css("display","none");
					$('#shop_stat_mirror').css("display","none");
				}
			);

			$("#shop_stat_icon > i").css({
				'-moz-transform':'rotate(0deg)',
		          '-webkit-transform':'rotate(0deg)',
		          '-o-transform':'rotate(0deg)',
		          '-ms-transform':'rotate(0deg)',
		          'transform':'rotate(0deg)'
			},300);

			$("#header").animate({
				height: "-=55vh"
			});

			$("#menu_icon").animate({
				marginTop: "-=55vh"
			});

			$("#menu_settings").animate({
				marginTop: "-=55vh"
			});

			$("#menu_logout").animate({
				marginTop: "-=55vh"
			});

			$("#content").animate({
				marginTop: "-=55vh"
			});
		}
	});

	function show_modal(elem,modal)
	{

		modal.css("left",elem.offset().left);
		modal.css("top",elem.offset().top);
		modal.css("display","block");
		modal.animate({
			width: "+=70vw",
			height: "+=80vh",
			top: "10vh",
			left: "15vw"
		},150, function(){
			$("#bod").css("filter","blur(5px)");
			$("#overlay").css("display","block");
		});
	}

	function hide_modal(elem,modal)
	{
		$("#bod").css("filter","blur(0)");
		modal.animate({
			top: elem.offset().top+"px",
			left: elem.offset().left+"px",
			width: "-=70vw",
			height: "-=80vh",
		},150, function(){
			modal.css("display","none");
			$("#overlay").css("display","none");
		});
	}

	$("body").on('dblclick','#my_page_menu ul > li',function() {
		show_modal($(this),$("#modal_update_wall"));
	});

	$(".my_page_gallery_list").mouseover(function(){
		$(this).children(".photo_container").children("#my_page_gallery_list_bottom").css("display", "flex");
		$(this).children("#my_page_gallery_list_top").css("display", "flex");
		$(this).children(".photo_container").children("#my_page_gallery_list_bottom").css("top", "-116px");
		$(this).children(".photo_container").children("img").css("top","-58px");
		$(this).children("iframe").css("top","58px");
	});

	$(".my_page_gallery_list").mouseleave(function(){
		$(this).children(".photo_container").children("#my_page_gallery_list_bottom").css("display", "none");
		$(this).children("#my_page_gallery_list_top").css("display", "none");
		$(this).children(".photo_container").children("img").css("top","0");
		$(this).children("iframe").css("top","0");
	});

		$("#friends_cont_menu > ul > li:nth-child(1)").click(function(){
		if($("#followers_view").attr("class") == "active")
		{
			$("#followers_view").animate({
				marginTop: "+=15vh",
				opacity: "0"
			});

			$("#friends_view").animate({
				marginTop: "-=15vh",
				opacity: "1"
			});
		}

		if($("#search_friends_view").attr("class") == "active")
		{
			$("#search_friends_view").animate({
				marginTop: "+=15vh",
				opacity: "0"
			});

			$("#friends_view").animate({
				marginTop: "-=15vh",
				opacity: "1"
			});
		}

		if($("#black_list_view").attr("class") == "active")
		{
			$("#black_list_view").animate({
				marginTop: "+=15vh",
				opacity: "0"
			});

			$("#friends_view").animate({
				marginTop: "-=15vh",
				opacity: "1"
			});
		}

		$("#friends_view").attr("class","active");
		$("#followers_view").attr("class","");
		$("#black_list_view").attr("class","");
		$("#search_friends_view").attr("class","");

		$("#friends_view").css("display","flex");
		$("#followers_view").css("display","none");
		$("#black_list_view").css("display","none");
		$("#search_friends_view").css("display","none");
	});

	$("#friends_cont_menu > ul > li:nth-child(2)").click(function(){
		if($("#friends_view").attr("class") == "active")
		{
			$("#friends_view").animate({
				marginTop: "+=15vh",
				opacity: "0"
			});

			$("#followers_view").animate({
				marginTop: "-=15vh",
				opacity: "1"
			});
		}

		if($("#search_friends_view").attr("class") == "active")
		{
			$("#search_friends_view").animate({
				marginTop: "+=15vh",
				opacity: "0"
			});

			$("#followers_view").animate({
				marginTop: "-=15vh",
				opacity: "1"
			});
		}

		if($("#black_list_view").attr("class") == "active")
		{
			$("#black_list_view").animate({
				marginTop: "+=15vh",
				opacity: "0"
			});

			$("#followers_view").animate({
				marginTop: "-=15vh",
				opacity: "1"
			});
		}

		$("#friends_view").attr("class","");
		$("#followers_view").attr("class","active");
		$("#black_list_view").attr("class","");
		$("#search_friends_view").attr("class","");

		$("#friends_view").css("display","none");
		$("#followers_view").css("display","flex");
		$("#black_list_view").css("display","none");
		$("#search_friends_view").css("display","none");
	});

	$("#friends_cont_menu > ul > li:nth-child(3)").click(function(){
		if($("#friends_view").attr("class") == "active")
		{
			$("#friends_view").animate({
				marginTop: "+=15vh",
				opacity: "0"
			},400);

			$("#search_friends_view").animate({
				marginTop: "-=15vh",
				opacity: "1"
			},400);
		}

		if($("#followers_view").attr("class") == "active")
		{
			$("#followers_view").animate({
				marginTop: "+=15vh",
				opacity: "0"
			},400);

			$("#search_friends_view").animate({
				marginTop: "-=15vh",
				opacity: "1"
			},400);
		}

		if($("#black_list_view").attr("class") == "active")
		{
			$("#black_list_view").animate({
				marginTop: "+=15vh",
				opacity: "0"
			},400);

			$("#search_friends_view").animate({
				marginTop: "-=15vh",
				opacity: "1"
			},400);
		}

		$("#friends_view").attr("class","");
		$("#followers_view").attr("class","");
		$("#black_list_view").attr("class","");
		$("#search_friends_view").attr("class","active");

		$("#friends_view").css("display","none");
		$("#followers_view").css("display","none");
		$("#black_list_view").css("display","none");
		$("#search_friends_view").css("display","flex");
	});

	$("#menu-open-handler").click(function(){ $(".real-menu").css("display","block"); })

	$("#friends_cont_menu > ul > li:nth-child(4)").click(function(){
		if($("#friends_view").attr("class") == "active")
		{
			$("#friends_view").animate({
				marginTop: "+=15vh",
				opacity: "0"
			},400);

			$("#black_list_view").animate({
				marginTop: "-=15vh",
				opacity: "1"
			},400);
		}

		if($("#followers_view").attr("class") == "active")
		{
			$("#followers_view").animate({
				marginTop: "+=15vh",
				opacity: "0"
			},400);

			$("#black_list_view").animate({
				marginTop: "-=15vh",
				opacity: "1"
			},400);
		}

		if($("#search_friends_view").attr("class") == "active")
		{
			$("#search_friends_view").animate({
				marginTop: "+=15vh",
				opacity: "0"
			},400);

			$("#black_list_view").animate({
				marginTop: "-=15vh",
				opacity: "1"
			},400);
		}

		$("#friends_view").attr("class","");
		$("#followers_view").attr("class","");
		$("#black_list_view").attr("class","active");
		$("#search_friends_view").attr("class","");

		$("#friends_view").css("display","none");
		$("#followers_view").css("display","none");
		$("#black_list_view").css("display","flex");
		$("#search_friends_view").css("display","none");
	});

	$("body").on('click','#friends_view > ul > li > div:nth-child(2) i:nth-child(1), #followers_view > ul > li > div:nth-child(2) i:nth-child(1), #black_list_view > ul > li > div:nth-child(2) i:nth-child(1)',function(){
		$("#modal_new_message").show();
		$("#bod").css("filter","blur(5px)");

		var id = $(this).parent().children('input').val();
		var avatar = $(this).parent().parent().children('div:nth-child(1)').children('a:nth-child(2)').children('img').attr("src");
		var name = $(this).parent().parent().children('div:nth-child(1)').children('a:nth-child(4)').html();

		$("#modal_new_message_header_img > img").attr("src",avatar);
		$("#modal_new_message_header_name").html(name);
		$("#modal_new_message_content_id").val(id);
	});

	$("#modal_new_message_content_new_mes_but").click(function(){
		var cok = $("#user_id").val();
		var id = $("#modal_new_message_content_id").val();
		var text = $("#modal_new_message_content_text").val();

		if(text != '')
		{
			function success(data)
			{
				window.location.reload();
			}

			create_ajax(host+"/api/create_dialog/?"+cok+"="+id+"&text="+text,success);
		}
	});

	$("#modal_new_message_header i").click(function(){
		$("#modal_new_message").hide();
		$("#bod").css("filter","blur(0)");
	});

	$("#search_friends_input_form > input").focus(function(){
		$(this).next().next().next().animate({
			marginLeft: "-=23vw",
			width: "+=46vw"
		},200);

		if($(this).val() == "")
		{
			$(this).next().next().animate({
				marginTop: "-=3vh"
			},200);
		}
	});

	$("#search_friends_input_form > input").focusout(function(){
		$(this).next().next().next().animate({
			marginLeft: "+=23vw",
			width: "-=46vw"
		},200);

		if($(this).val() == "")
		{
			$(this).next().next().animate({
				marginTop: "+=3vh"
			},200);
		}
	});

	$("#dialogs_view_search > button").click(function(){
	 	$("#create_dialog_modal").show(200);
	 	$("#overlay").css("display", "block");
	 });


	$("#shops_cont > div:nth-child(1) > div:nth-child(2)").click(function(){
		show_modal($(this),$("#modal_create_shop"));
	});

	$("#my_page_menu_search button").click(function(){
		show_modal($(this),$("#modal_add_wall"));
	});

	$("#my_info").click(function(){
		show_modal($(this),$("#page_add_avatar_modal"));
	});

	$("#cart").click(function(){
		$("#modal_cartung").css("left",$(this).offset().left);
		$("#modal_cartung").css("top",$(this).offset().top);
		$("#modal_cartung").css("display","block");
		$("#modal_cartung").animate({
			width: "+=60vw",
			height: "+=70vh",
			top: "10vh",
			left: "20vw"
		},150, function(){
			$("#bod").css("filter","blur(5px)");
			$("#overlay").css("display","block");
		});
	});

	$("#modal_cartung_top > i").click(function(){
		if($("#cart").offset() != undefined){hide_modal($("#cart"),$("#modal_cartung"));}
	});

	$("#create_order").click(function(){
			$("#modal_order").css("display","block");
		$("#modal_cartung").animate({
			width: "-=60vw"
		},200, function(){
		});

		$("#modal_order").animate({
			width: "+=60vw",
			left: "20vw"
		},200);
	});

	$("#back_to_cart").click(function(){
		$("#modal_cartung").animate({
			width: "+=60vw"
		},200);

		$("#modal_order").animate({
			width: "-=60vw",
			left: "70vw"
		},200, function(){
			$("#modal_order").css("display","none");
		});
	});

	$("#close_create_shop_modal").click(function(){
		hide_modal($("#shops_cont > div:nth-child(1) > div:nth-child(2)"),$("#modal_create_shop"));
		if($("#my_page_menu_search button").offset() != undefined){hide_modal($("#my_page_menu_search button"),$("#modal_add_wall"));}
		if($("body").offset() != undefined){hide_modal($("body"),$("#modal_update_wall"));}
	});

	$("#close_add_prod_modal").click(function(){
		hide_modal($("#add_product_but"),$("#modal_add_prod"));
	});

	$("#add_product_but").click(function(){
		show_modal($(this),$("#modal_add_prod"));
		$("#create_prod_button").next().val($("#cats > ul:nth-child(2) > li:nth-child(1)").next().val());
	});

	$("#overlay").click(function(){
		//if($("#my_info").offset() != undefined){hide_modal($("#my_info"),$("#page_add_avatar_modal"));}
		if($("#shops_cont > div:nth-child(1) > div:nth-child(2)").offset() != undefined){hide_modal($("#shops_cont > div:nth-child(1) > div:nth-child(2)"),$("#modal_create_shop"));}
		if($("#add_product_but").offset() != undefined){hide_modal($("#add_product_but"),$("#modal_add_prod"));}
		if($("#shop_content_left > ul > i").offset() != undefined){hide_modal($("#shop_content_left > ul > i"),$("#modal_update_prod"));}
		if($(".shops_list_dropdown > ul > li:nth-child(1)").offset() != undefined){hide_modal($(".shops_list_dropdown > ul > li:nth-child(1)"),$("#modal_update_shop"));}
		if($("#my_page_menu_search button").offset() != undefined){hide_modal($("#my_page_menu_search button"),$("#modal_add_wall"));}
		if($("#my_page_user").offset() != undefined){hide_modal($("#my_page_user"),$("#modal_update_wall"));}
		if($("#cart").offset() != undefined){hide_modal($("#cart"),$("#modal_cartung"));}
		$("#modal_order").css("width","0vw"); $("#modal_order").css("left","80vw"); $("#modal_order").css("display","none");
		$("#create_dialog_modal").hide(200);
	});

	$(".shops_list_dropdown > ul > li:nth-child(1)").click(function(){
		var id = $(this).next().next().val();
		var name = $(this).next().next().next().val();
		var descr = $(this).next().next().next().next().val();

		$("#modal_update_shop > div:nth-child(2) > div:nth-child(3) > input").val(name);
		$("#modal_update_shop > div:nth-child(2) > div:nth-child(3) > textarea").val(descr);
		$("#modal_update_shop_id").val(id);

		show_modal($(this),$("#modal_update_shop"));
	});

	$("body").on('click','#shop_content_left > ul > i',function(){
		var id = $(this).next().val();
		var name = $(this).next().next().val();
		var cost = $(this).next().next().next().val();
		var descr = $(this).next().next().next().next().val();

		$("#modal_update_prod > div:nth-child(2) > div:nth-child(3) > input:nth-child(2)").val(name);
		$("#modal_update_prod > div:nth-child(2) > div:nth-child(3) > input:nth-child(3)").val(cost);
		$("#modal_update_prod > div:nth-child(2) > div:nth-child(3) > input:nth-child(6)").val(id);
		$("#modal_update_prod > div:nth-child(2) > div:nth-child(3) > textarea").val(descr);
		$("#modal_update_prod_top > input").val(id);

		show_modal($(this),$("#modal_update_prod"));
	});


	$("html").click(function() {
		$('.shops_list_dropdown').attr("id","");
	});

	$('.shops_list_dropdown ul li').each(function() {
	    var delay = $(this).index() * 50 + 'ms';

	    $(this).css({
	        '-webkit-transition-delay': delay,
	        '-moz-transition-delay': delay,
	        '-o-transition-delay': delay,
	        'transition-delay': delay
	    });                  
	});

	$(".shops_list_drop_but").click (function(e){
	  $('.shops_list_dropdown').attr("id","");
	  e.stopPropagation();
	  if($(this).next().attr("id") == "active")
	  {
	  	$(this).next().attr("id","");
	  }
	  else
	  {
	  	$(this).next().attr("id","active");
	  }
	});

	 $('.shops_list_dropdown').click (function(e){
	  e.stopPropagation();
	});


	 $("#my_info > .icon_menu > i").mouseover(function(){
	 		$("#main_header").animate({
		 		width: "50%"
		 	}, 200);
		 	$("#main_header").css({
    				 background: "-webkit-gradient(linear, left top, right top, from(#ccc),to(#f8f7f7))"}).css({background: "-moz-linear-gradient(left, #ccc 0%, #f8f7f7 100%)"
    		});
	 });

	 $("#header").mouseleave(function(){
	 	$("#main_header").animate({
		 		width: "100%"
		 	}, 200);
		 	$("#main_header").css({
    				 background: "-webkit-gradient(linear, left top, right top, from(#f8f7f7),to(#ccc))"}).css({background: "-moz-linear-gradient(left, #f8f7f7 0%, #ccc 100%)"
    		});
	 });

	 $("#menu_icon > i").click(function(){
	 	$("#menu_icon > i").animate({
	 		opacity: "0"
	 	},{
	 		duration: 250,
	 		complete: function(){
	 			$("#menu_icon").css("border-radius",'3px');

				  $( "#menu_icon" ).animate({
				    marginLeft: "-=11vw",
					width: "30vw",
					opacity: "0.9"
				  }, {
				    duration: 500,
				    complete: function() {
				     $("#menu_icon > div").show(250);
				    }
				  });
				}
				});
	 	});

	 $("#menu_icon > div").click(function(){
		$("#menu_icon > div").hide(250);
	 	$("#menu_icon").animate({
	 		marginLeft: "70vw",
	 		width: "-=28.7vw",
	 		opacity: "1"
	 	},{
	 		duration: 500,
	 		complete: function(){
	 			$("#menu_icon").css("border-radius",'100px');
	 			$("#menu_icon > i").animate({
			 		opacity: "1"
			 	},250);
			}
		});
	 });
});