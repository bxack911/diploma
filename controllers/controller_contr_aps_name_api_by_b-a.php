<?php
class Controller_api extends Controller
{
	function __construct()
	{
		$this -> model = new Model_api();
		$this -> view = new View();
	}

	function get_params_name( $params )
	{
		$name = array();
	    $value = array();
	    $exploader = array();
	    for( $i = 0; $i < count($params); $i++ )
	    {
	        $exploader[$i] = explode('&', $params[$i]);
	    }
	    
	    for( $i = 0; $i < count($exploader); $i++ )
	    {
	        $expexp = explode('=', $exploader[$i][0]);
	        $name[$i] = $expexp[0];
	        $value[$i] = $expexp[1];
	    }

	    return $name;
	}

	function get_params_value( $params )
	{
		$name = array();
	    $value = array();
	    $exploader = array();
	    for( $i = 0; $i < count($params); $i++ )
	    {
	        $exploader[$i] = explode('&', $params[$i]);
	    }
	    
	    for( $i = 0; $i < count($exploader); $i++ )
	    {
	        $expexp = explode('=', $exploader[$i][0]);
	        $name[$i] = $expexp[0];
	        $value[$i] = $expexp[1];
	    }

	    return $value;
	}

	function action_user_reg( $params )
	{
		$data = $this->model->user_reg( $this -> get_params_name( $params ), $this -> get_params_value( $params ) );

		$this->view->generate('admin_view.php', 'api.php',$data);
	}

	function action_auth( $params )
	{
		$data = $this->model->auth( $this -> get_params_name( $params ), $this -> get_params_value( $params ) );

		$this->view->generate('admin_view.php', 'api.php',$data);
	}

	function action_search_wall( $params )
	{
		$data = $this->model->search_wall( $this -> get_params_name( $params ), $this -> get_params_value( $params ) );

		$this->view->generate('admin_view.php', 'api.php',$data);
	}

	function action_add_wall( $params )
	{
		$data = $this->model->add_wall( $this -> get_params_name( $params ), $this -> get_params_value( $params ) );

		$this->view->generate('admin_view.php', 'api.php',$data);
	}

	function action_delete_wall( $params )
	{
		$data = $this->model->delete_wall( $this -> get_params_name( $params ), $this -> get_params_value( $params ) );

		$this->view->generate('admin_view.php', 'api.php',$data);
	}

	function action_update_wall( $params )
	{
		$data = $this->model->update_wall( $this -> get_params_name( $params ), $this -> get_params_value( $params ) );

		$this->view->generate('admin_view.php', 'api.php',$data);
	}

	function action_show_wall( $params )
	{
		$data = $this->model->show_wall( $this -> get_params_name( $params ), $this -> get_params_value( $params ) );

		$this->view->generate('admin_view.php', 'api.php',$data);
	}
	
	function action_get_shop_cards( $params )
	{
		$data = $this->model->get_shop_cards( $this -> get_params_name( $params ), $this -> get_params_value( $params ) );

		$this->view->generate('admin_view.php', 'api.php',$data);
	}

	function action_add_message( $params )
	{
		$data = $this->model->add_message( $this -> get_params_name( $params ), $this -> get_params_value( $params ) );

		$this->view->generate('admin_view.php', 'api.php',$data);
	}

	function action_add_friend( $params )
	{
		$data = $this->model->add_friend( $this -> get_params_name( $params ), $this -> get_params_value( $params ) );

		$this->view->generate('admin_view.php', 'api.php',$data);
	}

	function action_delete_friend( $params )
	{
		$data = $this->model->delete_friend( $this -> get_params_name( $params ), $this -> get_params_value( $params ) );

		$this->view->generate('admin_view.php', 'api.php',$data);
	}

	function action_get_user_info( $params )
	{
		$data = $this->model->get_user_info( $this -> get_params_name( $params ), $this -> get_params_value( $params ) );

		$this->view->generate('admin_view.php', 'api.php',$data);
	}

	function action_search_users( $params )
	{
		$data = $this->model->search_users( $this -> get_params_name( $params ), $this -> get_params_value( $params ) );

		$this->view->generate('admin_view.php', 'api.php',$data);
	}

	function action_add_black_list( $params )
	{
		$data = $this->model->add_black_list( $this -> get_params_name( $params ), $this -> get_params_value( $params ) );

		$this->view->generate('admin_view.php', 'api.php',$data);
	}

	function action_remove_black_list( $params )
	{
		$data = $this->model->remove_black_list( $this -> get_params_name( $params ), $this -> get_params_value( $params ) );

		$this->view->generate('admin_view.php', 'api.php',$data);
	}

	function action_get_user_dialogs( $params )
	{
		$data = $this->model->get_user_dialogs( $this -> get_params_name( $params ), $this -> get_params_value( $params ) );

		$this->view->generate('admin_view.php', 'api.php',$data);
	}

	function action_get_messages( $params )
	{
		$data = $this->model->get_messages( $this -> get_params_name( $params ), $this -> get_params_value( $params ) );

		$this->view->generate('admin_view.php', 'api.php',$data);
	}

	function action_search_messages( $params )
	{
		$data = $this->model->search_messages( $this -> get_params_name( $params ), $this -> get_params_value( $params ) );

		$this->view->generate('admin_view.php', 'api.php',$data);
	}

	function action_search_dialogs( $params )
	{
		$data = $this->model->search_dialogs( $this -> get_params_name( $params ), $this -> get_params_value( $params ) );

		$this->view->generate('admin_view.php', 'api.php',$data);
	}

	function action_get_search( $params )
	{
		$data = $this->model->get_search( $this -> get_params_name( $params ), $this -> get_params_value( $params ) );

		$this->view->generate('admin_view.php', 'api.php',$data);
	}

	function action_create_dialog( $params )
	{
		$data = $this->model->create_dialog( $this -> get_params_name( $params ), $this -> get_params_value( $params ) );

		$this->view->generate('admin_view.php', 'api.php',$data);
	}

	function action_get_user_shops( $params )
	{
		$data = $this->model->get_user_shops( $this -> get_params_name( $params ), $this -> get_params_value( $params ) );

		$this->view->generate('admin_view.php', 'api.php',$data);
	}

	function action_delete_user_shop( $params )
	{
		$data = $this->model->delete_user_shop( $this -> get_params_name( $params ), $this -> get_params_value( $params ) );

		$this->view->generate('admin_view.php', 'api.php',$data);
	}

	function action_add_user_shop( $params )
	{
		$data = $this->model->add_user_shop( $this -> get_params_name( $params ), $this -> get_params_value( $params ) );

		$this->view->generate('admin_view.php', 'api.php',$data);
	}

	function action_add_shop( $params )
	{
		$data = $this->model->add_shop( $this -> get_params_name( $params ), $this -> get_params_value( $params ) );

		$this->view->generate('admin_view.php', 'api.php',$data);
	}

	function action_delete_shop( $params )
	{
		$data = $this->model->delete_shop( $this -> get_params_name( $params ), $this -> get_params_value( $params ) );

		$this->view->generate('admin_view.php', 'api.php',$data);
	}

	function action_update_shop( $params )
	{
		$data = $this->model->update_shop( $this -> get_params_name( $params ), $this -> get_params_value( $params ) );

		$this->view->generate('admin_view.php', 'api.php',$data);
	}

	function action_get_shop_prod( $params )
	{
		$data = $this->model->get_shop_prod( $this -> get_params_name( $params ), $this -> get_params_value( $params ) );

		$this->view->generate('admin_view.php', 'api.php',$data);
	}

	function action_add_shop_prod( $params )
	{
		$data = $this->model->add_shop_prod( $this -> get_params_name( $params ), $this -> get_params_value( $params ) );

		$this->view->generate('admin_view.php', 'api.php',$data);
	}

	function action_delete_shop_prod( $params )
	{
		$data = $this->model->delete_shop_prod( $this -> get_params_name( $params ), $this -> get_params_value( $params ) );

		$this->view->generate('admin_view.php', 'api.php',$data);
	}

	function action_update_shop_prod( $params )
	{
		$data = $this->model->update_shop_prod( $this -> get_params_name( $params ), $this -> get_params_value( $params ) );

		$this->view->generate('admin_view.php', 'api.php',$data);
	}

	function action_search_shops( $params )
	{
		$data = $this->model->search_shops( $this -> get_params_name( $params ), $this -> get_params_value( $params ) );

		$this->view->generate('admin_view.php', 'api.php',$data);
	}

	function action_add_cat( $params )
	{
		$data = $this->model->add_cat( $this -> get_params_name( $params ), $this -> get_params_value( $params ) );

		$this->view->generate('admin_view.php', 'api.php',$data);
	}

	function action_get_cats( $params )
	{
		$data = $this->model->get_cats( $this -> get_params_name( $params ), $this -> get_params_value( $params ) );

		$this->view->generate('admin_view.php', 'api.php',$data);
	}

	function action_update_cat( $params )
	{
		$data = $this->model->update_cat( $this -> get_params_name( $params ), $this -> get_params_value( $params ) );

		$this->view->generate('admin_view.php', 'api.php',$data);
	}

	function action_delete_cat( $params )
	{
		$data = $this->model->delete_cat( $this -> get_params_name( $params ), $this -> get_params_value( $params ) );

		$this->view->generate('admin_view.php', 'api.php',$data);
	}

	function action_get_cart( $params )
	{
		$data = $this->model->get_cart( $this -> get_params_name( $params ), $this -> get_params_value( $params ) );

		$this->view->generate('admin_view.php', 'api.php',$data);
	}

	function action_delete_cart( $params )
	{
		$data = $this->model->delete_cart( $this -> get_params_name( $params ), $this -> get_params_value( $params ) );

		$this->view->generate('admin_view.php', 'api.php',$data);
	}

	function action_plus_cart( $params )
	{
		$data = $this->model->plus_cart( $this -> get_params_name( $params ), $this -> get_params_value( $params ) );

		$this->view->generate('admin_view.php', 'api.php',$data);
	}

	function action_minus_cart( $params )
	{
		$data = $this->model->minus_cart( $this -> get_params_name( $params ), $this -> get_params_value( $params ) );

		$this->view->generate('admin_view.php', 'api.php',$data);
	}

	function action_input_cart( $params )
	{
		$data = $this->model->input_cart( $this -> get_params_name( $params ), $this -> get_params_value( $params ) );

		$this->view->generate('admin_view.php', 'api.php',$data);
	}

	function action_pay_order( $params )
	{
		$data = $this->model->pay_order( $this -> get_params_name( $params ), $this -> get_params_value( $params ) );

		$this->view->generate('admin_view.php', 'api.php',$data);
	}
}
?>