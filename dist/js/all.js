$(document).ready(function(){
	$(".admin_act").click(function(){
		var act = $(this).attr("act");
		var id = $(this).next().val();
		$.ajax({
				type: "POST",
				url: "../../progs/admin_act.php",
				data: "act="+act+"&id="+id,
				dataType: "html",
				cache: false,
				async: false,
				success: function(data){
					window.location.reload();
				},
				error: function(data){
					alert(data);
				}
		});
	});
});
$(document).ready(function(){
	var full_href = window.location.href;
	var exp_href = full_href.split('/');
	var href = exp_href[3];

	if(href == 'friends' || href == "dialogs" || href == "shops" || href == "shop")
	{
		$("#header_image").animate({
			height: "-=55vh"
		});

		$("#header_owerflow").animate({
			height: "-=55vh"
		});

		$("#shop_stat_icon").animate({
			marginTop: "-=55vh"
		});

		$("#header").animate({
			height: "-=55vh"
		});

		$("#menu_icon").animate({
			marginTop: "-=55vh"
		});

		$("#menu_settings").animate({
			marginTop: "-=55vh"
		});

		$("#menu_logout").animate({
			marginTop: "-=55vh"
		});

		$("#content").animate({
			marginTop: "-=55vh"
		});
	}

	var on_more = false;

	$("body").on("click","#shop_stat_icon",function(){
		if(on_more == false)
		{
			on_more = true;
			$("#header_image").animate({
				height: "+=55vh"
			});

			$("#shop_stat_cont").css("display", "block");
			$('#shop_stat_overlay').css("display","block");
			$('#shop_stat_mirror').css("display","block");

			$("#header_owerflow").animate({
				height: "+=55vh"
			});

			$("#shop_stat_icon").animate({
				marginTop: "+=55vh"
			});

			$("#shop_stat_icon > i").css({
				'-moz-transform':'rotate(180deg)',
		          '-webkit-transform':'rotate(180deg)',
		          '-o-transform':'rotate(180deg)',
		          '-ms-transform':'rotate(180deg)',
		          'transform':'rotate(180deg)'
			},300);

			$("#header").animate({
				height: "+=55vh"
			});

			$("#menu_icon").animate({
				marginTop: "+=55vh"
			});

			$("#menu_settings").animate({
				marginTop: "+=55vh"
			});

			$("#menu_logout").animate({
				marginTop: "+=55vh"
			});

			$("#content").animate({
				marginTop: "+=55vh"
			});
		}
		else
		{
			on_more = false;
			$("#header_image").animate({
			height: "-=55vh"
			});

			$("#header_owerflow").animate({
				height: "-=55vh"
			});

			$("#shop_stat_icon").animate({
				marginTop: "-=55vh"
			}, 400, function()
				{
					$("#shop_stat_cont").css("display", "none");
					$('#shop_stat_overlay').css("display","none");
					$('#shop_stat_mirror').css("display","none");
				}
			);

			$("#shop_stat_icon > i").css({
				'-moz-transform':'rotate(0deg)',
		          '-webkit-transform':'rotate(0deg)',
		          '-o-transform':'rotate(0deg)',
		          '-ms-transform':'rotate(0deg)',
		          'transform':'rotate(0deg)'
			},300);

			$("#header").animate({
				height: "-=55vh"
			});

			$("#menu_icon").animate({
				marginTop: "-=55vh"
			});

			$("#menu_settings").animate({
				marginTop: "-=55vh"
			});

			$("#menu_logout").animate({
				marginTop: "-=55vh"
			});

			$("#content").animate({
				marginTop: "-=55vh"
			});
		}
	});

	function show_modal(elem,modal)
	{

		modal.css("left",elem.offset().left);
		modal.css("top",elem.offset().top);
		modal.css("display","block");
		modal.animate({
			width: "+=70vw",
			height: "+=80vh",
			top: "10vh",
			left: "15vw"
		},150, function(){
			$("#bod").css("filter","blur(5px)");
			$("#overlay").css("display","block");
		});
	}

	function hide_modal(elem,modal)
	{
		$("#bod").css("filter","blur(0)");
		modal.animate({
			top: elem.offset().top+"px",
			left: elem.offset().left+"px",
			width: "-=70vw",
			height: "-=80vh",
		},150, function(){
			modal.css("display","none");
			$("#overlay").css("display","none");
		});
	}

	$(".my_page_gallery_list").mouseover(function(){
		$(this).children(".photo_container").children("#my_page_gallery_list_bottom").css("display", "flex");
		$(this).children("#my_page_gallery_list_top").css("display", "flex");
		$(this).children(".photo_container").children("#my_page_gallery_list_bottom").css("top", "-116px");
		$(this).children(".photo_container").children("img").css("top","-58px");
		$(this).children("iframe").css("top","58px");
	});

	$(".my_page_gallery_list").mouseleave(function(){
		$(this).children(".photo_container").children("#my_page_gallery_list_bottom").css("display", "none");
		$(this).children("#my_page_gallery_list_top").css("display", "none");
		$(this).children(".photo_container").children("img").css("top","0");
		$(this).children("iframe").css("top","0");
	});

	$("#search_friends_input_form > input").focus(function(){
		$(this).next().next().next().animate({
			marginLeft: "-=23vw",
			width: "+=46vw"
		},200);

		if($(this).val() == "")
		{
			$(this).next().next().animate({
				marginTop: "-=3vh"
			},200);
		}
	});

	$("#search_friends_input_form > input").focusout(function(){
		$(this).next().next().next().animate({
			marginLeft: "+=23vw",
			width: "-=46vw"
		},200);

		if($(this).val() == "")
		{
			$(this).next().next().animate({
				marginTop: "+=3vh"
			},200);
		}
	});

	$('html').click(function() {
		$('#friends_dropdown').removeClass("active");
	});

	$('#friends_dropdown ul li').each(function() {
	    var delay = $(this).index() * 50 + 'ms';

	    $(this).css({
	        '-webkit-transition-delay': delay,
	        '-moz-transition-delay': delay,
	        '-o-transition-delay': delay,
	        'transition-delay': delay
	    });                  
	});

	$("#friends_view > ul > li > div:nth-child(1) > i").click (function(e){
	  	e.stopPropagation();
	  	$(this).next().toggleClass("active");
	});

	$("#followers_view > ul > li > div:nth-child(1) > i").click (function(e){
	  	e.stopPropagation();
	  	$(this).next().toggleClass("active");
	});

	$('#friends_dropdown').click (function(e){
	  	e.stopPropagation();
	});


	$("#create_shop").click(function(){
		show_modal($(this),$("#modal_create_shop"));
	});

	$("#my_info").click(function(){
		show_modal($(this),$("#page_add_avatar_modal"));
	});

	$("#close_create_shop_modal").click(function(){
		hide_modal($("#create_shop"),$("#modal_create_shop"));
	});

	$("#close_add_prod_modal").click(function(){
		hide_modal($("#add_product_but"),$("#modal_add_prod"));
	});

	$("#add_product_but").click(function(){
		show_modal($(this),$("#modal_add_prod"));
	});

	$("#overlay").click(function(){
		hide_modal($("#my_info"),$("#page_add_avatar_modal"));
		if($("#create_shop").offset() != undefined){hide_modal($("#create_shop"),$("#modal_create_shop"));}
		if($("#add_product_but").offset() != undefined){hide_modal($("#add_product_but"),$("#modal_add_prod"));}
		$("#create_dialog_modal").hide(200);
	});




	$("html").click(function() {
	  $('.shops_list_dropdown').removeClass("active");
	});

	$('.shops_list_dropdown ul li').each(function() {
	    var delay = $(this).index() * 50 + 'ms';

	    $(this).css({
	        '-webkit-transition-delay': delay,
	        '-moz-transition-delay': delay,
	        '-o-transition-delay': delay,
	        'transition-delay': delay
	    });                  
	});

	$(".shops_list_drop_but").click (function(e){
	  $('.shops_list_dropdown').removeClass("active");
	  e.stopPropagation();
	  $(this).next().toggleClass("active");
	});

	 $('.shops_list_dropdown').click (function(e){
	  e.stopPropagation();
	});


	 $("#my_info > .icon_menu > i").mouseover(function(){
	 		$("#main_header").animate({
		 		width: "50%"
		 	}, 200);
		 	$("#main_header").css({
    				 background: "-webkit-gradient(linear, left top, right top, from(#ccc),to(#f8f7f7))"}).css({background: "-moz-linear-gradient(left, #ccc 0%, #f8f7f7 100%)"
    		});
	 });

	 $("#header").mouseleave(function(){
	 	$("#main_header").animate({
		 		width: "100%"
		 	}, 200);
		 	$("#main_header").css({
    				 background: "-webkit-gradient(linear, left top, right top, from(#f8f7f7),to(#ccc))"}).css({background: "-moz-linear-gradient(left, #f8f7f7 0%, #ccc 100%)"
    		});
	 });

	 $("#menu_icon > i").click(function(){
	 	$("#menu_icon > i").animate({
	 		opacity: "0"
	 	},{
	 		duration: 250,
	 		complete: function(){
	 			$("#menu_icon").css("border-radius",'3px');

				  $( "#menu_icon" ).animate({
				    marginLeft: "-=11vw",
					width: "30vw",
					opacity: "0.9"
				  }, {
				    duration: 500,
				    complete: function() {
				     $("#menu_icon > div").show(250);
				    }
				  });
				}
				});
	 	});

	 $("#menu_icon > div").click(function(){
		$("#menu_icon > div").hide(250);
	 	$("#menu_icon").animate({
	 		marginLeft: "70vw",
	 		width: "-=28.7vw",
	 		opacity: "1"
	 	},{
	 		duration: 500,
	 		complete: function(){
	 			$("#menu_icon").css("border-radius",'100px');
	 			$("#menu_icon > i").animate({
			 		opacity: "1"
			 	},250);
			}
		});
	 });

	 $("#dialogs_view_search > button").click(function(){
	 	$("#bod").css("filter","blur(5px)");
		$("#overlay").css("display","block");

	 	$("#create_dialog_modal").show(200);
	 });
});
$(document).ready(function(){

var host = "http://localhost/";
	$("#auth_cont input").focus(function(){
		$(this).prev().animate({
			marginTop: "-=3vh",
		},400);

		$(this).prev().css("color","#000");


		$(this).next().animate({
			width: "+=29.5vw",
			marginLeft: "-=14.75vw"
		},400);
	});

	$("#auth_cont input").focusout(function(){
		$(this).prev().animate({
			marginTop: "+=3vh",
		},400);

		$(this).prev().css("color","#42420C");

		$(this).next().animate({
			width: "-=29.5vw",
			marginLeft: "+=14.75vw"
		},400);
	});

	$("#to_reg_but").click(function(){
		$("#auth_cont").animate({
			opacity: "0",
			marginTop: "+=10vh"
		},500);

		$("#auth_cont").css("z-index",'0');

		$("#reg_cont").animate({
			opacity: "1",
			marginTop: "-=10vh"
		},500);

		$("#reg_cont").css("z-index",'2');
	});


	$("#reg_cont input").focus(function(){
		$(this).prev().animate({
			marginTop: "-=3vh",
		},400);

		$(this).prev().css("color","#000");


		$(this).next().animate({
			width: "+=29.5vw",
			marginLeft: "-=14.75vw"
		},400);
	});

	$("#reg_cont input").focusout(function(){
		$(this).prev().animate({
			marginTop: "+=3vh",
		},400);

		$(this).prev().css("color","#42420C");

		$(this).next().animate({
			width: "-=29.5vw",
			marginLeft: "+=14.75vw"
		},400);
	});

	$("#to_auth_but").click(function(){
		$("#auth_cont").animate({
			opacity: "1",
			marginTop: "-=10vh"
		},500);

		$("#auth_cont").css("z-index",'2');

		$("#reg_cont").animate({
			opacity: "0",
			marginTop: "+=10vh"
		},500);

		$("#reg_cont").css("z-index",'0');
	});

	$("#reg_but").click(function(){
		var name_surname = $("#reg_name").val();
		var name = '';
		var surname = '';
		var email = $("#reg_email").val();
		var pass = $("#reg_pass").val();
		var rep_pass = $("#reg_pass_rep").val();
		var to_ajax = 1;

		if(name_surname == '')
		{
			$("#reg_name").css("border-bottom","2px solid red");
			to_ajax = 0;
		}
		else
		{
			var exp = name_surname.split(' ');
			name = exp[0];
			alert(exp[1]);
			if(exp[1] == undefined)
			{
				surname = '';
			}
			else
			{
				surname = exp[1];
			}
			$("#reg_name").css("border-bottom","2px solid green");
		}

		if(email == '')
		{
			$("#reg_email").css("border-bottom","2px solid red");
			to_ajax = 0;
		}
		else
		{
			$("#reg_email").css("border-bottom","2px solid green");
		}

		if(pass.length < 8 || pass.length > 60)
		{
			$("#reg_pass").css("border-bottom","2px solid red");
			to_ajax = 0;
		}
		else
		{
			$("#reg_pass").css("border-bottom","2px solid green");
		}

		if(pass != rep_pass)
		{
			$("#reg_pass_rep").css("border-bottom","2px solid red");
			to_ajax = 0;
		}
		else
		{
			$("#reg_pass_rep").css("border-bottom","2px solid green");
		}

		if(to_ajax == 1)
		{
			$.ajax({
				type: "POST",
				url: host+"api/user_reg/?"+name+"="+surname+"&"+email+"="+pass,
				dataType: "html",
				cache: false,
				async: false,
				success: function(data){
					var dat = JSON.parse(data);
					if(dat.error == true)
					{
						alert(dat.error_msg);
					}
					else
					{
						window.location.href="/";
					}
				}
			});
		}
		else
		{
			to_ajax = 1;
		}
	});

	$("#go_but").click(function(){
		var email = $("#auth_login").val();
		var pass = $("#auth_pass").val();

		$.ajax({
			type: "POST",
			url: host+"api/auth/?"+email+"="+pass,
			dataType: "html",
			cache: false,
			async: false,
			success: function(data){
				var dat = JSON.parse(data);
				if(dat.error == true)
				{
					alert(dat.error_msg);
				}
				else
				{
					window.location.href="/";
				}
			}
		});
	});
});
//Відео-чат
/*var config = 
{
    apiKey: "AIzaSyA7m139DR91sbHinG8_QzVE8WgO-yAD7Qc",
    authDomain: "snnsh-5e384.firebaseapp.com",
    databaseURL: "https://snnsh-5e384.firebaseio.com",
    projectId: "snnsh-5e384",
    storageBucket: "",
    messagingSenderId: "493174809361"
};
firebase.initializeApp(config);

var yourVideo = document.getElementById("yourVideo");
var friendsVideo = document.getElementById("friendsVideo");
var myUsername;
var targetUsername;
var servers = {'iceServers': [{'urls': 'stun:stun.services.mozilla.com'}, {'urls': 'stun:stun.l.google.com:19302'}, {'urls': 'turn:numb.viagenie.ca','credential': 'beaver','username': 'webrtc.websitebeaver@gmail.com'}]};
var pc = new RTCPeerConnection(servers);
var database = firebase.database().ref();
var userTarget;

//function connect() 
//{
    database = firebase.database().ref();
    yourVideo = document.getElementById("yourVideo");
    friendsVideo = document.getElementById("friendsVideo");
    myUsername = $("#user_id").val();
    userTarget = targetUsername;
    //Create an account on Viagenie (http://numb.viagenie.ca/), and replace {'urls': 'turn:numb.viagenie.ca','credential': 'websitebeaver','username': 'websitebeaver@email.com'} with the information from your account
    servers = {'iceServers': [{'urls': 'stun:stun.services.mozilla.com'}, {'urls': 'stun:stun.l.google.com:19302'}, {'urls': 'turn:numb.viagenie.ca','credential': 'beaver','username': 'webrtc.websitebeaver@gmail.com'}]};
    pc = new RTCPeerConnection(servers);
    pc.onicecandidate = (event => event.candidate?sendToServer({type: "new-ice-candidate", target: userTarget, ice: event.candidate}):console.log("Sent All Ice") );
    pc.onaddstream = (event => friendsVideo.srcObject = event.stream);
	
	database.on('child_added', readMessage);
    
    function readMessage(evt) {
        var jssPARSE = JSON.parse(evt.A.B);
        console.dir(jssPARSE);
        //var type = jssPARSE.type;
        //var sender = jssPARSE.name;
        //var target = jssPARSE.target;
        //var ice = jssPARSE.ice;
        //var sdp = jssPARSE.sdp;
        console.log(jssPARSE.type);
        console.log(jssPARSE.name);
        console.log(jssPARSE.target);
        //console.log(jssPARSE.ice);
        //console.log(jssPARSE.sdp);
        if (jssPARSE.target == myUsername && jssPARSE.type == "call") 
        {
            //alert("Vam zvonit " + jssPARSE.name);
            //console.log("Vam zvonit " + jssPARSE.name);
            document.getElementById(sounds.rington).play();
            alertMsg(jssPARSE);
        }
        if (jssPARSE.name != myUsername) {
            if (jssPARSE.ice != undefined){
                pc.addIceCandidate(new RTCIceCandidate(jssPARSE.ice));
            }
            else if (jssPARSE.type == "video-offer"){
                pc.setRemoteDescription(new RTCSessionDescription(jssPARSE.sdp))
                  .then(() => pc.createAnswer())
                  .then(answer => pc.setLocalDescription(answer))
                  .then(() => sendToServer({name: myUsername, type: "video-answer", target: userTarget, sdp: pc.localDescription}));
            }
            else if (jssPARSE.type == "video-answer"){
                showFriendsFace();
                document.getElementById(sounds.calling).pause();
                pc.setRemoteDescription(new RTCSessionDescription(jssPARSE.sdp));
            }
            else if (jssPARSE.type == "hang-up"){
            	closeVideoCall();
            }
            else if (jssPARSE.type == "call-To-Off"){
            	alert(jssPARSE.name + " отклонил ваш вызов");
            }
            else if (jssPARSE.type == "callon"){
            	showFriendsFace();
            }
   		}
};
//}


function alertMsg(jssPARSE) 
{
	var answer = confirm("Вам звонит " + jssPARSE.name + <br /> + "Желаете ответить на звонок?");
  if (answer) 
	{
    document.getElementById(sounds.rington).pause();
		$("#overlay").css("display","block");
		$("#video_call_modal").css("display","block");
		showFace();
	}
	else
	{
    document.getElementById(sounds.rington).pause();
    document.getElementById(sounds.end).play();
	sendToServer({name: myUsername, type: "call-To-Off", target: userTarget});
	}
}


function sendToServer(massage) 
{
    var massageJSON = JSON.stringify(massage);
    console.log("Sending '" + massage.type + "' message: " + massageJSON);
    var msg = database.push(massageJSON);
    msg.remove();
}

var sounds = {
            'calling': 'callingSignal',
            'end': 'endCallSignal',
            'rington': 'ringtoneSignal'
        };

//database.on('child_added', readMessage);
function showFace() {
	userTarget = targetUsername;
	sendToServer({type: "callon"});
  	navigator.mediaDevices.getUserMedia({audio:true, video:true})
    .then(stream => yourVideo.srcObject = stream)
    .then(stream => pc.addStream(stream));
}
function showMyFace() {
	userTarget = targetUsername;
  	showFriendsFace();
  	document.getElementById(sounds.calling).play();
	sendToServer({name: myUsername, type: "call", target: userTarget});
  	navigator.mediaDevices.getUserMedia({audio:true, video:true})
    .then(stream => yourVideo.srcObject = stream)
    .then(stream => pc.addStream(stream));
}

function showFriendsFace() {
  pc.createOffer()
    .then(offer => pc.setLocalDescription(offer) )
    .then(() => sendToServer({name: myUsername, type: "video-offer", target: userTarget, sdp: pc.localDescription}));
}

function hangUpCall() 
{
  closeVideoCall();
  sendToServer({
    name: myUsername,
    type: "hang-up",
    target: userTarget
  });
}

function closeVideoCall() 
{
  var remoteVideo = document.getElementById("friendsVideo");
  var localVideo = document.getElementById("yourVideo");

  console.log("Closing the call");

  if (pc) 
  {
  	document.getElementById(sounds.calling).pause();
  	document.getElementById(sounds.end).play();
  	console.log("--> Closing the peer connection");

    pc.onaddstream = null;
    pc.ontrack = null;
    pc.onremovestream = null;
    pc.onnicecandidate = null;
    pc.oniceconnectionstatechange = null;
    pc.onsignalingstatechange = null;
    pc.onicegatheringstatechange = null;
    pc.onnotificationneeded = null;

    if (remoteVideo.srcObject) 
    {
      remoteVideo.srcObject.getTracks().forEach(track => track.stop());
    }

    if (localVideo.srcObject) 
    {
      localVideo.srcObject.getTracks().forEach(track => track.stop());
    }

    remoteVideo.src = null;
    localVideo.src = null;

    pc.close();
    pc = null;

    $(".modal_window").css("display","none");
    $("#overlay").css("display","none");

    document.getElementById(sounds.calling).load();
  }
}
*/

//Створення ajax
var host = "http://localhost";
function create_ajax(url,success)
{
	$.ajax({
		type: "POST",
		url: url,
		dataType: "html",
		asynch: true,
		cache: false,
		success: function(data)
		{
			success(data);
		},
		error: function(data)
		{
			alert("ERROR: "+data);
		}	
	});
}

$(document).ready(function(){
	$("#messages_row").scrollTop($("#messages_row").prop('scrollHeight'));

	$("#menu_logout > i").click(function(){
		function success(data)
		{
			window.location.reload();
		}
		create_ajax("/progs/logout.php",success);
	});

	$("#my_page_menu_search > input").keyup(function(event){
		var id = $("#user_id").val();
		var text = $(this).val();
		if (event.keyCode == 13) {
			function success(data)
			{
				var dat = JSON.parse(data);
				$("#my_page_menu > ul").html(dat.html);
			}
			create_ajax(host+"/api/search_wall/?"+id+"="+text,success);
		}
	});

	$("#my_page_menu > ul > li").click(function(){
		var id = $(this).next().val();

		function success(data)
		{
			var dat = JSON.parse(data);

			$("#my_page_content_main").html(dat.main_img);
			$("#my_page_content_main_cont").html(dat.content);
			$("#my_page_content_us_info").html(dat.us_info);
		}
		create_ajax(host+"/api/show_wall/?id="+id,success);
	});

	$("#overlay").click(function(){
		$("#new_message_modal").css("display","none");
		$("#overlay").css("display","none");
		$("#header > #search").css("z-index","0");
		$("#header > #search > input").css("z-index","0");
		$("#search > div").css("z-index","0");

		$("#search > input").val("");
		$("#search > div").html("");
	});

	$("body").on("click","#photo_modal button",function(){
		var cok = $("#user_id").val();
		var gal_id = $(this).next().val();
		var text = $(this).prev().val();
		var node = $(this);
		function success(data)
		{
			node.prev().prev().prepend(data);
		}
		create_ajax(host+"/api/add_comment/?"+cok+"="+gal_id+"&text="+text,success);
	});

	$("body").on("click", ".new_message", function(){
		$("#overlay").css("display","block");
		var id = $(this).next().val();
		var name = $(this).next().next().val();
		var ava = $(this).next().next().next().val();
		$("#new_message_modal").css("display","block");
		$("#new_message_modal > img").attr("src", "./img/"+ava);
		$("#new_message_modal > p").html(name);
		$("#new_message_modal > #fr_id").val(id);

		$("#header > #search").css("z-index","0");
		$("#header > #search > input").css("z-index","0");
		$("#search > div").css("z-index","0");

		$("#search > input").val("");
		$("#search > div").html("");
	});

	/*$("body").on("click","#friends_view > ul > li:nth-child(2) > a:nth-child(2)",function(){
		var cok = $("#user_id").val();
		var us_id = $(this).next().val();

		function success(data)
		{
			window.location.href="http://y-o.fun/messages/default/"+data;
		}
		create_ajax("http://y-o.fun/api/create_dialog/?"+cok+"="+us_id,success);
	});*/

	$("#friends_cont_menu > ul > li:nth-child(1)").click(function(){
		if($("#followers_view").attr("class") == "active")
		{
			$("#followers_view").animate({
				marginTop: "+=15vh",
				opacity: "0"
			});

			$("#friends_view").animate({
				marginTop: "-=15vh",
				opacity: "1"
			});
		}

		if($("#search_friends_view").attr("class") == "active")
		{
			$("#search_friends_view").animate({
				marginTop: "+=15vh",
				opacity: "0"
			});

			$("#friends_view").animate({
				marginTop: "-=15vh",
				opacity: "1"
			});
		}

		if($("#black_list_view").attr("class") == "active")
		{
			$("#black_list_view").animate({
				marginTop: "+=15vh",
				opacity: "0"
			});

			$("#friends_view").animate({
				marginTop: "-=15vh",
				opacity: "1"
			});
		}

		$("#friends_view").attr("class","active");
		$("#followers_view").attr("class","");
		$("#black_list_view").attr("class","");
		$("#search_friends_view").attr("class","");
	});

	$("#friends_cont_menu > ul > li:nth-child(2)").click(function(){
		if($("#friends_view").attr("class") == "active")
		{
			$("#friends_view").animate({
				marginTop: "+=15vh",
				opacity: "0"
			});

			$("#followers_view").animate({
				marginTop: "-=15vh",
				opacity: "1"
			});
		}

		if($("#search_friends_view").attr("class") == "active")
		{
			$("#search_friends_view").animate({
				marginTop: "+=15vh",
				opacity: "0"
			});

			$("#followers_view").animate({
				marginTop: "-=15vh",
				opacity: "1"
			});
		}

		if($("#black_list_view").attr("class") == "active")
		{
			$("#black_list_view").animate({
				marginTop: "+=15vh",
				opacity: "0"
			});

			$("#followers_view").animate({
				marginTop: "-=15vh",
				opacity: "1"
			});
		}

		$("#friends_view").attr("class","");
		$("#followers_view").attr("class","active");
		$("#black_list_view").attr("class","");
		$("#search_friends_view").attr("class","");
	});

	$("#friends_cont_menu > ul > li:nth-child(3)").click(function(){
		if($("#friends_view").attr("class") == "active")
		{
			$("#friends_view").animate({
				marginTop: "+=15vh",
				opacity: "0"
			},400);

			$("#search_friends_view").animate({
				marginTop: "-=15vh",
				opacity: "1"
			},400);
		}

		if($("#followers_view").attr("class") == "active")
		{
			$("#followers_view").animate({
				marginTop: "+=15vh",
				opacity: "0"
			},400);

			$("#search_friends_view").animate({
				marginTop: "-=15vh",
				opacity: "1"
			},400);
		}

		if($("#black_list_view").attr("class") == "active")
		{
			$("#black_list_view").animate({
				marginTop: "+=15vh",
				opacity: "0"
			},400);

			$("#search_friends_view").animate({
				marginTop: "-=15vh",
				opacity: "1"
			},400);
		}

		$("#friends_view").attr("class","");
		$("#followers_view").attr("class","");
		$("#black_list_view").attr("class","");
		$("#search_friends_view").attr("class","active");
	});

	$("#friends_cont_menu > ul > li:nth-child(4)").click(function(){
		if($("#friends_view").attr("class") == "active")
		{
			$("#friends_view").animate({
				marginTop: "+=15vh",
				opacity: "0"
			},400);

			$("#black_list_view").animate({
				marginTop: "-=15vh",
				opacity: "1"
			},400);
		}

		if($("#followers_view").attr("class") == "active")
		{
			$("#followers_view").animate({
				marginTop: "+=15vh",
				opacity: "0"
			},400);

			$("#black_list_view").animate({
				marginTop: "-=15vh",
				opacity: "1"
			},400);
		}

		if($("#search_friends_view").attr("class") == "active")
		{
			$("#search_friends_view").animate({
				marginTop: "+=15vh",
				opacity: "0"
			},400);

			$("#black_list_view").animate({
				marginTop: "-=15vh",
				opacity: "1"
			},400);
		}

		$("#friends_view").attr("class","");
		$("#followers_view").attr("class","");
		$("#black_list_view").attr("class","active");
		$("#search_friends_view").attr("class","");
	});

	$("#search_friends_input_form > input").focus(function(){
		var query = $(this).val();
		var id = $("#user_id").val();
		if (event.keyCode == 13) {
			function success(data)
			{
				$("#search_friends_result").html(data);
				$("#search_friends_stat > label:nth-child(2)").html($("#friends_search_count").val());
			}
			create_ajax(host+"/api/get_search/?"+id+"="+query,success);
		}
	});

	$("#search_friends_input_form > input").keyup(function(e){
		if(e.keyCode == 13)
		{
			var query = $(this).val();
			var id = $("#user_id").val();
			if (event.keyCode == 13) {
				function success(data)
				{
					$("#search_friends_result").html(data);
					$("#search_friends_stat > label:nth-child(2)").html($("#friends_search_count").val());
				}
				create_ajax(host+"/api/get_search/?"+id+"="+query,success);
			}
		}
	});

	$("body").on('click','#search_friends_input_form > i',function(){
		var query = $("#search_friends_input_form > input").val();
		var id = $("#user_id").val();
			function success(data)
			{
				$("#search_friends_result").html(data);
				$("#search_friends_stat").next().next().html($("#friends_search_count").val());
			}
			create_ajax(host+"/api/get_search/?"+id+"="+query,success);
	});

	$("body").on('click','#search_friends_result > ul > li > div:nth-child(3) > i:nth-child(1)',function(){
		var fr = $(this).next().next().next().val();
		var id = $("#user_id").val();

		function success(data)
			{
				$("#search_friends_result").html(data);
				$("#search_friends_stat").next().next().html($("#friends_search_count").val());
			}
			create_ajax(host+"/api/add_friend/?"+id+"="+fr,success);
	});



	$("#create_dialog_content > ul > li").click(function(){
		var cok = $("#user_id").val();
		var id = $(this).next().val();
		var text = 'Hi. I want to start community with you!';
			function success(data)
			{
				window.location.reload();
			}

			create_ajax(host+"/api/create_dialog/?"+cok+"="+id+"&text="+text,success);
	});

	$(".delete_fr").click(function(){
		var cok = $("#user_id").val();
		var id = $(this).children("input").val();
		alert(id);

		function success(data)
		{
			window.location.reload();
		}
		create_ajax(host+"/api/delete_friend/?"+cok+"="+id,success);
	});

	$(".call_to_friend").click(function(){
		$("#overlay").css("display","block");
		$("#video_call_modal").css("display","block");
		targetUsername = $(this).next().val();
		targetUser = $(this).next().next().val();
		showMyFace();
		//connect();
	});

	$(".end_call_button").click(function(){
		$(".modal_window").css("display","none");
		$("#overlay").css("display","none");
		//ex();
	});

	$(".settings_input").keyup(function(){
		var text = $(this).val();
		var type = $(this).attr("id");
		var cok = $("#user_id").val();
		var node = $(this);

		function success(data)
		{
			if(data == "")
			{
				node.css("border","1px solid green");
			}
		}
		create_ajax(host+"/api/update_user_info/?"+cok+"="+text+"&type="+type,success);
	});

	$(".group > input").keyup(function(event){
		var id = $("#user_id").val();
		var text = $(this).val();
		if (event.keyCode == 13) {
			function success(data)
			{
				var dat = JSON.parse(data);
				$("#shops_list").html(dat.html);
			}
			create_ajax(host+"/api/search_shops/?"+id+"="+text,success);
		}
	});

	$(".shops_list_dropdown > ul > li:nth-child(4), #shop_header > li").click(function(){
		var cok = $("#user_id").val();
		var type = $(this).prev().prev().val();
		var id = $(this).prev().val();

		var text = (type=="admin") ? 'Вы уверены, что хотите удалить этот магазин?' : 'Вы уверены, что хотите отписаться от этого магазина?';

		//var elem = $(this).closest('.item');
		        $.confirm({
		            'title'        : 'Удаление магазина',
		            'message'    : text,
		            'buttons'    : {
		                'Да'    : {
		                    'class'    : 'blue',
		                    'action': function(){
		                    	if(type == "admin")
								{
									function success(data)
									{
								        window.location.reload();
									}
									create_ajax(host+"/api/delete_shop/?"+cok+"="+id,success);
								}
								else
								{
									function success(data)
									{
								        window.location.reload();
									}
									create_ajax(host+"/api/delete_user_shop/?"+cok+"="+id,success);
								}
		                    }
		                },
		                'Нет'    : {
		                    'class'    : 'gray',
		                    'action': function(){}    // Nothing to do in this case. You can as well omit the action property.
		                }
		            }
		        });
	});


	$("#modal_create_shop > div:nth-child(2) > div:nth-child(2) > input").change(function(){
		var input = $(this)[0];
		var id = $(this).attr('id');

		if ( input.files && input.files[0] ) {
		    if ( input.files[0].type.match('image.*') ) {
		      var reader = new FileReader();
		      reader.onload = function(e) { $('#'+id).attr('src', e.target.result); }
		      reader.readAsDataURL(input.files[0]);
		    }
		  }
	});

	$("#create_shop_button").click(function(){
		$("#modal_create_shop_overflow").css("display","block");
		var name = $(this).prev().prev().val();
		var descr = $(this).prev().val();
		var cok = $("#user_id").val();
		if(name == "")
		{
			$("#modal_create_shop > div:nth-child(2) > div:nth-child(3) input").css("border","1px solid red");
		}
		if(descr == "")
		{
			$("#modal_create_shop > div:nth-child(2) > div:nth-child(3) textarea").css("border","1px solid red");
		}
		
		var data = new FormData();
			$.each($("#modal_create_shop > div:nth-child(2) > div:nth-child(2) > input")[0].files, function(i, file) {
    		data.append('file-'+i, file);
		});

		data.append('id',cok);
		data.append('name',decodeURIComponent(name));
		data.append('descr',decodeURIComponent(descr));

		$.ajax({
    		url: '../progs/create_shop.php',
    		data: data,
    		cache: false,
    		contentType: false,
    		processData: false,
    		type: 'POST',
    		success: function(data){
    			if(data != "name!")
    			{
					$("#modal_create_shop_overflow .spinner").css("display","none");
					$("#load_message").html("Перенаправляем вас к магазину");
					window.location.href="http://y-o.fun/shop/default/"+data;
				}
				else
				{
					$("#modal_create_shop_overflow").css("display","none");
					$("#modal_create_shop > div:nth-child(2) > div:nth-child(3) input").css("border","1px solid red");
					$("#modal_create_shop > div:nth-child(2) > div:nth-child(3) p").html("Магазин с таким именем уже сущевствует!");
				}
    		},
    		error: function(data){
    			alert("Error: "+data);
    		}
    	});
	});

	$("#modal_create_shop > div:nth-child(2) > div:nth-child(3) input").keyup(function(){
		$("#modal_create_shop > div:nth-child(2) > div:nth-child(3) p").html("");
		$(this).css("border","1px solid rgb(169, 169, 169)");
	});







	$("#modal_add_prod > div:nth-child(2) > div:nth-child(2) > input").change(function(){
		var input = $(this)[0];
		var id = $(this).attr('id');

		if ( input.files && input.files[0] ) {
		    if ( input.files[0].type.match('image.*') ) {
		      var reader = new FileReader();
		      reader.onload = function(e) { $('#'+id).attr('src', e.target.result); }
		      reader.readAsDataURL(input.files[0]);
		    }
		  }
	});

	$("#create_prod_button").click(function(){
		$("#modal_add_prod_overflow").css("display","block");
		var name = $(this).prev().prev().prev().val();
		var descr = $(this).prev().val();
		var cost = $(this).prev().prev().val();
		var cok = $(this).next().val();
		if(name == "")
		{
			$("#modal_add_prod > div:nth-child(2) > div:nth-child(3) input").css("border","1px solid red");
		}
		if(descr == "")
		{
			$("#modal_add_prod > div:nth-child(2) > div:nth-child(3) textarea").css("border","1px solid red");
		}
		
		var data = new FormData();
			$.each($("#modal_add_prod > div:nth-child(2) > div:nth-child(2) > input")[0].files, function(i, file) {
    		data.append('file-'+i, file);
		});

		data.append('id',cok);
		data.append('name',decodeURIComponent(name));
		data.append('cost',cost);
		data.append('descr',decodeURIComponent(descr));


		$.ajax({
    		url: '/progs/add_prod.php',
    		data: data,
    		cache: false,
    		contentType: false,
    		processData: false,
    		type: 'POST',
    		success: function(data){
				$("#modal_add_prod_overflow .spinner").css("display","none");
				$("#load_message").html("Обновляем магазин");
				window.location.reload();
    		},
    		error: function(data){
    			alert("Error: "+data);
    		}
    	});
	});

	$("#modal_add_prod > div:nth-child(2) > div:nth-child(3) input").keyup(function(){
		$("#modal_add_prod > div:nth-child(2) > div:nth-child(3) p").html("");
		$(this).css("border","1px solid rgb(169, 169, 169)");
	});

	$("#cats > ul:nth-child(2) > li:nth-child(1)").click(function(){
		var shop_id = $(this).next().val();
		var html = "<li id='cats_add_form'><div><i class='fas fa-eye-slash'></i></div><div><input placeholder='Название...' /></div><div id='cats_add'><i class='fa fa-plus'></i></div><input type='hidden' value='cat' /><input type='hidden' value='"+shop_id+"' /></li>"
		$(this).after(html);
	});

	$("body").on("click","#cats_add",function(){
		var type = $(this).next().val();
		var name = $(this).prev().children().val();
		var icon = $(this).prev().prev().children().attr("class");
		var id = $(this).next().next().val();

		function success(data)
		{
			alert(data);
		}
		create_ajax(host+"/api/add_cat/?"+type+"="+id+"&"+name+"="+icon,success);
	});


	$("#cats > ul:nth-child(2) > div > ul > li:nth-child(1)").click(function(){
		var shop_id = $(this).next().val();
		var html = "<li id='podcats_add_form'><div><i class='fas fa-eye-slash'></i></div><div><input placeholder='Название...' /></div><div id='podcats_add'><i class='fa fa-plus'></i></div><input type='hidden' value='podcat' /><input type='hidden' value='"+shop_id+"' /></li>"
		$(this).after(html);
	});

	$("body").on("click","#podcats_add",function(){
		var type = $(this).next().val();
		var name = $(this).prev().children().val();
		var icon = $(this).prev().prev().children().attr("class");
		var id = $(this).next().next().val();

		function success(data)
		{
			alert(data);
		}
		create_ajax(host+"/api/add_cat/?"+type+"="+id+"&"+name+"="+icon,success);
	});

	$(".cats").click(function(){
		$(this).next().slideToggle(200);
		$(this).children("div:nth-child(3)").slideToggle();
	});

	var dialog_id = "";
	$("#dialogs_view > ul").click(function(){
		var id = $("#user_id").val();
		var dialog = $(this).next().val();
		dialog_id = $(this).next().val();

		function success(data)
		{
			$dat = JSON.parse(data);
			$("#messages_view > div:nth-child(2)").animate({
				height: '-=67vh'
			},
			{
				duration: 800,
			complete: function(){
				$("#messages_view > div:nth-child(2)").css("height", "0");
				$("#messages_view > div:nth-child(1)").html($dat['div_top']);
			    $("#messages_view > div:nth-child(2)").html($dat['mes_div']);
				$("#messages_view > div:nth-child(3)").html($dat['input_div']);
				$("#messages_profile_m > div").html($dat['profile_m']);

			    $("#messages_view > div:nth-child(2)").animate({
			    height: '+=67vh'
			  }, {
			    duration: 800,
			    complete: function() {
			      $("#messages_view > div:nth-child(1), #messages_view > div:nth-child(2)").animate({
			      	opacity: '1'
			      },400);
			      var destination = $("#messages_view").prop('scrollHeight');
			      $("#messages_view").animate({ 
			        	scrollTop: destination 
			        },200);
			    }
			  });
			}
			});
			      

			
		}
		create_ajax(host+"/api/get_messages/?"+id+"="+dialog,success);
	});

	$("body").on('click',"#new_mes_but",function(){
		var dialog = $(this).next().val();
		var ava = $(this).next().next().val();
		var id = $("#user_id").val();
		var text = $("#new_message_inp").val();

		var destination = $("#messages_view").prop('scrollHeight');

		function success(data)
		{
			$("#messages_view > div:nth-child(2) > ul").append("<li class='my_message new_message' style='opacity: 0; width: 0; margin-top: 20vh'><div><img src='https://y-o.fun/img/"+ava+"' /></div><div>"+text+"</div><div></div></li>");
			$(".new_message").animate({
				opacity: 1,
				width: '100%',
				marginTop: '-=15vh'
			},200);

			$(".new_message").attr("class","my_message");
		}

        $("#messages_view").animate({ 
        	scrollTop: destination 
        },
        {
        		duration: 200,
        		complete: function()
        		{
        			create_ajax(host+"/api/add_message/?"+dialog+"="+text+"&id="+id,success);
        			$("#messages_view").animate({ 
			        	scrollTop: destination 
			        },200);
        		}
        });
	});

	$("body").on('keyup',"#input_message > input",function(e){
		if(e.keyCode == 13)
		{
			var dialog = $("#new_mes_but").next().val();
			var ava = $("#new_mes_but").next().next().val();
			var id = $("#user_id").val();
			var text = $("#new_message_inp").val();

			var destination = $("#messages_view").prop('scrollHeight');

			function success(data)
			{
				$("#messages_view > div:nth-child(2) > ul").append("<li class='my_message new_message' style='opacity: 0; width: 0; margin-top: 20vh'><div><img src='https://y-o.fun/img/"+ava+"' /></div><div>"+text+"</div><div></div></li>");
				$(".new_message").animate({
					opacity: 1,
					width: '100%',
					marginTop: '-=15vh'
				},200);

				$(".new_message").attr("class","my_message");
			}

	        $("#messages_view").animate({ 
	        	scrollTop: destination 
	        },
	        {
	        		duration: 200,
	        		complete: function()
	        		{
	        			create_ajax(host+"/api/add_message/?"+dialog+"="+text+"&id="+id,success);
	        			$("#messages_view").animate({ 
				        	scrollTop: destination 
				        },200);
	        		}
	        });
		}
	});

	$("#messages_view > div:nth-child(1)").click(function(){
		$("#messages_view").animate({
			width: "-=16vw"
		},800);

		$("#messages_profile_m").animate({
				height: "+=86vh",
				marginTop: "-=86vh"
			},800);

		$("#messages_profile_m > div").animate({
				opacity: "1"
			},800);
	});

	$("body").on('click','.friend_message',function(){
		$("#messages_view").animate({
			width: "-=16vw"
		},800);

		$("#messages_profile_m").animate({
				height: "+=86vh",
				marginTop: "-=86vh"
			},800);

		$("#messages_profile_m > div").animate({
				opacity: "1"
			},800);
	});

	$("body").on('click','#messages_profile_m > div > p:nth-child(1)',function(){
		$("#messages_view").animate({
			width: "+=16vw"
		},800);

		$("#messages_profile_m").animate({
			marginTop: "+=86vh",
				height: "-=86vh"
			},800);

		$("#messages_profile_m > div").animate({
				opacity: "0"
			},800);
	});

	var config = {
	    apiKey: "AIzaSyBeoJ-bXcfRKJ8_bQ9zUek9WqDH9xTAWCg",
	    authDomain: "y-o-fun.firebaseapp.com",
	    databaseURL: "https://y-o-fun.firebaseio.com",
	    projectId: "y-o-fun",
	    storageBucket: "y-o-fun.appspot.com",
	    messagingSenderId: "665753098818"
	  };
	  firebase.initializeApp(config);

	var database = firebase.database().ref();
	var valid = true;
	var interval = 2000;
	function add_elem()
	{
		var img = $("#friend_message > div:nth-child(3) > img").attr("src");
	  			if($("#content").attr("act") == "")
	  			{
	  				$("#messages_view > div:nth-child(2) > ul").append("<li class='friend_message on_delete'><div></div><div>Typing...</div><div><img src='"+img+"'></div></li>");
	  			}
	  			setTimeout(function(){
	  				$(".on_delete").remove();
	  				$("#content").attr("act","");
	  			},interval)
	  			$("#content").attr("act","active");
	}

	function valid_dialog(evt)
    {
  		var dialog_idd = evt.A.k.ba.left.value.B;
  		if(dialog_idd == dialog_id)
  		{
	  		var user2 = evt.A.k.ba.right.value.k.ba.left.value.B;
	  		var user1 = evt.A.k.ba.value.k.ba.left.value.B;
	  		if($("#user_id").val() != user1)
	  		{
	  			add_elem();
	  		}
  		}
	}
  	database.on('child_added', valid_dialog);

	function create_dialog_fire(dialog_id, user_1, user_2)
	{
  		var dat = {id: dialog_id,user1: {typing: 'true', id: user_1},user2:{typing: 'false', id: user_2}};
  		msg = database.push(dat); 		
	  	msg.remove();
	}

	var full_href = window.location.href;
	var exp_href = full_href.split('/');
	var href = exp_href[3];

	if(href == "dialogs")
	{
		$("body").css("overflow",'hidden');
	}

	$("body").on('keydown','#new_message_inp',function(){
		create_dialog_fire($(this).next().val(),$("#user_id").val(),$(this).next().next().val());
		interval+=2000;
	});
$.getJSON(
    'https://cdn.rawgit.com/highcharts/highcharts/057b672172ccc6c08fe7dbb27fc17ebca3f5b770/samples/data/usdeur.json',
function (data) {

        Highcharts.chart('shop_stat_cont', {
            chart: {
                zoomType: 'x',
                backgroundColor: 'transparent',
                textColor: '#fff'
            },
            title: {
                text: 'USD to EUR exchange rate over time'
            },
            subtitle: {
                text: document.ontouchstart === undefined ?
                        'Click and drag in the plot area to zoom in' : 'Pinch the chart to zoom in'
            },
            xAxis: {
                type: 'datetime'
            },
            yAxis: {
                title: {
                    text: 'Exchange rate'
                }
            },
            legend: {
                enabled: false
            },
            plotOptions: {
                area: {
                    fillColor: {
                        linearGradient: {
                            x1: 0,
                            y1: 0,
                            x2: 0,
                            y2: 1
                        },
                        stops: [
                            [0, Highcharts.getOptions().colors[0]],
                            [1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
                        ]
                    },
                    marker: {
                        radius: 2
                    },
                    lineWidth: 1,
                    states: {
                        hover: {
                            lineWidth: 1
                        }
                    },
                    threshold: null
                }
            },

            series: [{
                type: 'area',
                name: 'USD to EUR',
                data: [1, 0, 4,1.5,10,3]
            }]
        });
    }
    );
//---------------------------Update avatar (uploaded and cropping)

	$(document).on("change", "#my_info", function(){

		var data = new FormData();
		$.each($('#add_avatar_new_image')[0].files, function(i, file) {
    	data.append('file-'+i, file);
		});

		$.ajax({
    		url: '/progs/upload_new_avatar.php',
    		data: data,
    		cache: false,
    		contentType: false,
    		processData: false,
    		type: 'POST',
    		success: function(data){
        		var dat = data.split('=');
        		var filenamee = dat[1].split('&');
        		var filename = filenamee[0];
        		var height = dat[2];
        		/*alert(filename);
        		alert(height);*/
        		$.ajax({
					type: "POST",
					url: "/progs/cropping.php",
					data: "filename="+filename+"&height="+height,
					dataType: "html",
					asynch: false,
					cache: false,
					success: function(data)
					{
						alert(data);
						$("#page_add_avatar_modal").html(data);

						$("#page_add_avatar_modal_avatar_submit_crop_button").click(function(){
							
							var x = $("#page_add_avatar_modal_avatar_submit_crop_x").val();
							var y = $("#page_add_avatar_modal_avatar_submit_crop_y").val();
							var w = $("#page_add_avatar_modal_avatar_submit_crop_w").val();
							var h = $("#page_add_avatar_modal_avatar_submit_crop_h").val();
							var file = $("#page_add_avatar_modal_avatar_filename").val();

							$.ajax({
								type: "POST",
								url: "/progs/create_crop.php",
								data: "filename="+file+"&x="+x+"&w="+w+"&h="+h,
								dataType: "html",
								asynch: false,
								cache: false,
								success: function(data)
								{
									if(data == "OK")
									{
										window.location.href = "/";
										window.location.reload;
									}
								}
							});
						});
					}
		});
    		}
		});
	});
});
/*function getSizes(im,obj)
{
		var x_axis = obj.x1;
		var x2_axis = obj.x2;
		var y_axis = obj.y1;
		var y2_axis = obj.y2;
		var thumb_width = obj.width;
		var thumb_height = obj.height;
		if(thumb_width > 0)
			{
				if(confirm("Do you want to save image..!"))
					{
						$.ajax({
							type:"GET",
							url:"/progs/upload_ava.php?t=ajax&img="+$("#image_name").val()+"&w="+thumb_width+"&h="+thumb_height+"&x1="+x_axis+"&y1="+y_axis,
							cache:false,
							success:function(rsponse)
								{
								 $("#cropimage").hide();
								    $("#thumbs").html("");
									$("#thumbs").html("<img src='users/photo/"+rsponse+"' />");
								}
						});
					}
			}
		else
			alert("Please select portion..!");
	}*/

$(document).ready(function() {	
	$("#aded_action").click(function(){
		$("#overlay").css("display","block");
		$("#aded_modal").css("display","block");
	});

	$("#main_menu_icons > i:nth-child(1)").click(function(){
		$("#overlay").css("display","block");
		$("#video_call_modal").css("display","block");
		//showMyFace();
	});

	$("#overlay").click(function(){
		$("#overlay").css("display","none");
		$(".modal_window").css("display","none");
		$(".photo_container").next().css("display","none");
		$("#video_call_modal").css("display","none");
		//ex();
	});

	$(".modal_window_close").click(function(){
		$("#overlay").css("display","none");
		$(".modal_window").css("display","none");
	});

	$(".modal_cideo_call_modal_close").click(function(){
		$("#overlay").css("display","none");
		$(".modal_window").css("display","none");
		//ex();
	});

	$(".end_call_button").click(function(){
		$(".modal_window").css("display","none");
		$("#overlay").css("display","none");
		//ex();
	});

	$("#aded_modal_photo_but").click(function(){
		$("#aded_modal_photo").css("display","block");
		$("#aded_modal_video").css("display","none");
		$("#aded_modal_audio").css("display","none");
	});
	$("#aded_modal_video_but").click(function(){
		$("#aded_modal_photo").css("display","none");
		$("#aded_modal_video").css("display","block");
		$("#aded_modal_audio").css("display","none");
	});
	$("#aded_modal_audio_but").click(function(){
		$("#aded_modal_photo").css("display","none");
		$("#aded_modal_video").css("display","none");
		$("#aded_modal_audio").css("display","block");
	});

	$(".photo_container").click(function(){
		$("#overlay").css("display","block");
		$(this).next().css("display","block");
	});

	$("#main_settings_but").click(function(){
		$("#main_settings").css("display","block");
		$("#notice_settings").css("display","none");
		$("#develop_settings").css("display","none");
	});

	$("#notice_settings_but").click(function(){
		$("#main_settings").css("display","none");
		$("#notice_settings").css("display","block");
		$("#develop_settings").css("display","none");
	});

	$("#develop_settings_but").click(function(){
		$("#main_settings").css("display","none");
		$("#notice_settings").css("display","none");
		$("#develop_settings").css("display","block");
	});

	$("#develop_settings_start_but").click(function(){
		$("#develop_settings_start").css("display","block");
		$("#develop_settings_auth").css("display","none");
		$("#develop_settings_info").css("display","none");
		$("#develop_settings_notice").css("display","none");
		$("#develop_settings_friends").css("display","none");
		$("#develop_settings_music").css("display","none");
		$("#develop_settings_gallery").css("display","none");
	});

	$("#develop_settings_auth_but").click(function(){
		$("#develop_settings_start").css("display","none");
		$("#develop_settings_auth").css("display","block");
		$("#develop_settings_info").css("display","none");
		$("#develop_settings_notice").css("display","none");
		$("#develop_settings_friends").css("display","none");
		$("#develop_settings_music").css("display","none");
		$("#develop_settings_gallery").css("display","none");
	});

	$("#develop_settings_info_but").click(function(){
		$("#develop_settings_start").css("display","none");
		$("#develop_settings_auth").css("display","none");
		$("#develop_settings_info").css("display","block");
		$("#develop_settings_notice").css("display","none");
		$("#develop_settings_friends").css("display","none");
		$("#develop_settings_music").css("display","none");
		$("#develop_settings_gallery").css("display","none");
	});

	$("#develop_settings_notice_but").click(function(){
		$("#develop_settings_start").css("display","none");
		$("#develop_settings_auth").css("display","none");
		$("#develop_settings_info").css("display","none");
		$("#develop_settings_notice").css("display","block");
		$("#develop_settings_friends").css("display","none");
		$("#develop_settings_music").css("display","none");
		$("#develop_settings_gallery").css("display","none");
	});

	$("#develop_settings_friends_but").click(function(){
		$("#develop_settings_start").css("display","none");
		$("#develop_settings_auth").css("display","none");
		$("#develop_settings_info").css("display","none");
		$("#develop_settings_notice").css("display","none");
		$("#develop_settings_friends").css("display","block");
		$("#develop_settings_music").css("display","none");
		$("#develop_settings_gallery").css("display","none");
	});

	$("#develop_settings_music_but").click(function(){
		$("#develop_settings_start").css("display","none");
		$("#develop_settings_auth").css("display","none");
		$("#develop_settings_info").css("display","none");
		$("#develop_settings_notice").css("display","none");
		$("#develop_settings_friends").css("display","none");
		$("#develop_settings_music").css("display","block");
		$("#develop_settings_gallery").css("display","none");
	});

	$("#develop_settings_gallery_but").click(function(){
		$("#develop_settings_start").css("display","none");
		$("#develop_settings_auth").css("display","none");
		$("#develop_settings_info").css("display","none");
		$("#develop_settings_notice").css("display","none");
		$("#develop_settings_friends").css("display","none");
		$("#develop_settings_music").css("display","none");
		$("#develop_settings_gallery").css("display","block");
	});
});
var config = 
{
    apiKey: "AIzaSyA7m139DR91sbHinG8_QzVE8WgO-yAD7Qc",
    authDomain: "snnsh-5e384.firebaseapp.com",
    databaseURL: "https://snnsh-5e384.firebaseio.com",
    projectId: "snnsh-5e384",
    storageBucket: "",
    messagingSenderId: "493174809361"
};
firebase.initializeApp(config);

var yourVideo = document.getElementById("yourVideo");
var friendsVideo = document.getElementById("friendsVideo");
var myUsername;
var targetUsername;
var servers = {'iceServers': [{'urls': 'stun:stun.services.mozilla.com'}, {'urls': 'stun:stun.l.google.com:19302'}, {'urls': 'turn:numb.viagenie.ca','credential': 'beaver','username': 'webrtc.websitebeaver@gmail.com'}]};
var pc;
var database = firebase.database().ref();
var userTarget;

//function connect() 
//{
    database = firebase.database().ref();
    yourVideo = document.getElementById("yourVideo");
    friendsVideo = document.getElementById("friendsVideo");
    myUsername = $("#user_id").val();
    userTarget = targetUsername;
    //Create an account on Viagenie (http://numb.viagenie.ca/), and replace {'urls': 'turn:numb.viagenie.ca','credential': 'websitebeaver','username': 'websitebeaver@email.com'} with the information from your account
    servers = {'iceServers': [{'urls': 'stun:stun.services.mozilla.com'}, {'urls': 'stun:stun.l.google.com:19302'}, {'urls': 'turn:numb.viagenie.ca','credential': 'beaver','username': 'webrtc.websitebeaver@gmail.com'}]};
    pc = new RTCPeerConnection(servers);
    pc.onicecandidate = (event => event.candidate?sendToServer({type: "new-ice-candidate", target: userTarget, ice: event.candidate}):console.log("Sent All Ice") );
    pc.onaddstream = (event => friendsVideo.srcObject = event.stream);
	
	database.on('child_added', readMessage);
    
    function readMessage(evt) {
        var jssPARSE = JSON.parse(evt.A.B);
        console.dir(jssPARSE);
        //var type = jssPARSE.type;
        //var sender = jssPARSE.name;
        //var target = jssPARSE.target;
        //var ice = jssPARSE.ice;
        //var sdp = jssPARSE.sdp;
        console.log(jssPARSE.type);
        console.log(jssPARSE.name);
        console.log(jssPARSE.target);
        //console.log(jssPARSE.ice);
        //console.log(jssPARSE.sdp);
        if (jssPARSE.target == myUsername && jssPARSE.type == "call") 
        {
            //alert("Vam zvonit " + jssPARSE.name);
            //console.log("Vam zvonit " + jssPARSE.name);
            document.getElementById(sounds.rington).play();
            alertMsg(jssPARSE);
        }
        if (jssPARSE.name != myUsername) {
            if (jssPARSE.ice != undefined){
                pc.addIceCandidate(new RTCIceCandidate(jssPARSE.ice));
            }
            else if (jssPARSE.type == "video-offer"){
                pc.setRemoteDescription(new RTCSessionDescription(jssPARSE.sdp))
                  .then(() => pc.createAnswer())
                  .then(answer => pc.setLocalDescription(answer))
                  .then(() => sendToServer({name: myUsername, type: "video-answer", target: userTarget, sdp: pc.localDescription}));
            }
            else if (jssPARSE.type == "video-answer"){
                showFriendsFace();
                document.getElementById(sounds.calling).pause();
                pc.setRemoteDescription(new RTCSessionDescription(jssPARSE.sdp));
            }
            else if (jssPARSE.type == "hang-up"){
            	closeVideoCall();
            }
            else if (jssPARSE.type == "call-To-Off"){
            	alert(jssPARSE.name + " отклонил ваш вызов");
            }
            else if (jssPARSE.type == "callon"){
            	showFriendsFace();
            }
   		}
};
//}


function alertMsg(jssPARSE) 
{
	var answer = confirm("Вам звонит " + jssPARSE.name + " Желаете ответить на звонок?");
  if (answer) 
	{
    document.getElementById(sounds.rington).pause();
		$("#overlay").css("display","block");
		$("#video_call_modal").css("display","block");
		showFace();
	}
	else
	{
    document.getElementById(sounds.rington).pause();
    document.getElementById(sounds.end).play();
	sendToServer({name: myUsername, type: "call-To-Off", target: userTarget});
	}
}


function sendToServer(massage) 
{
    var massageJSON = JSON.stringify(massage);
    console.log("Sending '" + massage.type + "' message: " + massageJSON);
    var msg = database.push(massageJSON);
    msg.remove();
}

var sounds = {
            'calling': 'callingSignal',
            'end': 'endCallSignal',
            'rington': 'ringtoneSignal'
        };

function showFace() {
	userTarget = targetUsername;
	sendToServer({type: "callon"});
  	navigator.mediaDevices.getUserMedia({audio:true, video:true})
    .then(stream => yourVideo.srcObject = stream)
    .then(stream => pc.addStream(stream));
}
function showMyFace() {
	userTarget = targetUsername;
  	showFriendsFace();
  	document.getElementById(sounds.calling).play();
	sendToServer({name: myUsername, type: "call", target: userTarget});
  	navigator.mediaDevices.getUserMedia({audio:true, video:true})
    .then(stream => yourVideo.srcObject = stream)
    .then(stream => pc.addStream(stream));
}

function showFriendsFace() {
  pc.createOffer()
    .then(offer => pc.setLocalDescription(offer) )
    .then(() => sendToServer({name: myUsername, type: "video-offer", target: userTarget, sdp: pc.localDescription}));
}

function hangUpCall() 
{
  closeVideoCall();
  sendToServer({
    name: myUsername,
    type: "hang-up",
    target: userTarget
  });
}

function muteVideo() {

}

function muteAudio() {

}

function closeVideoCall() 
{
  var remoteVideo = document.getElementById("friendsVideo");
  var localVideo = document.getElementById("yourVideo");

  console.log("Closing the call");

  if (pc) 
  {
  	document.getElementById(sounds.calling).pause();
  	document.getElementById(sounds.end).play();
  	console.log("--> Closing the peer connection");

    pc.onaddstream = null;
    pc.ontrack = null;
    pc.onremovestream = null;
    pc.onnicecandidate = null;
    pc.oniceconnectionstatechange = null;
    pc.onsignalingstatechange = null;
    pc.onicegatheringstatechange = null;
    pc.onnotificationneeded = null;

    if (remoteVideo.srcObject) 
    {
      remoteVideo.srcObject.getTracks().forEach(track => track.stop());
    }

    if (localVideo.srcObject) 
    {
      localVideo.srcObject.getTracks().forEach(track => track.stop());
    }

    remoteVideo.src = null;
    localVideo.src = null;

    pc.close();
    pc = null;

    $(".modal_window").css("display","none");
    $("#overlay").css("display","none");

    document.getElementById(sounds.calling).load();
  }
}