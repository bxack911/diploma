<?php
	require_once '../progs/functions.php';
	require_once '../database/DB.php';

	$id = $_POST['id'];
	$name = addslashes(htmlspecialchars(urldecode($_POST['name'])));
	$cost = addslashes(htmlspecialchars(urldecode($_POST['cost'])));
	$descr = addslashes(htmlspecialchars(urldecode($_POST['descr'])));

	$shop = DB::getInstance() -> query("SELECT name FROM us_shops WHERE id = ".$id);

	$folder = "../img/products/".$shop->results()[0] -> name."/";
	$orig_w = 300;

	$imageFile = $_FILES['file-0']['tmp_name'];
	$filename = basename($_FILES['file-0']['name']);

	$to_db = $id.'-'.$filename;

	DB::getInstance() -> no_returns("INSERT INTO sh_product(name,descr,cost,photo,shop_id)VALUES(
				'".htmlspecialchars(urldecode($name))."',
				'".htmlspecialchars(urldecode($descr))."',
				'".htmlspecialchars(urldecode($cost))."',
				'".htmlspecialchars(urldecode($to_db))."',
				".$id."
			)");
	
		list($width, $height ) = getimagesize($imageFile);

		$src = imagecreatefromjpeg($imageFile);
		$orig_h = ($height/$width)*$orig_w;

		$tmp = imagecreatetruecolor($orig_w, $orig_h);
		imagecopyresampled($tmp,$src,0,0,0,0,$orig_w, $orig_h,$width,$height);
		imagejpeg($tmp, $folder.$id.'-'.$filename,100);

		imagedestroy($tmp);
		imagedestroy($src);

		$filename = urlencode($filename);
?>