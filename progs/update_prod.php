<?php
	require_once '../progs/functions.php';
	require_once '../database/DB.php';

	$id = $_POST['id'];
	$name = addslashes(htmlspecialchars(urldecode($_POST['name'])));
	$cost = addslashes(htmlspecialchars(urldecode($_POST['cost'])));
	$descr = addslashes(htmlspecialchars(urldecode($_POST['descr'])));
	$imageFile = $_FILES['file-0']['tmp_name'];
	$filename = basename($_FILES['file-0']['name']);

	$prod = DB::getInstance() -> query("SELECT id,shop_id,photo FROM sh_product WHERE id = ".$id);
	$shop = DB::getInstance() -> query("SELECT name FROM us_shops WHERE id = ".$prod->results()[0] -> shop_id);
	if($filename != "")
	{
		unlink("../img/products/".$shop->results()[1] -> name."/".$prod->results()[0] -> photo);

		$folder = "../img/products/".$shop->results()[1] -> name."/";
		$orig_w = 300;

		$to_db = $id.'-'.$filename;
	}
	DB::getInstance() -> no_returns("UPDATE sh_product SET name = '".$name."' WHERE id = ".$prod->results()[0] -> id);
	DB::getInstance() -> no_returns("UPDATE sh_product SET descr = '".$descr."' WHERE id = ".$prod->results()[0] -> id);
	DB::getInstance() -> no_returns("UPDATE sh_product SET cost = '".$cost."' WHERE id = ".$prod->results()[0] -> id);

	if($filename != "")
	{
		DB::getInstance() -> no_returns("UPDATE sh_product SET photo = '".$to_db."' WHERE id = ".$prod->results()[0] -> id);

	
		list($width, $height ) = getimagesize($imageFile);

		$src = imagecreatefromjpeg($imageFile);
		$orig_h = ($height/$width)*$orig_w;

		$tmp = imagecreatetruecolor($orig_w, $orig_h);
		imagecopyresampled($tmp,$src,0,0,0,0,$orig_w, $orig_h,$width,$height);
		imagejpeg($tmp, $folder.$id.'-'.$filename,100);

		imagedestroy($tmp);
		imagedestroy($src);

		$filename = urlencode($filename);
	}
?>