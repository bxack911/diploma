<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="utf-8">
	<!--<link rel="stylesheet" href="./libs/jquery.Jcrop.css">-->
	<link href="https://fonts.googleapis.com/css?family=Shadows+Into+Light" rel="stylesheet">
	<link rel="stylesheet" href="./libs/player/css/sc-player-minimal.css" type="text/css" />
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
	<link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet"> 
	<link href="https://fonts.googleapis.com/css?family=Lobster" rel="stylesheet">
	
	<link type="text/css" rel="stylesheet" href="http://localhost/libs/carusel/src/css/lightslider.css" />   
	<link rel="stylesheet" media="screen" type="text/css" href="http://localhost/libs/colorpicker/css/colorpicker.css" />

	<link rel="stylesheet" href="/css/style.css">
	<link rel="stylesheet" href="/css/modal.css">

	<link rel="stylesheet" type="text/css" href="http://localhost/libs/jquery.confirm.css" />

	<!--<script src="https://www.gstatic.com/firebasejs/3.6.4/firebase.js"></script>-->

</head>
<body>
<div id="overlay"></div>
<?php include "include/modal.php"; ?>
<div id="bod">
<!--<button id="asd" @mousedown="showHeader">Hide</button>-->
    <div id="header_image"></div>

	<div id="header_owerflow"></div>
	<div id="header">
	<?php
		$url = explode('/', $_SERVER['REQUEST_URI']);
		if($url[1] == "shops" || $url[1] == "shop"){
	?>
	<div id="cart">
		<i class="fas fa-shopping-cart"></i>
		<div><?php echo $data["cart"]["cartung"]["count"]; ?></div>
		<div><?php echo $data["cart"]["cartung"]["sum"]; ?> grn.</div>
	</div>
	<?php } ?>
	<div id="search">
	 		<?php if($data["my_info"]["sys_status"] == "active"){ ?>
   	 			<div></div>
	 				<i class="fas fa-search"></i>
   	 			<input type="text" placeholder="Search...">
	 		<?php } ?>
	 	</div>
		<div id="main_menu">
		 	<div id="main_menu_links">
		 		<a href="/">My page</a>
		 		<a href="/friends">Friends</a>
		 		<a href="/dialogs">
		 			Dialogs
		 			<span><?php if($data["dialog_notice"]["dialog_count"] > 0){ ?>
		 				<?php echo $data["dialog_notice"]["dialog_count"]; ?>
		 			<?php } ?>
		 			</span>
		 		</a>
		 		<a href="/shops">Shops</a>
	 		</div>
	 		<div id="main_menu_dropdown">
	 			<i class="fas fa-video"></i>
	 			<i class="fas fa-plus" id="aded_action"></i>
	 			<p id="my_page_logout">Выход</p>
	 		</div>
		</div>
	<div id="my_info">
		<div>
			<div>
				<p><?php echo $data['my_info']['main_name']; ?></p>
		    	<p><?php echo $data['my_info']['main_lastname']; ?></p>
	        </div>
			<img class="avatar_to_ch" src="<?php echo __NAME__; ?>img/<?php echo ($data['my_info']['sys_avatar'] == '') ? 'no_photo.jpg' : $data['my_info']['sys_avatar']; ?>" alt="<?php echo $data['my_info']['sys_avatar']; ?>" />
	    </div>
	</div>
	<div id="shop_stat_overlay"></div>
	<div id="shop_stat_mirror"></div>
	<div id="shop_stat_cont"></div>
	<div id="shop_stat_icon"></div>		
</div>
   	 	<!--<div id="logo"><img src="http://sn.nsh.com.ua/img/logo.png" alt="Logo!"></div>
   	 	<ul>
   	 		<li><a href="/friends_search">Пошук</a></li>
   	 		<li><a>Карти</a></li>
   	 		<li><a>Підтримка</a></li>
   	 		<li id="my_page_logout"><a>Вийти</a></li>
   	 	</ul>-->
   	<div id="menu_icon">
   		<!--<i class="fa fa-bars"></i>
   		<div>
   			<a href="/">My page</a>
	 		<a href="/friends">Friends</a>
	 		<a href="/dialogs">
	 			Dialogs
	 			<span><?php if($data["dialog_notice"]["dialog_count"] > 0){ ?>
	 				<?php echo $data["dialog_notice"]["dialog_count"]; ?>
	 			<?php } ?>
	 			</span>
	 		</a>
	 		<a href="/shops">Shops</a>
   		</div>-->
   		<nav class="item-menu">
		    <input type="checkbox" href="#" class="menu-open" name="menu-open" id="menu-open"/>
		    <label id="menu-open-handler" class="menu-open-button" for="menu-open">
		        <span class="plus plus-1"></span>
		        <span class="plus plus-2"></span>
		    </label>
		    <div class="real-menu">
		        <a href="<?php echo __NAME__; ?>" class="menu-item" data-name="File"> <i class="fas fa-home"></i> <label>Home</label></a>
		        <a href="<?php echo __NAME__; ?>friends" class="menu-item" data-name="Photo"> <i class="fas fa-user-friends"></i> <label>Friends</label></a>
		        <a href="<?php echo __NAME__; ?>dialogs" class="menu-item" data-name="Video"> <i class="fas fa-comments"></i> <label>Dialogs</label></a>
		        <a href="<?php echo __NAME__; ?>shops" class="menu-item" data-name="Snippet"> <i class="fas fa-shopping-cart"></i> <label>Shops</label></a>
		        <a href="<?php echo __NAME__; ?>" class="menu-item" data-name="Folder"> <i class="fa fa-folder"></i> <label>Whats</label></a>
		    </div>
		</nav>
		<!-- filters -->
<svg xmlns="http://www.w3.org/2000/svg" version="1.1" style="width: 0; height: 0;">
    <defs>
    <filter id="shadowed-goo">
        <feGaussianBlur in="SourceGraphic" result="blur" stdDeviation="10" />
        <feColorMatrix in="blur" mode="matrix" values="1 0 0 0 0  0 1 0 0 0  0 0 1 0 0  0 0 0 18 -7" result="goo" />
        <feGaussianBlur in="goo" stdDeviation="3" result="shadow" />
        <feColorMatrix in="shadow" mode="matrix" values="0 0 0 0 0  0 0 0 0 0  0 0 0 0 0  0 0 0 1 -0.2" result="shadow" />
        <feOffset in="shadow" dx="1" dy="1" result="shadow" />
        <feBlend in2="shadow" in="goo" result="goo" />
        <feBlend in2="goo" in="SourceGraphic" result="mix" />
    </filter>
    <filter id="goo">
        <feGaussianBlur in="SourceGraphic" result="blur" stdDeviation="10" />
        <feColorMatrix in="blur" mode="matrix" values="1 0 0 0 0  0 1 0 0 0  0 0 1 0 0  0 0 0 18 -7" result="goo" />
        <feBlend in2="goo" in="SourceGraphic" result="mix" />
    </filter>
    </defs>
</svg>
   	</div>
   	<div id="menu_settings">
   		<i class="fa fa-cog"></i>
   	</div>
   	<div id="menu_logout">
   		<i class="fas fa-sign-out-alt"></i>
   	</div>
	 <div id="content" act="">
		<?php include 'views/'.$content_view; ?>
	 </div>
</div>
		 <input type="hidden" id="user_id" value="<?php echo $_COOKIE['uid']; ?>" />
		 <input type="hidden" id="user_global_name" value="<?php echo $data['my_info']["main_name"]; ?>" />
	       <!-- SOUNDS -->
    <audio id="endCallSignal" preload="auto">
        <source src="/js/webrtc/audio/end_of_call.ogg" type="audio/ogg" />
        <source src="/js/webrtc/audio/end_of_call.mp3" type="audio/mp3" />
    </audio>

    <audio id="callingSignal" loop preload="auto">
        <source src="/js/webrtc/audio/calling.ogg" type="audio/ogg" />
        <source src="/js/webrtc/audio/calling.mp3" type="audio/mp3" />
    </audio>

    <audio id="ringtoneSignal" loop preload="auto">
        <source src="/js/webrtc/audio/AkellovDrPl.mp3" type="audio/mp3" />
    </audio>

    <!--<script src="https://cdnjs.cloudflare.com/ajax/libs/velocity/2.0.5/velocity.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/velocity/2.0.5/velocity.ui.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/vue@2.5.17/dist/vue.js"></script>-->
        <script type="text/javascript">
		var shref = window.location.href;
		var xshref = shref.split('/');
		/*var show_h = true;
        if(xshref[3] == 'friends' || xshref[3] == "dialogs" || xshref[3] == "shops" || xshref[3] == "shop")
        {
        	show_h = true;
        }

        if(show_h == true)
  		{
  			alert("OK");
  			document.getElementById("header_image").velocity({
	  			height: '70vh'
	  		},{
	  			duration: 200
	  		})
  		}
  		else
  		{
  			alert("OK");
  			Velocity(this.$refs.header_image, {
  				height: '15vh'
	  		},{
	  			duration: 200
	  		})
  		}
    new Vue({
  el: 'body',
  methods:{
  	showHeader(){

  	}
  }
  
})*/
		if(xshref[3] == "shops" || xshref[3] == "shop")
		{
			document.getElementById("shop_stat_icon").innerHTML = "<i class='fas fa-angle-down'></i>";
		}
	</script>
	<script src="http://localhost/libs/jquery.js"></script>
	<script src="https://code.highcharts.com/highcharts.js"></script>
	<script src="http://localhost/libs/carusel/src/js/lightslider.js"></script>
	<script src="http://localhost/libs/jquery.Jcrop.js"></script>
	<script src="http://localhost/libs/date.js"></script>
	<script type="text/javascript" src="http://localhost/libs/colorpicker/js/colorpicker.js"></script>
	
	<?php if($_COOKIE["priv"] == "admin"){ ?>
		<script src="/js/admin.js"></script>
	<?php } ?>
	<script src="http://localhost/libs/jquery.confirm.js"></script>
	<script src="http://localhost/js/basic.js"></script>
	<!--<script src="/js/videoChat.js"></script>-->
	<script src="http://localhost/js/modal.js"></script>
	<script src="http://localhost/js/anim.js"></script>
	<script src="http://localhost/js/user_wall.js"></script>
</body>
</html>